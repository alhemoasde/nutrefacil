<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

class Supernewsletter extends Module{

    /*
     * Réalisé par Webbax
     * http://www.webbax.ch
     * contact@webbax.ch
     * Module Newsletter
     * 
     
     V1.4.21 - 06.02.17
     - suppression utilisation cookie / cache newsletter (erreur 502) 
     V1.4.20 - 21.12.16
     - correction envoi SMTP version < PS 1.6.1.5
     V1.4.19 - 07.12.16
     - correction sur les emails envoyés X fois au même destinataire
     V1.4.18 - 30.09.16
     - contrôle validité de l'email dans front_cron_send.php
     V1.4.17 - 26.09.16
     - correction sur MailClass.php
     V1.4.16 - 13.09.16
     - correction sur envoi test SMTP
     V1.4.15 - 29.07.16
     - correction Swift Mailer SMTP PS >= 1.6.1.5
     V1.4.14 - 09.06.16
     - nouvelle méthode de détection HTTPS
     - correction Swift Mailer PS >= 1.6.1.5
     V1.4.13 - 22.02.16
     - contrôle supplémentaire pour le HTTPS
     - ajout ui.sortable (édition newsletter)
     - stripslash (textes avec apostrophe)
     - modification apparence formulaire désinscription
     V1.4.12 - 03.02.16
     - génération newsletter HTML (lien) à la fin sur balise img
     V1.4.11 - 13.11.15
     - correction sur le titre (db forcé en utf8)
     V1.4.10 - 20.07.15
     - correction pSQL dans "duplicateObject"
     V1.4.9 - 14.07.15
     - correction lien "delete.png" ajout produit ajax
     - correction balise fermante image produit
     - correction lien image produit / au lieu de TD
     - modification nom du champ "db" header_products en headerproducts 
     V1.4.8 - 09.06.15
     - correction affichage nb. clients enregistrés
     V1.4.7 - 29.05.15
     - correction notice import e-mails
     V1.4.6 - 21.05.15
     - ajout option email de l'expéditeur 
     - optimisations pour Prestashop Addons
     V1.4.5 - 07.05.15
     - révision structure du module
     - amélioration des traductions
     V1.4.4 - 30.04.15
     - ajout option prix par groupe clients
     - correction affichage message désinscription 
     - ajout image basique pour les thèmes clonés
     V1.4.3 - 29.04.15
     - intégration favicon (prévisualisation)
     V1.4.2 - 30.03.15
     - correction processus duplication newsletter
     - optimisations pour Prestashop Addons
     V1.4.1 - 23.03.15
     - correction "front_cron_send" (col. "id_lang" inexistante PS 1.5)
     - ajout traduction espagnol
     V1.4.0 - 17.03.15
     - correction sur le comptage des emails
     - correction sur le processus d'envoi tournant en boucle
     V1.3.9 - 19.12.14
     - correction import "nom" & "prénom"
     V1.3.9 - 19.12.14
     - correction import "nom" & "prénom"
     V1.3.8 - 13.10.14
     - correction adresse destinataire malformée
     V1.3.7 - 23.09.14
     - corrections traductions du coeur
     V1.3.6 - 10.09.14
     - correction sur le comptage des désinscriptions
     V1.3.5 - 01.09.14
     - correction suppression mail désinscription
     V1.3.4 - 16.07.14
     - retrait "bug" getIsset sur groupe clients (importation emails)
     V1.3.4 - 16.07.14
     - retrait "bug" getIsset sur groupe clients (importation emails)
     V1.3.3 - 04.06.14
     - correction SSL "jquery-ui-1.10.3.css" (newsletter_edit)
     - retrait $FileManager -> erreur fatale (newsletter_edit) 
     V1.3.2 - 28.04.14
     - correction image Legacy
     - retrait encodage email
     V1.3.1 - 15.04.14
     - ajout historique liste mails désinscription
     - optimisations exigences PS Addons
     V1.3.0 - 11.04.14
     - compatibilité Prestashop 1.6
     - ajout tri produits de la newsletter
     - correction UTM tracking (réseaux sociaux)
     */

    private $_html = '';
    public  $ps_version;

    function __construct(){
        
        $this->name = 'supernewsletter';
        $this->tab = 'advertising_marketing';
        $this->author = 'Webbax';
        $this->version = '1.4.21';
        $this->module_key = 'd2381116885cfe6e18bd1a86f63ece55';
        
        /* PS 1.6 */
        $this->bootstrap = true;
        $this->ps_version  = Tools::substr(_PS_VERSION_,0,3);
        
        parent::__construct();
        
        $this->displayName = $this->l('Module Super Newsletter');
        $this->description = $this->l('Envoyez facilement une newsletter à vos clients');
        
        // permet d'envoyer uniquement sur x adresses de debug
        $this->webbax_debug = Configuration::get('SUPERNEWS_WEBBAX_DEBUG');
        if($this->webbax_debug){
            $this->webbax_emails_debug = array('webbax.client@gmail.com','webbax.client.de@gmail.com');
        }else{
            $this->webbax_emails_debug = array();
        }
        
        require_once(dirname(__FILE__)).'/models/SupernewsletterContent.php';
        require_once(dirname(__FILE__)).'/models/SupernewsletterEmail.php';
        require_once(dirname(__FILE__)).'/models/SupernewsletterTemplate.php';
        require_once(dirname(__FILE__)).'/models/SupernewsletterStats.php';
        require_once(dirname(__FILE__)).'/classes/FileManagerClass.php';
        require_once(dirname(__FILE__)).'/classes/HelperClass.php';
        require_once(dirname(__FILE__)).'/classes/ToolClass.php';
        require_once(dirname(__FILE__)).'/classes/MailClass.php';
        require_once(dirname(__FILE__)).'/classes/CompatibilityClass.php';
        require_once(dirname(__FILE__)).'/classes/PostmarkSpamClass.php';
    }

    function install(){
        
        $statut = ToolClass::copyFolder(dirname(__FILE__).'/views/img/templates',dirname(__FILE__).'/templates');
        // error
        if($statut=='error'){
            Tools::displayError($this->l('Veuillez mettre le CHMOD 777 sur le répertoire').' : "/modules/supersnewsletter"');
            exit();
        // ok
        }else{
            // db
            require_once(dirname(__FILE__).'/install/init_db.php');
            // configuration par défaut
            Configuration::updateValue('SUPERNEWS_UTF8ENCODE',1);
            Configuration::updateValue('SUPERNEWS_EMAILS_PACK',20); 
            Configuration::updateValue('SUPERNEWS_WEBBAX_DEBUG',0);
        }
        
        // ajoute un accès dans le menu client
        if(!parent::install() OR !$this->installModuleTab('AdminSupernewsletter', array(1 => 'Super Newsletter', 2 => 'Super Newsletter', 3 => 'Super Newsletter', 4 => 'Super Newsletter', 5 => 'Super Newsletter'), 'AdminParentCustomer'))
            return false;
        else
            return true;
    }

    function uninstall(){
        
        // supprime les tables 
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'supernewsletter_content`');
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'supernewsletter_content_lang`');
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'supernewsletter_email`');
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'supernewsletter_stats`');
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'supernewsletter_template`');
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'supernewsletter_template_lang`');
        
        // retire les configs
        Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'configuration` WHERE `name` LIKE "%SUPERNEWS_%"');
        
        // supprime les thèmes modifiés
        Tools::deleteDirectory(dirname(__FILE__).'/templates');
        
        if(!parent::uninstall() OR !$this->uninstallModuleTab('AdminSupernewsletter'))
            return false;
        return true;
    }
    
    /**
     * Ajout d'un controller en BO dans l'onglets clients
     * @param type $tabClass
     * @param type $tabName
     * @param type $parentName
     * @return boolean
     */
    private function installModuleTab($tabClass, $tabName, $parentName) {
        $idTab = Tab::getIdFromClassName($parentName);
        if ($idTab != 0) {
            $tab = new Tab();
            $tab->name = $tabName;
            $tab->class_name = $tabClass;
            $tab->module = $this->name;
            $tab->id_parent = $idTab;
            if (!$tab->save())
                return false;
        }
        return true;
    }
    
    /**
     * suppression du controllers du BO
     * @param type $tabClass
     * @return boolean
     */
    private function uninstallModuleTab($tabClass) {
        $idTab = Tab::getIdFromClassName($tabClass);
        if ($idTab != 0) {
            $tab = new Tab($idTab);
            $tab->delete();
            return true;
        }
        return false;
    }
    
    public function getContent(){
        
        // librairie requise
        if(!extension_loaded('curl')) {
           $this->_html.= '<div class="warn">'.Tools::displayError('Veuillez activer l\'extension PHP "curl" dans votre php.ini').'</div>';
        }
        
        if($this->ps_version=='1.6'){
            $this->_html .= '<link rel="stylesheet" type="text/css" href="'.$this->_path.'views/css/styles_1.6.css">';
        }
        
        // ressources pour l'ensemble des pages
        $cookie = $this->context->cookie;
        $id_lang = $this->context->cookie->id_lang;
        $id_shop = $this->context->shop->id;
        $languages = Language::getLanguages(false);
        $current_index = 'index.php?controller=AdminModules&token='.Tools::getAdminTokenLite('AdminModules').'&configure='.$this->name;
        $Shop = new Shop($id_shop);
        
        // CSS
        $this->_html.='<link rel="stylesheet" type="text/css" href="'.$this->_path.'views/css/global.css">';
        $this->_html.='<link rel="stylesheet" type="text/css" href="'.$this->_path.'views/css/bo_style.css">';
        
        // Webbax Debug ?
        if($this->webbax_debug){
            $this->_html.='<div class="warn">'.$this->l('Webbax debug actif : envoi uniquement sur ').': <strong>webbax.client@gmail.com / webbax.client.de@gmail.com</strong></div>';
        }
        
        // messages
        $this->_html.=$this->msgType();
        
        ini_set('display_errors','on');
        
        // add - update newsletter
        if(Tools::isSubmit('updatesupernewsletter') || Tools::isSubmit('addsupernewsletter')){
            $page_name = 'admin_newsletter_edit';
        }elseif(Tools::isSubmit('templates') || Tools::isSubmit('duplicatesupernewsletter_template') || Tools::isSubmit('deletesupernewsletter_template')){
            $page_name = 'admin_templates';
        }elseif(Tools::isSubmit('updatesupernewsletter_template')){
             $page_name = 'admin_template_edit';
        }elseif(Tools::isSubmit('importemailsedit')){
            $page_name = 'admin_emails_import_edit';
        }elseif(Tools::isSubmit('importemails')){
            $page_name = 'admin_emails_import';
        }elseif(Tools::isSubmit('configuration')){
            $page_name = 'admin_configuration';
        }elseif(Tools::isSubmit('stats')){
            $page_name = 'admin_stats';
        }elseif(Tools::isSubmit('sending')){
            $page_name = 'admin_sending';
        }elseif(Tools::isSubmit('cron')){
            $page_name = 'admin_cron';
        // home
        }else{
            require_once(dirname(__FILE__).'/update/update.php');
            $page_name = 'admin_home';
        }
        
        $this->_html.= '<div id="'.$page_name.'">';
            require_once(dirname(__FILE__).'/'.$page_name.'.php');
        $this->_html.= '</div>';
            
        return $this->_html;
        
    }
    
    /*
     * Messages de confirmation
     * @param -
     * @return -
     */
    function msgType(){
        $msgtype = Tools::getValue('msgtype');
        if(!empty($msgtype)){
            if($msgtype==1){$msgtype=$this->l('La newsletter a été enregistrée');}
            if($msgtype==2){$msgtype=$this->l('La newsletter a été modifiée');}
            if($msgtype==3){$msgtype=$this->l('Le thème a été enregistré');}
            if($msgtype==4){$msgtype=$this->l('Le thème a été modifié');}
            if($msgtype==5){$msgtype=$this->l('La configuration a été modifiée');}
            return $this->displayConfirmation($msgtype);
        }
    }  
    
    /*
     * Retourn le statut de la newsletter + icon
     * @param int (statut)
     * @return html 
     */
    function getStatutNewsletter($statut){
        if($statut==0){
            $html='<img src="'.$this->_path.'views/img/time.png'.'" class="icon_top" /> '.$this->l('En attente d\'envoi');
        }elseif($statut==1){
            $html='<img src="'.$this->_path.'views/img/email_error.png'.'" class="icon_top" /> '.$this->l('Envoi interrompu');
        }elseif($statut){
            $html='<img src="'.$this->_path.'views/img/ok.png'.'" class="icon_top" /> '.$this->l('Envoi effectué avec succès');
        }
        return $html;
    }
    
    /*
     * Liste des adresses emails clients en fonction des critères de la newsletter
     * @param int $id_supernewsletter_content
     * @return array (emails)
     */
    function getCustomersEmails($id_supernewsletter_content){
            
        $SupernewsletterContent = new SupernewsletterContent($id_supernewsletter_content);
        
        // init
        $LEFT_JOIN = '';
        $AND = '';
        $GROUP_BY = '';

        // si restriction sur le groupe
        $ids_groups = unserialize($SupernewsletterContent->ids_groups);
        $list_id_groups='';
        foreach($ids_groups as $id_group){
            if($id_group!=''){$list_id_groups.=$id_group.',';}
        }
        $list_id_groups = Tools::substr($list_id_groups,0,-1);

        if(!empty($list_id_groups)){
            $LEFT_JOIN = 'LEFT JOIN `'._DB_PREFIX_.'customer_group` cg
                          ON cg.id_customer = c.id_customer ';
            $AND = 'AND cg.id_group IN('.$list_id_groups.') ';
            $GROUP_BY = 'c.`email`';
        }

        // si restriction sur le pays
        if(!empty($SupernewsletterContent->id_country)){
            $LEFT_JOIN .= 'LEFT JOIN `'._DB_PREFIX_.'address` ad
                          ON ad.id_customer = c.id_customer ';
            $AND .= 'AND ad.id_country ="'.pSQL($SupernewsletterContent->id_country).'" ';
            $GROUP_BY .= (!empty($GROUP_BY)?$GROUP_BY.=',':'').'ad.`id_customer` ';
        }

        $sql = 'SELECT `email` FROM `'._DB_PREFIX_.'customer` c '.
                                                     $LEFT_JOIN.'
                                                     WHERE c.`id_shop`="'.pSQL($this->context->shop->id).'" 
                                                     '.((!$SupernewsletterContent->cust_disabled)?'AND c.`active`=1':'').'
                                                     '.((!$SupernewsletterContent->cust_no_news)?'AND c.`newsletter`=1':'').' 
                                                     '.((!$SupernewsletterContent->cust_deleted)?'AND c.`deleted`=0':'').
                                                     ' '.$AND.
                                                     (!empty($GROUP_BY)?'GROUP BY '.$GROUP_BY:'');

        if(empty($list_id_groups)){
            return array();
        }else{
            $emails_customers_clean = array();
            $emails_customers = Db::getInstance()->Executes($sql);
            foreach($emails_customers as $e_c_c){
                $emails_customers_clean[]['email'] = $this->cleanEmail($e_c_c['email']);
            }
            return $emails_customers_clean;
        }
    }
    
    /*
     * Liste des adresses emails des inscrits via le front
     * @param bool
     * @param $id_supernewsletter_content
     * @return array (emails)
     */
    function getSubscribesEmails($register_front,$id_supernewsletter_content){
        if($register_front){
            $SupernewsletterContent = new SupernewsletterContent($id_supernewsletter_content);
            $sql = 'SELECT `email` FROM `'._DB_PREFIX_.'newsletter` 
                    WHERE `id_shop`="'.pSQL($this->context->shop->id).'" '.
                    ((!$SupernewsletterContent->cust_disabled)?'AND `active`=1':'');
            $emails_subscribes = Db::getInstance()->ExecuteS($sql);
            if(empty($emails_subscribes)){return array();}
            $emails_subscribes_clean = array();
            foreach($emails_subscribes as $e_s_c){
                $emails_subscribes_clean[]['email'] = $this->cleanEmail($e_s_c['email']);
            }
            return $emails_subscribes_clean;
            
        }else{
            return array();
        }
    }
      
    /*
     * Liste des adresses emails importées
     * @param  array (groups_names)
     * @return array (emails)
     */
    function getImportEmails($groups_names=array()){
        
        $AND = '';
        $groups_names_list='';
        
        if(!empty($groups_names)){
            foreach($groups_names as $group_name){
                $groups_names_list.='"'.$group_name.'",';
            }
            $groups_names_list = Tools::substr($groups_names_list,0,-1);
            $AND = 'AND `group` IN ('.$groups_names_list.')';
            $sql = 'SELECT `email` 
                    FROM `'._DB_PREFIX_.'supernewsletter_email` 
                    WHERE `id_shop`="'.pSQL($this->context->shop->id).'"'.
                    $AND;
            $emails_import = Db::getInstance()->ExecuteS($sql);
            $emails_import_clean = array();
            foreach($emails_import as $e_i_c){
                $emails_import_clean[]['email'] = $this->cleanEmail($e_i_c['email']);
            }
            return $emails_import_clean;
            
        }else{
            return array();
        }
    }
    
    /*
     * Liste les alias disponibles dans la newsletter
     * @param  -
     * @return array
     */
    function getAlias(){
        $alias = array();
        $alias[] = array('field_name'=>'firstname',
                         'label'=>$this->l('Prénom'));
        $alias[] = array('field_name'=>'lastname',
                         'label'=>$this->l('Nom'));
        return $alias;
    }
    
    
    /* ######################################################
     * Helpers
     * ######################################################
     */
    
    /*
     * Helpers - colonne statut
     * @param value
     * @return string statut
     */
    function admin_home_colStatut($statut){
        return $this->getStatutNewsletter($statut);             
    }
    
    /*
     * Helpers - colonne date
     * @param value
     * @return string date
     */
    function admin_home_colDate($date){
        return ToolClass::dateUStoFR($date);       
    }
    
    /*
     * Helpers - colonne date
     * @param value
     * @return string date
     */
    function admin_home_colCron($cron){
        if($cron){
            $html = '<img src="'.$this->_path.'views/img/ok.png" class="icon_top" /> '.$this->l('Oui');
        }else{
            $html = '<img src="'.$this->_path.'views/img/cancel.png" class="icon_top" /> '.$this->l('Non');
        } 
        return $html;
    }
    
    /*
     * Helpers - colonne langue
     * @param value
     * @return string langue
     */
    function admin_emails_import_colLang($id_lang){
        $Language = new Language($id_lang);
        return $Language->iso_code;       
    }
    
    /*
     * Helpers - colonne date
     * @param value
     * @return string date
     */
    function admin_emails_import_colDate($date){
        return ToolClass::dateUStoFR($date);       
    }
    
    /*
     * Helpers - colonne preview
     * @param value
     * @return string html
     */
    function admin_templates_colPreview($template_name){
        $id = Db::getInstance()->getValue('SELECT `id_supernewsletter_template` FROM `'._DB_PREFIX_.'supernewsletter_template` WHERE `name`="'.pSQL($template_name).'"');
        if(file_exists(dirname(__FILE__).'/templates/'.$id.'/preview.jpg') && strpos($template_name,'>')==3){
           $link = $this->_path.'/templates/'.$id.'/preview.jpg';
        }else{
           $link = $this->_path.'/views/img/no_preview.jpg';
        }
        return '
        <table class="table_img_preview">
            <tr><td><img src="'.$link.'" class="img_preview" /></td></tr>
            <tr><td align="center">'.$template_name.'</td></tr>
        </table>';
    }
    
    /*
     * Toolbar pour Prestashop 1.6
     * @param object $helper
     * @return html
     */
    function toolbarModule($helper){
        $toolbar = '<div id="module_toolbar" class="panel-header">';
        if(isset($helper->toolbar_btn)){$toolbar_btn=$helper->toolbar_btn;}else{$toolbar_btn=$helper;}
        foreach($toolbar_btn as $btn){
            if(!empty($btn['icon'])){
                if(isset($btn['target']) && !empty($btn['target'])){$target='target="_blank"';}else{$target='';}
                if(isset($btn['icon'])){$icon='<i class="'.$btn['icon'].'"></i> ';}else{$icon='';}
                $toolbar.= '<span class="span_block"><a href="'.$btn['href'].'" '.$target.' class="btn btn-default">'.$icon.$btn['desc'].'</a></span>';
            }
        } 
        $toolbar.='</div>';
        return $toolbar;
    }
    
    
    /*
     * Nettoie le format de l'email
     * @param string $email
     * @return string
     */
    function cleanEmail($email){
        $email = str_replace(array("\n","\r","\t"),'',$email);
        $email = str_replace("'",'',$email);
        $email = str_replace('"','',$email);
        $email = Tools::stripslashes($email);
        $email = pSQL($email);
        $email = filter_var($email,FILTER_SANITIZE_EMAIL);   
        return $email;
    } 
    
    /*
     * Trouve le nom exact de l'image type
     * @param string ($name)
     * @return string
     */
    function getImageType($name){ 
        $image_type='';
        $imagesType = ImageType::getImagesTypes();
        foreach($imagesType as $it){if(strpos($it['name'],$name)!==false){$image_type=$it['name'];break;}}
        if(empty($image_type)){return 'unknow';}else{return $image_type;}
    }
    
}
?>