<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

if(!defined('_COOKIE_KEY_')){die('token error');}

// importation des adresses
if(Tools::getIsset('submitImportEmails')){
    
    $FileManager = new FileManager();
    if($FileManager->checkFormat('csv',$_FILES['file_csv']['name']) && empty($this->_errors)){
        
        // tranfert
        $type = Tools::strtolower(Tools::substr(strrchr($_FILES['file_csv']['name'],'.'),1));
        $folder_dir = dirname(__FILE__).'/imports/';
        $file_dest = $folder_dir.'emails_'.date('Y-d-m_H-i-s').'.'.$type;
        $FileManager->moveUploadedFile($_FILES['file_csv']['tmp_name'],$file_dest);    
        
        // import
        $nb_insert = 0;
        $nb_duplicate = 0;
        $duplicate_emails = array();
        $f=fopen($file_dest,"r");
        while($line = fgets($f,1000)){
            $values = explode(';',utf8_decode($line));
            $email = $values[0];
            // contrôle doublon
            $exist_supernewsletter_email = Db::getInstance()->getRow('SELECT `email` FROM `'._DB_PREFIX_.'supernewsletter_email` WHERE `email`="'.pSQL($email).'"');
            $exist_customer = Db::getInstance()->getRow('SELECT `email` FROM `'._DB_PREFIX_.'customer` WHERE `email`="'.pSQL($email).'" AND `id_shop`="'.pSQL($id_shop).'"');
            $exist_newsletter = Db::getInstance()->getRow('SELECT `email` FROM `'._DB_PREFIX_.'newsletter` WHERE `email`="'.pSQL($email).'" AND `id_shop`="'.pSQL($id_shop).'"');
            
            if(empty($exist_supernewsletter_email) && empty($exist_customer) && empty($exist_newsletter) && !empty($email)){
                $SupernewsletterEmail = new SupernewsletterEmail();
                $SupernewsletterEmail->email = $email;
                $SupernewsletterEmail->firstname = @$values[1];
                $SupernewsletterEmail->lastname = @$values[2];
                $SupernewsletterEmail->date_add = date('Y-m-d H:i:s');
                    $group = str_replace(CHR(13).CHR(10),"",trim(@$values[3]));
                    if(empty($group)){$group='no_group';}
                $SupernewsletterEmail->group = $group;
                    if(@$values[4]!=0){$id_lang=$values[4];}
                $SupernewsletterEmail->id_lang = $id_lang;
                $SupernewsletterEmail->id_shop = $id_shop;
                $SupernewsletterEmail->add();
                $nb_insert++;
            }else{
                $nb_duplicate++;
                $duplicate_emails[] = $email;
            }
        }
        
        $this->_html.=$this->displayConfirmation($this->l('Import effectué',$page_name));
        if($nb_insert>0){
            $this->_html.=$this->displayConfirmation($nb_insert.' '.$this->l('Adresse(s) importée(s)',$page_name));
        }
        if($nb_duplicate>0){
            $list_emails = '';
            foreach($duplicate_emails as $email){$list_emails.=$email.'<br/>';}
            $this->_html.=$this->displayError($nb_duplicate.' '.$this->l('Adresse(s) déjà présente(s) dans la base',$page_name).' : <br/>'.$list_emails);
        }
        
    }
    // Affiche les erreurs
    if(count($FileManager->_errors)){
        foreach($FileManager->_errors as $err){
            $this->_html .= '<div class="alert error">'.$err.'</div>';
        }
    }
}

// suppression unique email
if(Tools::getIsset('delete_email')){
    Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'supernewsletter_email` WHERE `id_supernewsletter_email`="'.pSQL(Tools::getValue('delete_email')).'"');
    $this->_html.=$this->displayConfirmation($this->l('Suppression effectuée',$page_name));
}

// suppression en masse des emails
if(Tools::getIsset('submitDeleteMulti')){
    $ids_supernewsletter_email = Tools::getValue('line_email');
    if(!empty($ids_supernewsletter_email)){
        foreach($ids_supernewsletter_email as $id_supernewsletter_email){
            Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'supernewsletter_email` WHERE `id_supernewsletter_email`="'.pSQL($id_supernewsletter_email).'"');
        }
        $this->_html.=$this->displayConfirmation($this->l('Suppression effectuée',$page_name));
    }
}
   
$this->toolbar_btn['back'] =  array(
    'href' => $current_index,
    'desc' => $this->l('Revenir à la liste des newsletter',$page_name),
    'icon' => 'icon-arrow-left'
);

if($this->ps_version==1.6){$this->_html.=$this->toolbarModule($this->toolbar_btn);}
		
// Formulaire
foreach($languages as $k => $language){$languages[$k]['is_default'] = (int)($language['id_lang'] == Configuration::get('PS_LANG_DEFAULT'));}

$helper = new HelperForm();  
$helper->module = $this;
$helper->name_controller = 'supernewsletter';
$helper->identifier = $this->identifier;
$helper->token = Tools::getAdminTokenLite('AdminModules');
$helper->languages = $languages;
$helper->currentIndex = $current_index.'&importemails';
$helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
$helper->allow_employee_form_lang = true;
$helper->toolbar_scroll = true;
$helper->toolbar_btn = $this->toolbar_btn;
$helper->show_toolbar = true;
$helper->title = $this->displayName;
$helper->submit_action = 'submitImportEmails';

$this->fields_form[0]['form'] = array(
    'tinymce' => true,
    'legend' => array(
            'title' => $this->l('Importer des adresses email',$page_name),
            'image' => $this->_path.'logo.gif'
    ),
    'input' => array(
            array(
                    'type' => 'file',
                    'label' => $this->l('Fichier',$page_name).' .CSV',
                    'name' => 'file_csv',
                    'desc' => '<br/>
                              ('.$this->l('CSV séparé par des points virgules',$page_name).')<br/>'.
                              $this->l('1ère colonne => adresse email',$page_name).'<br/>'.
                              $this->l('2ème colonne => prénom',$page_name).'<br/>'.
                              $this->l('3ème colonne => nom',$page_name).'<br/>'.
                              $this->l('4ème colonne => nom du groupe client',$page_name).'<br/>'.
                              $this->l('5ème colonne => langue (id_lang)',$page_name).'<br/>
                              <br/>'.
                              '<a href="'.$Shop->getBaseURL().'modules/'.$this->name.'/imports/sample.csv" target="_blank"><img src="'.$this->_path.'views/img/csv.gif"/></a> <a href="'.$Shop->getBaseURL().'modules/'.$this->name.'/imports/sample.csv" target="_blank" class="link">'.$this->l('Télécharger le fichier d\'exemple',$page_name).'</a>',
            ),
    ),	
    'submit' => array(
            'name' => 'submitImportEmails',
            'title' => $this->l('Importer les adresses',$page_name),
            'class' => 'button'
    ),
);		
$this->_html .= $helper->generateForm($this->fields_form);


// ----------------------------------------
// Tableau listes des emails 
// ----------------------------------------

$nb_emails = Db::getInstance()->getValue('SELECT count(id_supernewsletter_email) FROM '._DB_PREFIX_.'supernewsletter_email sc WHERE sc.`id_shop`='.pSQL($this->context->shop->id)); 
$emails = Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'supernewsletter_email sc WHERE sc.`id_shop`='.pSQL($this->context->shop->id).' '.HelperClass::orderByAndSearchParams()); 

$Helper = new HelperClass();
$this->_html.='
<br/>
<table class="table table_grid" cellspacing="0" cellpadding="0">';
$this->_html.='
<tr>
    <th>'.$Helper->buttonCheckUncheck('line_email').'</td>
    <th>ID</td>
    <th>'.$this->l('Email',$page_name).'</td>
    <th>'.$this->l('Prénom',$page_name).'</th>
    <th>'.$this->l('Nom',$page_name).'</th>
    <th>'.$this->l('Groupe',$page_name).'</th>
    <th>'.$this->l('Langue',$page_name).'</th>
    <th>'.$this->l('Date ajout',$page_name).'</th>
    <th>'.$this->l('Actions',$page_name).'</th>
</tr>';

$this->_html.='<div id="block_orderby" class="float_left">'.$Helper->widgetOrderBy(array('id_supernewsletter_email'=>$this->l('ID',$page_name),
                                            'email'=>$this->l('Email',$page_name),
                                            'firstname'=>$this->l('Prénom',$page_name),
                                            'lastname'=>$this->l('Nom',$page_name),
                                            'group'=>$this->l('Groupe',$page_name),
                                            'id_lang'=>$this->l('Langue',$page_name)
                                      ),
                                     $current_index.'&importemails').'</div>';

$this->_html.='<div id="block_searchby" class="float_right">'.$Helper->widgetSearch(array('email'=>$this->l('Email',$page_name),
                                                  'firstname'=>$this->l('Prénom',$page_name),
                                                  'lastname'=>$this->l('Nom',$page_name),
                                                  'group'=>$this->l('Groupe',$page_name),
                                                  )
                                            ).'</div><br/><br/>';

$this->_html.='
<form method="post">';
    foreach($emails as $k=>$email){
        if($k%2==1){$alt_row='alt_row';}else{$alt_row='';}
        $this->_html.='
        <tr class="row_hover '.$alt_row.'">
            <td><input type="checkbox" class="line_email" value="'.$email['id_supernewsletter_email'].'" name="line_email[]"></td>
            <td>'.$email['id_supernewsletter_email'].'</td>
            <td>'.$email['email'].'</td>
            <td>'.$email['firstname'].'</td>
            <td>'.$email['lastname'].'</td>
            <td>';
                if($email['group']=='no_group'){$email_group=$this->l('Aucun groupe',$page_name);}else{$email_group=$email['group'];}
                $this->_html.= $email_group.
            '</td>';
            $Language = new Language($email['id_lang']);
            $this->_html.='
            <td>'.$Language->iso_code.'</td>
            <td>'.$email['date_add'].'</td>
            <td>
                <a href="'.$_SERVER['REQUEST_URI'].'&importemailsedit&id_supernewsletter_email='.$email['id_supernewsletter_email'].'"><img src="'.$this->_path.'views/img/edit.png'.'" /></a>
                <a href="'.$_SERVER['REQUEST_URI'].'&delete_email='.$email['id_supernewsletter_email'].'"><img src="'.$this->_path.'views/img/delete.png'.'"  onclick="if(confirm(\''.$this->l('Supprimer cet élément ?',$page_name).'\')){return true;}else{event.stopPropagation();event.preventDefault();};" /></a>
            </td>
        </tr>';
    }
    if(empty($emails)){
        $this->_html.='<tr><td colspan="9">- '.$this->l('Aucun enregistrement',$page_name).'</td></tr>';
    }

    $this->_html.='</table>
    <br/>
    <input type="submit" name="submitDeleteMulti" value="'.$this->l('Supprimer la sélection',$page_name).'" class="button btn btn-default" />
</form>
<br/>
<br/>';

$Helper = new HelperClass();
$this->_html.='<div id="pagination">'.$Helper->widgetPagination($nb_emails,50,$current_index.'&importemails').'</div>';
		
?>
