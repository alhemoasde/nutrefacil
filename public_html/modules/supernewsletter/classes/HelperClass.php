<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

class HelperClass extends Module{
    
    /*
     * Ajout la notion de tri à la requête
     * @param  -
     * @return string
     */
    static function orderByAndSearchParams(){
        
        $req_end = '';
        $order_by = Tools::getValue('order_by');
        $order_type = Tools::getValue('order_type');
        $search_string = Tools::getValue('search_string');
                
        // gestion du tri uniquement si aucune recherche en cours
        if(empty($search_string)){
            if(empty($order_type)){
                $order_type='ASC';
            }
            if(!empty($order_by)){
                $req_end = ' ORDER BY `'.$order_by.'` '.$order_type; 
            }
        }
        
        if(empty($search_string)){
           $req_end .= ' LIMIT '.Tools::getValue('limit1',0).','.Tools::getValue('limit_per_page',50);
        }else{
           $req_end .= ' AND `'.Tools::getValue('search_column').'` LIKE "%'.Tools::getValue('search_string').'%"'; 
        }
        
        return $req_end;  
    }
    
    
   /*
    * Permet de faire le tri
    * @param array (colname => label)
    * @param string $current_index
    * return html
    */
    function widgetOrderBy($columns,$current_index){
        
        $html= '
        <script type="text/javascript">
        $(document).ready(function(){
            $("#order_by, #order_type").change(function(){
                if($("#order_by").val()!=""){
                    window.location.href = $("#order_by").val()+"&order_type="+$("#order_type").val();
                }
            });
            $("#reset_filter").click(function(){
                 window.location.href = $("#reset_filter_url").val();
            });
        })
        </script>';
        
        $order_type = Tools::getValue('order_type');
        if($order_type==''){$order_type='ASC';}
        $order_by = Tools::getValue('order_by');
        
        $html .=
        self::HelperClassTrans('tri_par').' : 
        <select id="order_by">';
            $html.='<option value="">'.self::HelperClassTrans('selectionner');
            foreach($columns as $k=>$v){
                if($order_by==$k){$selected='selected';}else{$selected='';}
                $html.='<option value="'.$current_index.'&order_by='.$k.'" '.$selected.'>'.$v;
            }
        $html.='
        </select>';
        
        $html.='
        &nbsp;
        <select id="order_type">';
            if(empty($order_by)){
              $html.='<option value="">-';
            }
            $order_type = Tools::getValue('order_type');
            if($order_type=='ASC'){$selected='selected';}else{$selected='';}
            $html.='<option value="ASC" '.$selected.'>&darr; A-z';
            if($order_type=='DESC'){$selected='selected';}else{$selected='';}
            $html.='<option value="DESC" '.$selected.'>&uarr; Z-a
        </select>';
        
        $html.='
        <input type="hidden" value="'.$current_index.'"  id="reset_filter_url" />
        <input value="'.self::HelperClassTrans('reinitialiser').'" class="button btn btn-default" type="button" style="margin-left:5px;" id="reset_filter">';
            
        return $html;
    }
    
    /*
    * Affiche un composant de pagination
    * @param int nbre d'enregistrements
    * @param int nbre enregistrements par page
    * @param string url
    */ 
   function widgetPagination($nb_data,$limit_per_page,$current_index){   
       
       $html = '';
       $nb_pages = ceil($nb_data/$limit_per_page); 
       $limit_per_page_get = Tools::getValue('limit_per_page');
       if($limit_per_page_get>=1000000){$nb_pages=1;}
       
       $html .= self::HelperClassTrans('page').' : ';
       for($i=1;$nb_pages>=$i;$i++){
            $page_no = Tools::getValue('page_no');
            if(empty($page_no)){$page_no=1;}
            if($page_no==$i){$html.='<span style="font-weight:bold;text-decoration:underline;">';}
                $html.= '<a href="'.$current_index.'&order_by='.Tools::getValue('order_by').'&order_type='.Tools::getValue('order_type').'&page_no='.$i.'&limit1='.(($i-1)*$limit_per_page).'">'.$i.'</a>';
            if($page_no==$i){$html.='</span>';}
             $html.= '&nbsp;';
       }
        
       if($limit_per_page_get>=1000000){
           $html.=' <a href="'.$current_index.'&order_by='.Tools::getValue('order_by').'&order_type='.Tools::getValue('order_type').'&limit1=0&limit_per_page='.$limit_per_page.'">'.' - '.self::HelperClassTrans('afficher_la_pagination').'</a>';
       }else{
           $html.=' <a href="'.$current_index.'&order_by='.Tools::getValue('order_by').'&order_type='.Tools::getValue('order_type').'&limit1=0&limit_per_page=1000000">'.' - '.self::HelperClassTrans('tout_sur_une_page').'</a>';
       }
       
       // pagination affichée uniquement en cas de non-recherche
       $search_string = Tools::getValue('search_string');
       if(empty($search_string)){
           return $html;
       }
   }
   
   function widgetSearch($columns){
        
        $search_column = Tools::getValue('search_column');
        $select = 
        '<span style="margin-left:5px">'.self::HelperClassTrans('dans_la_colonne').' : </span> 
        <select id="search_column" name="search_column">';
            foreach($columns as $k=>$v){
                if($search_column==$k){$selected='selected';}else{$selected='';}
                $select.='<option value="'.$k.'" '.$selected.'>'.$v;
            }
        $select.='
        </select>';
       
       $search_string = Tools::getValue('search_string');
       if(!empty($search_string)){$value=$search_string;}else{$value='';}
       $html = '
       <form method="post">
            <span style="margin-right:5px;">'.self::HelperClassTrans('rechercher').' : </span><input type="text" name="search_string" value="'.$value.'" />'.
            $select.'
            <input type="submit" class="button btn btn-default" style="margin-left:5px;" value="'.self::HelperClassTrans('valider').'" />
            <a href="'.$_SERVER['REQUEST_URI'].'" class="button btn btn-default">'.self::HelperClassTrans('reinitialiser').'</a>
        </form>';
       return $html;
   }
   
   /*
    * Bouton qui permet de cocher/déocher de multiples checkbox
    * @param string nom de la class
    * @return html
    */
   function buttonCheckUncheck($class_name){
       $html = '
       <script type="text/javascript">
            $(document).ready(function(){
                $(".check:button").toggle(function(){
                    $(".'.$class_name.'").attr("checked","checked");
                    $(this).val("'.self::HelperClassTrans('decocher_tout').'")
                },function(){
                    $(".'.$class_name.'").removeAttr("checked");
                    $(this).val("'.self::HelperClassTrans('tout_cocher').'");        
                })
            })
       </script>
       <input type="button" class="check button btn btn-default" value="'.self::HelperClassTrans('tout_cocher').'" />';
       return $html;
   }
   
   /*
    * Gestion des traductions de la classe
    * @param string (code traduction)
    * @return string (traduction dans la langue courante)
    */
   function HelperClassTrans($string){
       $Context = Context::getContext();
       $id_lang = $Context->cookie->id_lang;
       $iso = Language::getIsoById($id_lang);
       if($iso=='de'){
           $t=array(
                'afficher_la_pagination'=>'Zeigen Paginierung',
                'dans_la_colonne' => 'In der Spalte',
                'decocher_tout' => 'Auswahl aufheben',
                'page'=>'Seite',
                'rechercher' => 'Suchen',
                'reinitialiser' => 'Zurücksetzen',
                'selectionner'=>'Wählen',
                'tout_cocher' => 'Überprüfen Sie alle',
                'tout_sur_une_page'=>'Alles auf einer Seite',
                'tri_par'=>'Sortieren nach',
                'valider' => 'Einreichen',  
            );
       }elseif($iso=='en'){
            $t=array(
                'afficher_la_pagination'=>'Show Pagination',
                'dans_la_colonne' => 'In the column',
                'decocher_tout' => 'Uncheck all',
                'page'=>'Page',
                'rechercher' => 'Search',
                'reinitialiser' => 'Reset',
                'selectionner'=>'Select',
                'tout_cocher' => 'Check all',
                'tout_sur_une_page'=>'All on one page',
                'tri_par'=>'Sort by',
                'valider' => 'Submit',  
            );
       }else{
            $t=array(
                'afficher_la_pagination'=>'Afficher la pagination',
                'dans_la_colonne' => 'Dans la colonne',
                'decocher_tout' => 'Décocher tout',
                'page'=>'Page',
                'rechercher' => 'Rechercher',
                'reinitialiser' => 'Réinitialiser',
                'selectionner'=>'Sélectionner',
                'tout_cocher' => 'Tout cocher',
                'tout_sur_une_page'=>'Tout sur une page',
                'tri_par'=>'Tri par',
                'valider' => 'Valider',  
            );
       }
       return $t[$string];
   }
    
}

?>
