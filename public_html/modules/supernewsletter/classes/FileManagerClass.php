<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

/*
 * Webbax - 19.07.13
 * Class facilitant la gestion des fichiers
 */
class FileManager extends Module{
    
    public $_errors;
    
    /*
     * Vérifie le format d'un fichier 
     * @param string type de fichier
     * @param string nom du fichier
     * @param return bool
     */
    function checkFormat($type,$file_name){       
        if(isset($file_name) && !empty($file_name)){     
            if($type=='image'){
                $types = array('jpg','gif','jpeg','png');
            }elseif($type=='csv'){
                $types = array('csv');
            }
            $type = Tools::strtolower(Tools::substr(strrchr($file_name, '.'), 1));
            if(!in_array($type,$types)){
                $this->_errors[] = self::FileManagerClassTrans('format_fichier_invalide');
                return false;
            }else{
                return true;
            } 
        }  
    }
   
    /*
     * Crée un nouveau répertoire
     * @param string emplacement
     * @param return bool
     */
    function newFolder($folder){
        if(!file_exists($folder)){
            if(!mkdir($folder,0777)){
                $this->_errors[] = self::FileManagerClassTrans('erreur_creation_repertoire').' '.$folder; 
                return false;
            }else{
                return true;
            }
        }else{
            return true;
        }
    }
    
    /*
     * Transfert le fichier uploadé
     * @param string emplacement fichier source
     * @param string emplacement fichier destination
     * @param int largeur de l'image
     * @param return bool
     */
    function moveUploadedFile($file_src,$file_dest,$img_width=null){
        if(move_uploaded_file($file_src,$file_dest)){
            if(!empty($img_width)){
                ImageManager::resize($file_dest,$file_dest,$img_width);
            }
            return true;
        }else{
           $this->_errors[] = self::FileManagerClassTrans('erreur_transfert');
           return false;
        }
    }
    
    /*
     * Supprime un fichier
     * @param string url fichier
     * @param return -
     */
    static function deleteFile($url_file){
        @chmod($url_file,0777);
        @unlink($url_file);
    }
    
    /*
    * Gestion des traductions de la classe
    * @param string (code traduction)
    * @return string (traduction dans la langue courante)
    */
   function FileManagerClassTrans($string){
       $t=array(
           'erreur_creation_repertoire'=>'Erreur de création du répertoire, mettez le CHMOD 777 sur le dossier',
           'erreur_transfert'=>'Erreur de transfert de fichier',
           'format_fichier_invalide'=>'Format du fichier non valide',
       );
       return $t[$string];
   }
    
}

?>
