<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

class ToolClass{
        
    /*
     * Converti une date simple US en FR
     * @param string (date)
     * @return string
     */
    static function dateUStoFR($date){
        $date = explode('-',$date);
        return $date[2].'-'.$date[1].'-'.$date[0];
    }   
    
    /*
     * Converti une date simple FR en US
     * @param string (date)
     * @return string
     */
    static function dateFRtoUS($date){
        $date = explode('-',$date);
        return $date[2].'-'.$date[1].'-'.$date[0];
    } 
    
    /*
     * Max ID
     * @param string (nom table)
     * @param string (nom colonne)
     * @return int (max id)
     */
    static function maxID($table,$column,$where=''){
        if(!empty($where)){$where='WHERE '.$where;}
        return Db::getInstance()->getValue('SELECT max(`'.$column.'`) FROM `'.$table.'` '.$where);
    }
    
    /*
     * @param string (chaine à cleaner)
     * @return string (clean)
     */
    static public function cleanText($string,$force_utf8=true){
        
        // conversion UTF
        if($force_utf8){
            $string = Tools::htmlentitiesDecodeUTF8($string);
        }
            
        // cleanage du contenu en général
        $string = html_entity_decode($string);
        $string = strip_tags($string);
        $string = str_replace(CHR(13).CHR(10),"",$string); // enlève les retours chariot
        $string = preg_replace('/<br\\s*?\/??>/i','', $string);
        $string = trim($string);
        $string = str_replace("\r",'',$string);
        $string = str_replace("\n",'',$string);
        
        // remplacer des caractère déformé
        $char_spec = Configuration::get('SALESBOOSTER_CHAR_SPEC');
        if($char_spec){
            $string = str_replace('Ã©','é',$string);
            $string = str_replace('à‰','É',$string);
            $string = str_replace('â€™',"'",$string);
            $string = str_replace('Ã','à',$string);
            $string = str_replace('à¨','è',$string);
            $string = str_replace('àª','ê',$string);
            $string = str_replace('à§','ç',$string);
            $string = str_replace('à¢','â',$string);
            $string = str_replace('ï»¿','',$string);
            $string = str_replace('Â','',$string);
            $string = str_replace('â„¢','',$string);
            $string = str_replace("%&",'',$string);
            $string = str_replace('|',' ',$string);
        }
        
        return $string;
    }
    
    /* 
     * Retourne les valeurs d'un array (sans les clés)
     * @param string (nom de la clé)
     * @return array (tableau)
     */
    static function getArrayValues($key,array $arr){
        $vals = array();
        foreach($arr as $a){
            $vals[] = $a[$key];
        }
        return $vals;
    }
    
    /* 
     * Retourne les valeurs dupliquées d'un array
     * @param array (brut)
     * @return array (doublons)
     */
    static function getDuplicatesValues($array){
        $values_unique_not_empty = array();
        $values = @array_unique(array_diff_assoc($array,array_unique($array)));
        if(is_array($values)){
            foreach($values as $v){if(!empty($v)){$values_unique_not_empty[]=$v;}}
        }
        return $values_unique_not_empty;
    }
    
    /*
     * Copie un répertoire et tous ses fichiers
     * @param string (url source)
     * @param string (url dest)
     * @return array (statut)
     */
    static function copyFolder($src,$dst){
        $statut = 'success';
        $dir = opendir($src);
        @mkdir($dst);
        while(false !==($file=readdir($dir))){
            if (($file != '.')&&( $file != '..' )){
                if(is_dir($src.'/'.$file)){
                    ToolClass::copyFolder($src.'/'.$file,$dst.'/'.$file);
                }else{
                    if(!Tools::copy($src.'/'.$file,$dst .'/'.$file)){
                        $statut = 'error';
                    }
                }
            }
        }
        closedir($dir);
        return $statut;
    } 
    
}

?>