<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

/**
 * 
 * Return the spam score from the PostMark Spam Check.
 * @author Roger Thomas (http://www.rogerethomas.com).
 * @copyright NONE! It's free, do as you please with it :)
 *
 */
class postmarkSpamClass {
	
    public $_Email;
    public $_Option;

    private $_ValuesArray; // built in validate();

    private function validate(){
        /**
         * TODO: Handle this a bit more efficiently.
         */
        if ($this->_Email == "" || $this->_Option == "") {
                die('Required data missing.');
        } else {
                $this->_ValuesArray = array('email' => $this->_Email,'options' => $this->_Option);
        }
    }

    public function checkSpam()
    {
        $this->validate();
        // encode the data ready to be sent
        $json_data = Tools::jsonEncode($this->_ValuesArray);
        // set the headers in an array to be used by curl
        $http_headers = array("Accept: application/json","Content-Type: application/json");
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,"http://spamcheck.postmarkapp.com/filter");
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1); // return the data
        curl_setopt($ch,CURLOPT_CUSTOMREQUEST,"POST"); // we're doing a POST
        curl_setopt($ch,CURLOPT_POSTFIELDS,$json_data); // send the data in the json array
        curl_setopt($ch,CURLOPT_HTTPHEADER, $http_headers); // add the headers
        $result = curl_exec($ch); // run the curl
        if (curl_error($ch) != "") {
                /**
                 * TODO: Handle a nicer exception.
                 */
                die('Curl reported this error: '.curl_error($ch));
        }
        curl_close($ch); // close curl
        $result = Tools::jsonDecode($result,true); // decode the json data contained in the result
        $result['email'] = $this->_Email;
        return $result; // return result.
    }
	
}
