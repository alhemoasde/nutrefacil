<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

class CompatibilityClass{
 
    /**
     * Duplique un objet
     * @param object
     * @return new object
     */
    static public function duplicateObject($Object)
    {
            $definition = ObjectModel::getDefinition($Object);

            $res = Db::getInstance()->getRow('
                                    SELECT * 
                                    FROM `'._DB_PREFIX_.bqSQL($definition['table']).'`
                                    WHERE `'.bqSQL($definition['primary']).'` = '.pSQL($Object->id)
                            );
            if (!$res)
                    return false;
            unset($res[$definition['primary']]);
            foreach ($res as $field => &$value)
                    if (isset($definition['fields'][$field]))
                            $value = ObjectModel::formatValue($value, $definition['fields'][$field]['type']);

            if (!Db::getInstance()->insert($definition['table'], $res))
                    return false;

            $object_id = Db::getInstance()->Insert_ID();

            if (isset($definition['multilang']) && $definition['multilang'])
            {
                    $result = Db::getInstance()->executeS('
                    SELECT * 
                    FROM `'._DB_PREFIX_.bqSQL($definition['table']).'_lang`
                    WHERE `'.bqSQL($definition['primary']).'` = '.pSQL($Object->id));
                    if (!$result)
                            return false;

                    foreach ($result as &$row)
                            foreach ($row as $field => &$value)
                                    if (isset($definition['fields'][$field])){
                                        if($field==='content'){
                                            $value = pSQL($value,true);
                                        }else{
                                            $value = pSQL(ObjectModel::formatValue($value,$definition['fields'][$field]['type']),true);
                                        }
                                    }
                    // Keep $row2, you cannot use $row because there is an unexplicated conflict with the previous usage of this variable
                    foreach ($result as $row2)
                    {
                            $row2[$definition['primary']] = pSQL($object_id);
                            if (!Db::getInstance()->insert($definition['table'].'_lang', $row2))
                                    return false;
                    }
            }

            $object_duplicated = new $definition['classname']((int)$object_id);
            $object_duplicated->duplicateShops((int)$Object->id);

            return $object_duplicated;
    }
    
}

?>