<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

// V1
if(Tools::version_compare(_PS_VERSION_, '1.6.1.5','<')){
    include_once(_PS_SWIFT_DIR_.'Swift.php');
    include_once(_PS_SWIFT_DIR_.'Swift/Connection/SMTP.php');
    include_once(_PS_SWIFT_DIR_.'Swift/Connection/NativeMail.php');
    include_once(_PS_SWIFT_DIR_.'Swift/Plugin/Decorator.php');
// V2
}else{
    include_once(_PS_SWIFT_DIR_.'swift_required.php');
}

class MailClass
{

    /**
     * Send Email
     * 
     * @param int $id_lang Language of the email (to translate the template)
     * @param string $template Template: the name of template not be a var but a string !
     * @param string $subject
     * @param string $to
     * @param string $to_name
     * @param string $from
     * @param string $from_name
     * @param array $file_attachment Array with three parameters (content, mime and name). You can use an array of array to attach multiple files
     * @param bool $modeSMTP
     * @param bool $die
     */
    public static function Send($subject,$content,$to,$email_bounces=null,
            $to_name = null, $from = null, $from_name = null, $file_attachment = null, $mode_smtp = null, $die = false, $id_shop = null,$test_spam=false)
    {
        
            /* patch d'envoi pour configuration spéciales */
            /*
            $headers = "From: " .Configuration::get('PS_SHOP_NAME').' <'.Configuration::get('PS_SHOP_EMAIL') . '>'."\r\n";
            $headers .= "Reply-To: ". Configuration::get('PS_SHOP_EMAIL') . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            mail($to,$subject,$content,$headers);
            return;
            */
        
            // évite les mails invalides - espace suite à import etc...
            $to = trim($to);
            $configuration = Configuration::getMultiple(array(
                    'PS_SHOP_EMAIL',
                    'SUPERNEWS_SMTP_SERVER',
                    'SUPERNEWS_SMTP_USER',
                    'SUPERNEWS_SMTP_PASSWD',
                    'PS_SHOP_NAME',
                    'SUPERNEWS_SMTP_ENCRYPTION',
                    'SUPERNEWS_SMTP_PORT',
                    'SUPERNEWS_MAIL_METHOD',
                    'PS_MAIL_TYPE'
            ), null, null, $id_shop);

            // Returns immediatly if emails are deactivated
            if ($configuration['SUPERNEWS_MAIL_METHOD'] == 3)
                    return true;

            if (!isset($configuration['SUPERNEWS_SMTP_ENCRYPTION']))
                    $configuration['SUPERNEWS_SMTP_ENCRYPTION'] = 'off';
            if (!isset($configuration['SUPERNEWS_SMTP_PORT']))
                    $configuration['SUPERNEWS_SMTP_PORT'] = 'default';

            // Sending an e-mail can be of vital importance for the merchant, when his password is lost for example, so we must not die but do our best to send the e-mail
            if (!isset($from) || !Validate::isEmail($from))
                    $from = $configuration['PS_SHOP_EMAIL'];
            if (!Validate::isEmail($from))
                    $from = null;

            // $from_name is not that important, no need to die if it is not valid
            if (!isset($from_name) || !Validate::isMailName($from_name))
                    $from_name = $configuration['PS_SHOP_NAME'];
            if (!Validate::isMailName($from_name))
                    $from_name = null;

            // It would be difficult to send an e-mail if the e-mail is not valid, so this time we can die if there is a problem
            if (!is_array($to) && !Validate::isEmail($to))
            {
                    Tools::dieOrLog(Tools::displayError('Error: parameter "to" is corrupted'), $die);
                    return false;
            }

            // Do not crash for this error, that may be a complicated customer name
            if (is_string($to_name) && !empty($to_name) && !Validate::isMailName($to_name))
                    $to_name = null;
            
            /*
            if (!Validate::isMailSubject($subject))
            {
                    Tools::dieOrLog(Tools::displayError('Error: invalid e-mail subject'), $die);
                    return false;
            }
            */

            /* Construct multiple recipients list if needed */
            if (is_array($to) && isset($to))
            {
                    $to_list = new Swift_RecipientList();
                    foreach ($to as $key => $addr)
                    {
                            $to_name = null;
                            $addr = trim($addr);
                            if (!Validate::isEmail($addr))
                            {
                                    Tools::dieOrLog(Tools::displayError('Error: invalid e-mail address'), $die);
                                    return false;
                            }
                            if (is_array($to_name))
                            {
                                    if ($to_name && is_array($to_name) && Validate::isGenericName($to_name[$key]))
                                            $to_name = $to_name[$key];
                            }
                            if ($to_name == null)
                                    $to_name = $addr;
                            /* Encode accentuated chars */
                            $to_list->addTo($addr,$to_name);
                    }
                    $to_plugin = $to[0];
                    $to = $to_list;
            } else {
                    /* Simple recipient, one address */
                    $to_plugin = $to;
                    if ($to_name == null)
                            $to_name = $to;
                    
                    // V1
                    if(Tools::version_compare(_PS_VERSION_, '1.6.1.5','<')){
                        $to = new Swift_Address($to);
                    }
            }
            try {
                    /* Connect with the appropriate configuration */
                    if ($configuration['SUPERNEWS_MAIL_METHOD'] == 2)
                    {
                            if (empty($configuration['SUPERNEWS_SMTP_SERVER']) || empty($configuration['SUPERNEWS_SMTP_PORT']))
                            {
                                    Tools::dieOrLog(Tools::displayError('Error: invalid SMTP server or SMTP port'), $die);
                                    return false;
                            }
                                 
                            // var
                            $smtpServer = $configuration['SUPERNEWS_SMTP_SERVER'];
                            $smtpPort = $configuration['SUPERNEWS_SMTP_PORT'];
                            $smtpEncryption = $configuration['SUPERNEWS_SMTP_ENCRYPTION'];
                            $smtpLogin = Configuration::get('SUPERNEWS_SMTP_USER');
                            $smtpPassword = Configuration::get('SUPERNEWS_SMTP_PASSWD');   
                            
                            // config
                            // V1
                            if(Tools::version_compare(_PS_VERSION_, '1.6.1.5','<')){
                                
                                /*
                                $smtp = new Swift_Connection_SMTP($smtpServer, $smtpPort, ($smtpEncryption == 'off') ? 
                                Swift_Connection_SMTP::ENC_OFF : (($smtpEncryption == 'tls') ? Swift_Connection_SMTP::ENC_TLS : Swift_Connection_SMTP::ENC_SSL));
                                $smtp->setUsername($smtpLogin);
                                $smtp->setpassword($smtpPassword);
                                $smtp->setTimeout(5);
                                $swift = new Swift($smtp, Configuration::get('PS_MAIL_DOMAIN'));
                                */
                                
                                // Webbax 19.12.16 - révision code envoi SMTP
                                $smtp = new Swift_Connection_SMTP($smtpServer, $smtpPort, ($smtpEncryption == 'off') ? 
                                Swift_Connection_SMTP::ENC_OFF : (($smtpEncryption == 'tls') ? Swift_Connection_SMTP::ENC_TLS : Swift_Connection_SMTP::ENC_SSL));
                                $smtp->setUsername($smtpLogin);
                                $smtp->setpassword($smtpPassword);
                                $smtp->setTimeout(5);
                                $swift = new Swift($smtp);
                                $message = new Swift_Message($subject,$content,'text/html');
                                $result = false;
                                $from = Configuration::get('PS_SHOP_EMAIL');
                                if(!empty($email_sender)){$from = Configuration::get('SUPERNEWS_EMAIL_SENDER');}
                                if($swift->send($message,$to,$from)){
                                    $result = true;
                                }
                                $swift->disconnect();
                                return $result;
                                
                            // V2
                            }else{
                                if(Tools::strtolower($smtpEncryption)==='off'){$smtpEncryption = false;}
                                $connection = Swift_SmtpTransport::newInstance($smtpServer,$smtpPort,$smtpEncryption)
                                              ->setUsername($smtpLogin)
                                              ->setPassword($smtpPassword);
                            }
                            
                    }else{
                            
                        // V1
                        if(Tools::version_compare(_PS_VERSION_, '1.6.1.5','<')){
                            $connection = new Swift_Connection_NativeMail();
                            $swift = new Swift($connection);
                        // V2
                        }else{
                            $connection = Swift_MailTransport::newInstance();
                        }
                    }        
                    
                            
                    if (!$connection)
                            return false;
                    
                    
                    if ($configuration['SUPERNEWS_MAIL_METHOD']==2){
                        $mail_domain = Configuration::get('SUPERNEWS_MAIL_DOMAIN',null,null,$id_shop);
                    }else{
                        $mail_domain = Configuration::get('PS_MAIL_DOMAIN', null, null, $id_shop);
                    }
                    
                    /*
                    if($test_spam==true){
                        // V1
                        if(Tools::version_compare(_PS_VERSION_, '1.6.1.5','<')){
                            $swift = new Swift($connection,$mail_domain);
                        // V2
                        }else{
                            $connection = Swift_MailTransport::newInstance();
                        }
                    }
                    */
                    
                    // V1 
                    if(Tools::version_compare(_PS_VERSION_, '1.6.1.5','<')){
                        /* Create mail and attach differents parts */
                        $message = new Swift_Message('['.Configuration::get('PS_SHOP_NAME', null, null, $id_shop).'] '.$subject);

                        /* Set Message-ID - getmypid() is blocked on some hosting */
                        $message->setId(MailClass::generateId());

                        $message->headers->setEncoding('Q');
                        $message->setBody($content);
                        $message->setContentType('text/html');

                        // propose le retour d'erreurs "bounces" sur une adresse spécifique
                        if(!empty($email_bounces)){
                            $message->setReturnPath($email_bounces);
                        }

                        if ($file_attachment && !empty($file_attachment))
                        {
                                // Multiple attachments?
                                if (!is_array(current($file_attachment)))
                                        $file_attachment = array($file_attachment);

                                foreach ($file_attachment as $attachment)
                                        if (isset($attachment['content']) && isset($attachment['name']) && isset($attachment['mime']))
                                                $message->attach(new Swift_Message_Attachment($attachment['content'], $attachment['name'], $attachment['mime']));
                        }
                    }
                        
                    // retourne le raw d'un mail de test avec header
                    if($test_spam){
                        // V1 
                        if(Tools::version_compare(_PS_VERSION_, '1.6.1.5','<')){
                            $message->setTo(Configuration::get('PS_SHOP_EMAIL'));
                            $message->setFrom(Configuration::get('PS_SHOP_EMAIL'));
                            $message->setReplyTo(Configuration::get('PS_SHOP_EMAIL'));
                            return $message->headers->getList();
                        // V2
                        }else{
                            $message = new Swift_Message($subject,$content,'text/html');  
                            $message->setTo(Configuration::get('PS_SHOP_EMAIL'));
                            $message->setFrom(Configuration::get('PS_SHOP_EMAIL'));
                            $message->setReplyTo(Configuration::get('PS_SHOP_EMAIL'));   
                            $headers = $message->getHeaders();
                            return $headers->toString();
                        }
                    }

                    /* Send mail */
                    // V1 
                    if(Tools::version_compare(_PS_VERSION_, '1.6.1.5','<')){
                        $send = $swift->send($message,$to,new Swift_Address($from,$from_name));
                        $swift->disconnect();
                        return $send;
                    // V2
                    }else{
                        $swift = Swift_Mailer::newInstance($connection);
                        $message = new Swift_Message($subject,$content,'text/html');  
                        $message->setFrom(array(Configuration::get('PS_SHOP_EMAIL') => Configuration::get('PS_SHOP_NAME')));
                        $message->setTo($to);                  
                        return $swift->send($message);
                    }

            }catch(Swift_Exception $e){
                    return false;
            }
    }

    /* Rewrite of Swift_Message::generateId() without getmypid() */
    protected static function generateId($idstring = null)
    {
            $midparams =  array(
                    "utctime" => gmstrftime("%Y%m%d%H%M%S"),
                    "randint" => mt_rand(),
                    "customstr" => (preg_match("/^(?<!\\.)[a-z0-9\\.]+(?!\\.)\$/iD", $idstring) ? $idstring : "swift") ,
                    "hostname" => (isset($_SERVER["SERVER_NAME"]) ? $_SERVER["SERVER_NAME"] : php_uname("n")),
            );
            return vsprintf("<%s.%d.%s@%s>", $midparams);
    }
    
}

?>
