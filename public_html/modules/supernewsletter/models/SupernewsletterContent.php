<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

class SupernewsletterContent extends ObjectModel
{
    
	public $id;
        public $date;
        public $cron;
        public $id_supernewsletter_template;
        public $id_lang_default;
        public $id_group_price;
        public $id_currency;
        public $ids_groups;
        public $cust_disabled;
        public $cust_no_news;
        public $cust_deleted;          
        public $id_country;
        public $register_front;
        public $groups;
        public $products;
        public $statut;
        public $emails_sent;
        public $title;
	public $content;
        public $id_shop;
        
	public static $definition = array(
            'table' => 'supernewsletter_content',
            'primary' => 'id_supernewsletter_content',
            'multilang' => true,
            'fields' => array(
                               'date' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
                               'cron' => array('type' => self::TYPE_BOOL),
                               'id_supernewsletter_template' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt','required' => true),
                               'id_lang_default' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                               'id_group_price' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                               'id_currency' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                               'ids_groups' => array('type' => self::TYPE_STRING),
                               'cust_disabled' => array('type' => self::TYPE_BOOL),
                               'cust_no_news' => array('type' => self::TYPE_BOOL),
                               'cust_deleted' => array('type' => self::TYPE_BOOL),
                               'id_country' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                               'register_front' => array('type' => self::TYPE_BOOL),
                               'groups' => array('type' => self::TYPE_STRING),
                               'products' => array('type' => self::TYPE_STRING),
                               'statut' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                               'emails_sent' => array('type' => self::TYPE_HTML),
                               'title' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName','required' => true),
                               'content' => array('type' => self::TYPE_HTML, 'lang' => true),
                               'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
                              )
	);

	public function copyFromPost()
	{
            /* Classical fields */
            foreach($_POST AS $key => $value)
                if(key_exists($key, $this) AND $key != 'id_'.$this->table)
                    $this->{$key} = $value;

            /* Multilingual fields */
            if (sizeof($this->fieldsValidateLang)){
                $languages = Language::getLanguages(false);
                foreach ($languages AS $language)
                    foreach ($this->fieldsValidateLang AS $field => $validation)
                        if (Tools::getIsset($field.'_'.(int)($language['id_lang'])))
                            $this->{$field}[(int)($language['id_lang'])] = Tools::getValue($field.'_'.(int)($language['id_lang']));
            }
	}
        
        /*
         * Liste des newsletters
         * @param  -
         * @return array
         */
        static function getNewsletters(){
            $context = Context::getContext(); 
            $cookie = $context->cookie;
            $sql = 'SELECT * FROM '._DB_PREFIX_.'supernewsletter_content sc
            LEFT JOIN `'._DB_PREFIX_.'supernewsletter_content_lang` scl ON (sc.`id_supernewsletter_content` = scl.`id_supernewsletter_content`) 
            WHERE sc.`id_shop`='.pSQL($context->shop->id).' AND scl.`id_lang`='.pSQL($cookie->id_lang).'
            ORDER BY sc.`id_supernewsletter_content` DESC';
            return Db::getInstance()->ExecuteS($sql);   
        }
    
}
