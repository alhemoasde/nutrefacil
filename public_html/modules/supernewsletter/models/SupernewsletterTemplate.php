<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

class SupernewsletterTemplate extends ObjectModel
{
    
	public $id;
        public $name;
        public $width;
        public $bg_body;
        public $link_img_header;
        public $logo;
        public $logo_position;
        public $link_img_headerproducts;
        
        public $product_img_size;
        public $product_title_len;
        public $product_desc_type;
        public $product_desc_len;
        public $product_display_price;
           
        public $link_facebook;
        public $link_twitter;
        public $link_rss;
        public $link_img_footer;
        
        public $signing;
        public $police;
        public $police_size;
        public $bg_newsletter;
        public $bg_product;
        
        public $col_content;
        public $size_content;
        public $bold_content;
        public $bdr_content;
        public $bdr_width_content;
        
        public $bdr_block_products;
        public $bdr_width_block_products;
        
        public $col_separator_product;
        public $bdr_width_separator_product;
        
        public $bdr_img_product;
        public $bdr_width_img_product;
        
        public $col_title_product;
        public $size_title_product;
        public $bold_title_product;
        
        public $col_desc_product;
        public $size_desc_product;
        public $bold_desc_product;
        
        public $col_pricelabel_product;
        public $size_pricelabel_product;
        public $bold_pricelabel_product;
        public $col_price_product;
        public $size_price_product;
        public $bold_price_product;
        
        public $col_promo_product;
        public $size_promo_product;
        public $bold_promo_product;
        
        public $bg_btn_view_product;
        public $col_btn_view_product;
        public $size_btn_view_product;
        public $bold_btn_view_product;
        public $bdr_btn_view_product;
        public $bdr_width_btn_view_product;
        public $pad_lft_btn_view_product;
        
        public $col_links_hf;
        public $size_links_hf;
        
	public static $definition = array(
            'table' => 'supernewsletter_template',
            'primary' => 'id_supernewsletter_template',
            'multilang' => true,
            'fields' => array(
                               'name' => array('type' => self::TYPE_STRING, 'name' => true,'required' => true),
                               'width' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                               'bg_body' => array('type' => self::TYPE_STRING,'required' => true),
                               'link_img_header' => array('type' => self::TYPE_STRING),
                               'logo' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
                               'logo_position' => array('type' => self::TYPE_STRING),
                               'link_img_headerproducts' => array('type' => self::TYPE_STRING),
                               
                               'product_img_size' => array('type' => self::TYPE_STRING,'required' => true),
                               'product_title_len' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                               'product_desc_type' => array('type' => self::TYPE_STRING,'required' => true),
                               'product_desc_len' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                               'product_display_price' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
                              
                               'link_facebook' => array('type' => self::TYPE_STRING),
                               'link_twitter' => array('type' => self::TYPE_STRING),
                               'link_rss' => array('type' => self::TYPE_STRING),
                               'link_img_footer' => array('type' => self::TYPE_STRING), 
                
                               'signing' => array('type' => self::TYPE_HTML,'lang' => true),
                               'police' => array('type' => self::TYPE_STRING,'required' => true),
                               'police_size' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt','required' => true),
                               'bg_newsletter' => array('type' => self::TYPE_STRING,'required' => true),
                               'bg_product' => array('type' => self::TYPE_STRING,'required' => true),
                               
                               'col_content' => array('type' => self::TYPE_STRING,'required' => true),
                               'size_content' => array('type' => self::TYPE_INT),
                               'bold_content' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
                               'bdr_content' => array('type' => self::TYPE_STRING,'required' => true),
                               'bdr_width_content' => array('type' => self::TYPE_INT),

                               'bdr_block_products' => array('type' => self::TYPE_STRING,'required' => true),
                               'bdr_width_block_products' => array('type' => self::TYPE_INT),
                               
                               'col_separator_product' => array('type' => self::TYPE_STRING,'required' => true),
                               'bdr_width_separator_product' => array('type' => self::TYPE_INT),
                               
                               'bdr_img_product' => array('type' => self::TYPE_STRING,'required' => true),
                               'bdr_width_img_product' => array('type' => self::TYPE_INT),
                               
                               'col_title_product' => array('type' => self::TYPE_STRING,'required' => true),
                               'size_title_product' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt','required' => true),
                               'bold_title_product' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
                
                               'col_desc_product' => array('type' => self::TYPE_STRING,'required' => true),
                               'size_desc_product' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt','required' => true),
                               'bold_desc_product' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
                
                               'col_pricelabel_product' => array('type' => self::TYPE_STRING,'required' => true),
                               'size_pricelabel_product' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt','required' => true),
                               'bold_pricelabel_product' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
                               'col_price_product' => array('type' => self::TYPE_STRING,'required' => true),
                               'size_price_product' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt','required' => true),
                               'bold_price_product' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
                
                               'col_promo_product' => array('type' => self::TYPE_STRING,'required' => true),
                               'size_promo_product' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt','required' => true),
                               'bold_promo_product' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
                               
                               'bg_btn_view_product' => array('type' => self::TYPE_STRING,'required' => true),
                               'col_btn_view_product' => array('type' => self::TYPE_STRING,'required' => true),
                               'size_btn_view_product' => array('type' => self::TYPE_STRING,'required' => true),
                               'bold_btn_view_product' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
                               'bdr_btn_view_product' => array('type' => self::TYPE_STRING,'required' => true),
                               'bdr_width_btn_view_product' => array('type' => self::TYPE_STRING,'required' => true),
                               'pad_lft_btn_view_product' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                               
                               'col_links_hf' => array('type' => self::TYPE_STRING,'required' => true),
                               'size_links_hf' => array('type' => self::TYPE_STRING,'required' => true),          
                              )
	);

	public function copyFromPost()
	{
            /* Classical fields */
            foreach($_POST AS $key => $value)
                if(key_exists($key, $this) AND $key != 'id_'.$this->table)
                    $this->{$key} = $value;

            /* Multilingual fields */
            if (sizeof($this->fieldsValidateLang)){
                $languages = Language::getLanguages(false);
                foreach ($languages AS $language)
                    foreach ($this->fieldsValidateLang AS $field => $validation)
                        if(Tools::getIsset(Tools::getValue($field.'_'.(int)($language['id_lang']))))
                            $this->{$field}[(int)($language['id_lang'])] = Tools::getValue($field.'_'.(int)($language['id_lang']));
            }
	}
        
        /*
         * Retourne la liste des templates
         * @param - 
         * @return array
         */
        static function getTemplates(){
            $context = Context::getContext(); 
            return Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'supernewsletter_template sc ORDER BY `name` ASC');  
        }

}