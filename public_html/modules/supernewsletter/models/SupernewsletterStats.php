<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

class SupernewsletterStats extends ObjectModel
{
    
	public $id;
        public $open_ip;
        public $redirect_ip;
        public $clicked_products;
        public $clicked_links;
        public $nb_unsubscribe;
        public $emails_unsubscribe;
        
	public static $definition = array(
            'table' => 'supernewsletter_stats',
            'primary' => 'id_supernewsletter_content',
            'multilang' => false,
            'fields' => array(
                               'open_ip' => array('type' => self::TYPE_HTML,'validate' => 'isString'),
                               'redirect_ip' => array('type' => self::TYPE_HTML,'validate' => 'isString'),
                               'clicked_products' => array('type' => self::TYPE_HTML,'validate' => 'isString'),
                               'clicked_links' => array('type' => self::TYPE_HTML,'validate' => 'isString'),
                               'nb_unsubscribe' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt'),
                               'emails_unsubscribe' => array('type' => self::TYPE_HTML,'validate' => 'isString'),
                              )
	);

	public function copyFromPost()
	{
            /* Classical fields */
            foreach($_POST AS $key => $value)
               if(key_exists($key, $this) AND $key != 'id_'.$this->table)
                    $this->{$key} = $value;

            /* Multilingual fields */
            if (sizeof($this->fieldsValidateLang)){
                $languages = Language::getLanguages(false);
                foreach ($languages AS $language)
                    foreach ($this->fieldsValidateLang AS $field => $validation)
                        if (Tools::getIsset(Tools::getValue($field.'_'.(int)($language['id_lang']))))
                            $this->{$field}[(int)($language['id_lang'])] = Tools::getValue($field.'_'.(int)($language['id_lang']));
            }
	}
    
}
