<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

class SupernewsletterEmail extends ObjectModel
{
    
	public $id;
        public $email;
        public $firstname;
        public $lastname;
        public $group;
        public $date_add;
        public $products;
        public $id_lang;
        public $id_shop;
        
	public static $definition = array(
            'table' => 'supernewsletter_email',
            'primary' => 'id_supernewsletter_email',
            'fields' => array(
                               'email' => array('type' => self::TYPE_STRING, 'validate' => 'isString', 'required' => true),
                               'firstname' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                               'lastname' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                               'group' => array('type' => self::TYPE_STRING, 'validate' => 'isString'),
                               'date_add' => array('type' => self::TYPE_DATE, 'validate' => 'isDateFormat'),
                               'id_lang' => array('type' => self::TYPE_INT),
                               'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
                              )
	);

	public function copyFromPost()
	{
            /* Classical fields */
            foreach($_POST AS $key => $value)
                if(key_exists($key, $this) AND $key != 'id_'.$this->table)
                    $this->{$key} = $value;

            /* Multilingual fields */
            if (sizeof($this->fieldsValidateLang)){
                $languages = Language::getLanguages(false);
                foreach ($languages AS $language)
                    foreach ($this->fieldsValidateLang AS $field => $validation)
                        if (Tools::getIsset($field.'_'.(int)($language['id_lang'])))
                            $this->{$field}[(int)($language['id_lang'])] = Tools::getValue($field.'_'.(int)($language['id_lang']));
            }
	}
        
        /*
         * Retourne la liste des emails
         * @param - 
         * @return array
         */
        static function getEmails(){
            $context = Context::getContext(); 
            return Db::getInstance()->ExecuteS('SELECT * FROM '._DB_PREFIX_.'supernewsletter_email sc WHERE sc.`id_shop`='.pSQL($context->shop->id));  
        }
        
        /*
         * Retourne la liste des groupes importés
         * @param bool (compter les enregistrement)
         * @return array
         */
        static function getGroups($countEmail=true){
            // liste des groupes clients
            $groups_email = array();
            $groups = Db::getInstance()->ExecuteS('SELECT `group` FROM `'._DB_PREFIX_.'supernewsletter_email` WHERE `group`!="no_group" GROUP BY `group`');
            foreach($groups as $k=>$v){
                $group = $v['group'];
                $name = $v['group'];
                if($countEmail){
                    $nbre = Db::getInstance()->getValue('SELECT count(`id_supernewsletter_email`) FROM `'._DB_PREFIX_.'supernewsletter_email` WHERE `group`="'.pSQL($v['group']).'" AND  `group`!=""');
                    $name .= ' ('.$nbre.')';
                }else{
                    $name .= '';
                }
                if($nbre>0){
                    $groups_email[$k] = array('group'=>$group,'name'=>$name,'nbre'=>$nbre);
                }
            }
                     
            if($countEmail){
                $nbre = Db::getInstance()->getValue('SELECT count(`id_supernewsletter_email`) FROM `'._DB_PREFIX_.'supernewsletter_email` WHERE `group`="no_group"');
                if($countEmail){$nbre = '('.$nbre.')';}else{$nbre='';}
            }
            
            $name = Tools::strtolower(self::SupernewsletterEmailTrans('aucun'));
            $groups_email[] = array('group'=>'no_group','name'=>self::SupernewsletterEmailTrans('aucun').' '.$nbre,'nbre'=>$nbre,);
            
            $groups_email = array_reverse($groups_email);
            return $groups_email;
        }
        
        /*
        * Gestion des traductions de la classe
        * @param string (code traduction)
        * @return string (traduction dans la langue courante)
        */
       static function SupernewsletterEmailTrans($string){
           $t=array(
               'aucun'=>'Aucun',
           );
           return $t[$string];
       }
        
        
}
