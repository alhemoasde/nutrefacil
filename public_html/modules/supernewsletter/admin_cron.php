<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

if(!defined('_COOKIE_KEY_')){die('token error');}

$helper = new HelperForm();
// Toolbar
$helper->toolbar_btn['back'] =  array(
    'href' => $current_index,
    'desc' => $this->l('Revenir à la liste des newsletter',$page_name),
    'icon' => 'icon-arrow-left',
);
if($this->ps_version==1.6){$toolbarModule=$this->toolbarModule($helper->toolbar_btn);}else{$toolbarModule='';}

foreach($languages as $k => $language){$languages[$k]['is_default'] = (int)($language['id_lang'] == Configuration::get('PS_LANG_DEFAULT'));}

$helper->module = $this;
$helper->name_controller = 'supernewsletter';
$helper->identifier = $this->identifier;
$helper->token = Tools::getAdminTokenLite('AdminModules');
$helper->languages = $languages;
$helper->currentIndex = $_SERVER['REQUEST_URI'];
$helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
$helper->allow_employee_form_lang = true;
$helper->toolbar_scroll = true;
$helper->toolbar_btn = $helper->toolbar_btn;
$helper->title = $this->l('Tâches planifiées',$page_name);
$helper->submit_action = 'submitCron';

// Jquery Date
$this->context->controller->addJqueryUI('ui.datepicker');
$this->context->controller->addJqueryUI('ui.autocomplete' );
$this->_html.='
<script type="text/javascript">
$(document).ready(function(){  
    $("#date").datepicker({dateFormat:"dd-mm-yy"});   
});
</script>';

$this->fields_form[0]['form'] = array(
    'tinymce' => true,
    'legend' => array(
            'title' => $this->l('Cron',$page_name),
            'image' => $this->_path.'views/img/time.png'
    ),
    'input' => array(
            array(
                'type' => 'free',
                'label' => '',
                'name' => 'infos',
            ),
            array(
                'type' => 'text',
                'label' => $this->l('Date d\'envoi',$page_name),
                'name' => 'date',
                'size' => 10,
            ),
    ),
    'submit' => array(
            'name' => 'submitDate',
            'title' => $this->l('Rechercher',$page_name),
            'class' => 'button'
    ),
);

// remplissage des champs classiques
if(!Tools::getIsset('date')){$helper->fields_value['date']=date('d-m-Y');}else{$helper->fields_value['date']=Tools::getValue('date');}

$infos = '';

// submit
if(Tools::isSubmit('submitDate')){ 
    
     $sql = 'SELECT `id_supernewsletter_content` FROM `'._DB_PREFIX_.'supernewsletter_content` WHERE `date`="'.pSQL(ToolClass::dateFRtoUS(Tools::getValue('date'))).'" AND `id_shop`="'.pSQL($id_shop).'" ORDER BY `id_supernewsletter_content` DESC';
     $id_supernewsletter_content = Db::getInstance()->getValue($sql);
     
     if(empty($id_supernewsletter_content)){
         $results = '- '.$this->l('Aucune newsletter à envoyer pour cette date',$page_name);
     }else{
         $SupernewsletterContent = new SupernewsletterContent($id_supernewsletter_content);
         $results = '
         <table class="table" id="newsletters">
           <tr>
               <th>'.$this->l('Date',$page_name).'</th>
               <th>'.$this->l('Titre',$page_name).'</th>
               <th>'.$this->l('Cron activé',$page_name).'</th>    
           </tr>
           <tr>
               <td>'.$SupernewsletterContent->date.'</td>
               <td>'.$SupernewsletterContent->title[$id_lang].'</td>
               <td>';
                  if($SupernewsletterContent->cron){
                      $results.= '<img src="'.$this->_path.'views/img/ok.png" /> '.$this->l('Oui',$page_name);
                  }else{
                      $results.= '<img src="'.$this->_path.'views/img/cancel.png" /> '.$this->l('Non',$page_name);
                  }
               $results.='
               </td>
           </tr>
        </table>';
     }
     $find_newsletter = '
     <table class="table">
        <tr><th>'.$this->l('Résultats',$page_name).'</th></tr>
        <tr><td id="results_search">'.$results.'</td></tr>
     </table>';
}

// fonctionnement
$infos .= '
<div id="infos_cron" class="hint info alert alert-info">'.
    $this->l('La tâche cron permet d\'envoyer automatiquement la newsletter du jour.',$page_name).' '.
    $this->l('Vous devez créer une tâche CRON journalière sur votre hébergement, libre à vous de définir l\'horaire à laquelle elle devra s\'exécuter',$page_name).' :<br/>
    <br/>
    <img src="'.$this->_path.'views/img/script_link.png" /> <span class="label_url_cron">'.$this->l('URL CRON',$page_name).'</span> : <span class="url_cron">'.$Shop->getBaseURL().'modules/'.$this->name.'/front_cron_send.php?identifier=date&identifier_value=date&emails_pack=unlimited&id_shop='.$this->context->shop->id.'&token='._COOKIE_KEY_.'</span><br/>
    <br/>'.
    $this->l('Il est recommandé d\'adapter le temps d\'exécution sur votre serveur pour que le script ne soit pas interrompu durant l\'envoi, cela varie en fonction de votre volume d\'emails.',$page_name).
    $this->l('Plus votre volume est important, plus il faudra de mémoire et de temps au script pour réaliser l\'opération.',$page_name).'<br/>
    <br/>'.
    $this->l('Temps d\'exécution maximal (par défaut) de votre serveur',$page_name).' : <strong>'.ini_get('max_execution_time').' '.$this->l('secondes',$page_name).'</strong><br/>'.
    $this->l('Mémoire  maximale (par défaut) de votre serveur',$page_name).' : <strong>'.ini_get('memory_limit').'</strong><br/>
    <br/>'.
    $this->l('Vous pouvez forcer ces valeurs dans',$page_name).' <a href="'.$current_index.'&configuration" class="link"><img src="'.$this->_path.'views/img/edit.png" /> '.$this->l('la configuration avancée',$page_name).'</a><br/>
 </div>';
$helper->fields_value['infos'] = $infos.'<br/>'.(!empty($find_newsletter)?$find_newsletter:'');

$this->_html .= $toolbarModule.$helper->generateForm($this->fields_form);

?>
