<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

if(!defined('_COOKIE_KEY_')){die('token error');}

$this->fields_list = 
array(
    'id_supernewsletter_content' => array(
            'title' => $this->l('ID',$page_name),
            'width' => 120,
            'type' => 'text',
    ),
    'date' => array(
            'title' => $this->l('Date',$page_name),
            'width' => 140,
            'type' => 'text',
            'callback' => 'admin_home_colDate',
            'callback_object' => $this,
    ),
    'title' => array(
            'title' => $this->l('Titre',$page_name),
            'width' => 140,
            'type' => 'text',
    ),
    'statut' => array(
            'title' => $this->l('Statut',$page_name),
            'width' => 140,
            'type' => 'text',
            'callback' => 'admin_home_colStatut', 
            'callback_object' => $this,
    ),
    'cron' => array(
            'title' => $this->l('Tâche planifiée',$page_name),
            'width' => 140,
            'type' => 'text',
            'callback' => 'admin_home_colCron', 
            'callback_object' => $this,
    ),
 );

$helper = new HelperList();


$helper->toolbar_btn['new'] = array(
        'href' => $current_index.'&addsupernewsletter',
        'desc' => $this->l('Ajouter une newsletter',$page_name),
        'icon' => 'icon-plus-sign'
);
$helper->toolbar_btn['templates'] = array(
        'href' => $current_index.'&templates',
        'desc' => $this->l('Gestion des thèmes',$page_name),
        'icon' => 'icon-picture'
);
$helper->toolbar_btn['sending'] = array(
        'href' => $current_index.'&sending',
        'desc' => $this->l('Gestion des envois',$page_name),
        'icon' => 'icon-bullhorn'
);
$helper->toolbar_btn['export-csv-details'] = array(
        'href' => $current_index.'&importemails',
        'desc' => $this->l('Importer des adresses email',$page_name),
        'icon' => 'icon-filter'
);
$helper->toolbar_btn['stats'] = array(
        'href' => $current_index.'&stats',
        'desc' => $this->l('Statistiques',$page_name),
        'icon' => 'icon-signal'
);
$helper->toolbar_btn['configuration'] = array(
        'href' => $current_index.'&configuration',
        'desc' => $this->l('Configuration avancée',$page_name),
        'icon' => 'icon-cog'
);
$helper->toolbar_btn['cron'] = array(
        'href' => $current_index.'&cron',
        'desc' => $this->l('Tâche planifiée',$page_name),
        'icon' => 'icon-time'
);

$helper->shopLinkType = '';
$helper->simple_header = true;
$helper->identifier = 'id_supernewsletter_content';
$helper->actions = array('edit','duplicate','delete');
$helper->show_toolbar = true;
$helper->imageType = 'jpg';
$helper->title = $this->l('Liste des newsletters',$page_name);
$helper->table = $this->name;
$helper->token = Tools::getAdminTokenLite('AdminModules');
$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

if($this->ps_version==1.6){$this->_html.=$this->toolbarModule($helper);}

// dupliquer la newsletter
if(Tools::isSubmit('duplicatesupernewsletter')){

    // content + lang
    $SupernewsletterContent = new SupernewsletterContent(Tools::getValue('id_supernewsletter_content'));
    $O = CompatibilityClass::duplicateObject($SupernewsletterContent);
  
    $data = array('cron'=>0,'statut'=>0,'emails_sent'=>'');
    Db::getInstance()->autoExecute(_DB_PREFIX_.'supernewsletter_content',$data,'UPDATE','id_supernewsletter_content='.pSQL($O->id));
    foreach($languages as $l){
       $data = array('title'=>pSQL($this->l('Copie de',$page_name)).' '.pSQL($O->title[$l['id_lang']]));
       Db::getInstance()->autoExecute(_DB_PREFIX_.'supernewsletter_content_lang',$data,'UPDATE','id_supernewsletter_content='.pSQL($O->id).' AND id_lang='.pSQL($l['id_lang']));
    }
    // stats
    $data = array('id_supernewsletter_content'=>pSQL($O->id));
    Db::getInstance()->autoExecute(_DB_PREFIX_.'supernewsletter_stats',$data,'INSERT');  
    $this->_html .= $this->displayConfirmation('La newsletter a été dupliquée');
}

// suppression newsletter 
if(Tools::isSubmit('deletesupernewsletter')){
    $id = Tools::getValue('id_supernewsletter_content');
    $SupernewsletterContent = new SupernewsletterContent($id);
    $SupernewsletterContent->delete();
    $SupernewsletterStats = new SupernewsletterStats($id);
    $SupernewsletterStats->delete();
    $this->_html .= $this->displayConfirmation('Suppression effectuée');
}

$news = SupernewsletterContent::getNewsletters();
$this->_html .= $helper->generateList($news,$this->fields_list);

?>
