<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

if(!defined('_COOKIE_KEY_')){die('token error');}

// soumission du formulaire
if(Tools::isSubmit('submitEditNewsletter') || Tools::isSubmit('submitEditNewsletterAndStay')){
    
    // Contrôle du formulaire
    $title = Tools::getValue('title_'.$id_lang);
    if(empty($title)){
        $this->_errors[] = $this->l('Veuillez indiquer le titre',$page_name); 
    }
    $date = Tools::getValue('date');
    if(!Validate::isDateFormat($date)){
        $this->_errors[] = $this->l('Veuillez indiquer la date',$page_name); 
    }
    $id_supernewsletter_template = Tools::getValue('id_supernewsletter_template');
    if(empty($id_supernewsletter_template)){
        $this->_errors[] = $this->l('Veuillez sélectionner le thème graphique',$page_name); 
    }
    $id_lang_default = Tools::getValue('id_lang_default');
    if($id_lang_default==''){
        $this->_errors[] = $this->l('Veuillez indiquer la langue',$page_name); 
    }
    if(count($this->_errors)){
        foreach($this->_errors as $err){
            $this->_html .= '<div class="alert error module_error alert-danger">'.$err.'</div>';
        }
    }
    
    // Si le formulaire est ok
    if(empty($this->_errors)){
        
        $id = Tools::getValue('id_supernewsletter_content');
        // add newsletter
        if(empty($id)){
            $SupernewsletterContent = new SupernewsletterContent();
        // update newsletter
        }else{
            $SupernewsletterContent = new SupernewsletterContent($id);
        }
        
        // id_shop
        $SupernewsletterContent->id_shop = $id_shop;
        // title
        foreach($languages as $k => $language){
            $SupernewsletterContent->title[$language['id_lang']] = Tools::getValue('title_'.$language['id_lang']);
        }

        $SupernewsletterContent->date = Tools::getValue('date');  
        $SupernewsletterContent->cron = Tools::getValue('cron');  
        $SupernewsletterContent->id_supernewsletter_template = Tools::getValue('id_supernewsletter_template');
        $SupernewsletterContent->id_lang_default = Tools::getValue('id_lang_default');
        $SupernewsletterContent->id_group_price = Tools::getValue('id_group_price');
        $SupernewsletterContent->id_currency = Tools::getValue('id_currency');
        
        // groupes clients
        $ids_groups = array();
        $groups = Group::getGroups($cookie->id_lang);
        foreach($groups as $g){
            $field_name = 'ids_groups_'.str_replace(' ','_',trim($g['id_group']));
            $value = Tools::getValue($field_name);
            if($value=='on'){$id_group=$g['id_group'];}else{$id_group=0;}
            if($id_group!=0){$ids_groups[] = $id_group;}
        }
        $SupernewsletterContent->ids_groups = serialize($ids_groups);
        
        // clients désactivés
        $SupernewsletterContent->cust_disabled = Tools::getValue('cust_disabled');
        // clients non inscrits à la news
        $SupernewsletterContent->cust_no_news = Tools::getValue('cust_no_news');
        // clients supprimés
        $SupernewsletterContent->cust_deleted = Tools::getValue('cust_deleted');
        
        // Pays
        $SupernewsletterContent->id_country = Tools::getValue('id_country');
        
        // register_front
        $SupernewsletterContent->register_front=Tools::getValue('register_front');
        
        // groupes emails importés
        $groups = array();
        $groups_email = SupernewsletterEmail::getGroups();
        foreach($groups_email as $g){
            $field_name = 'groups_'.str_replace(' ','_',trim($g['group'])); 
            $value = Tools::getValue($field_name);
            $group = '';
            if($value=='on'){$group=$g['group'];}
            if(isset($group) && $group!==''){$groups[] = $group;}
        }
        $SupernewsletterContent->groups = serialize($groups);   
        
        // content
        foreach($languages as $k => $language){
            $SupernewsletterContent->content[$language['id_lang']] = Tools::getValue('content_'.$language['id_lang']);
        }
        
        // sauve les produits
        $id_product_lst = Tools::getValue('id_product_lst');
        $id_product_attribute_lst = Tools::getValue('id_product_attribute_lst');
        $products = array();
        if(empty($id_product_lst)){$id_product_lst=array();}
        foreach($id_product_lst as $k=>$id_product){
            $products[] = array('id_product'=>$id_product,
                                'id_product_attribute'=>$id_product_attribute_lst[$k]);
        }
        $SupernewsletterContent->products = serialize($products);
        // --
        
        // add newsletter
        if(empty($id)){
            
            $SupernewsletterContent->add();
            $id = ToolClass::maxID(_DB_PREFIX_.'supernewsletter_content','id_supernewsletter_content');
            
            // ajout de la satistique à vide
            Db::getInstance()->insert('supernewsletter_stats',array('id_supernewsletter_content'=>pSQL($id)));
            
            if(Tools::isSubmit('submitEditNewsletterAndStay') || $this->ps_version==1.6){
                $url = $current_index.'&updatesupernewsletter&id_supernewsletter_content='.$id.'&msgtype=1';
            }else{
                $url = $current_index.'&msgtype=1';
            }
        // update newsletter
        }else{
            $SupernewsletterContent->update(); 
            if(Tools::isSubmit('submitEditNewsletterAndStay') || $this->ps_version==1.6){
                $url = $current_index.'&updatesupernewsletter&id_supernewsletter_content='.$id.'&msgtype=2';
            }else{
                $url = $current_index.'&msgtype=2';
            }
        }
        
        if(empty($this->_errors)){
            Tools::redirectAdmin($url);
        }
    }
}

// inclusion de la librairie
$this->context->controller->addJqueryUI('ui.sortable');

// Toolbar
$this->toolbar_btn['back'] =  array(
    'href' => $current_index,
    'desc' => $this->l('Revenir à la liste des newsletter',$page_name),
    'icon' => 'icon-arrow-left'
);
$this->toolbar_btn['save'] = array(
    'href' => $_SERVER['REQUEST_URI'],
    'desc' => $this->l('Sauver',$page_name),
);
$this->toolbar_btn['save-and-stay'] = array(
    'short' => 'SaveAndStay',
    'href' => $_SERVER['REQUEST_URI'],
    'desc' => $this->l('Sauver & Rester',$page_name),
);

$id = Tools::getValue('id_supernewsletter_content');
$SupernewsletterContentCustomLoad = new SupernewsletterContent($id);
if(!empty($id)){
    $this->toolbar_btn['preview'] = array(
        'short' => 'preview',
        'href' => $Shop->getBaseURL().'modules/supernewsletter/front_generate_newsletter.php?id_supernewsletter_content='.$id.'&id_shop='.$this->context->shop->id.'&id_lang='.$id_lang.'&id_currency='.$SupernewsletterContentCustomLoad->id_currency.'&preview=1&see_online=1&token='.md5($id),
        'desc' => $this->l('Prévisualiser',$page_name),
        'target'=>'blank',
        'icon' => 'icon-eye-open'
    );
}
		
// Formulaire
foreach($languages as $k => $language){$languages[$k]['is_default'] = (int)($language['id_lang'] == Configuration::get('PS_LANG_DEFAULT'));}

$helper = new HelperForm();
$helper->module = $this;
$helper->name_controller = 'supernewsletter';
$helper->identifier = $this->identifier;
$helper->token = Tools::getAdminTokenLite('AdminModules');
$helper->languages = $languages;
$helper->currentIndex = $_SERVER['REQUEST_URI'];
$helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
$helper->allow_employee_form_lang = true;
$helper->toolbar_scroll = true;
$helper->toolbar_btn = $this->toolbar_btn;
$helper->title = $this->l('Contenu de la newsletter',$page_name);
$helper->submit_action = 'submitEditNewsletter';

if($this->ps_version==1.6){$this->_html.=$this->toolbarModule($helper);}

// templates
$templates = array();
$templates_db = SupernewsletterTemplate::getTemplates();
foreach($templates_db as $t){
    $templates[] = array('name'=>$t['name'],'id_supernewsletter_template'=>$t['id_supernewsletter_template']); 
}

// traductions
$langs_send = array();
$langs_send[] = array('name'=>$this->l('Envoyer dans la langue du client',$page_name),'id_lang'=>'9999'); 
$langs = Language::getLanguages($cookie->id_lang);
foreach($langs as $l){
    $langs_send[] = array('name'=>$this->l('Toujours envoyer en',$page_name).' : '.$l['name'],'id_lang'=>$l['id_lang']); 
}

$groups = Group::getGroups($cookie->id_lang);

// liste des groupes clients
$groups_list = array();
foreach($groups as $g){
    $Group = new Group($g['id_group'],$cookie->id_lang);
    $nbre = $Group->getCustomers(true,0,0,true);
    $groups_list[] = array('id_group'=>$g['id_group'],'name'=>$Group->name.' ('.$nbre.')'); 
}

// liste des groupes clients prix
$groups_list_price = array();
foreach($groups as $g){
    $Group = new Group($g['id_group'],$cookie->id_lang);
    $groups_list_price[] = array('id_group'=>$g['id_group'],'name'=>$Group->name); 
}

// pays
$countries = Country::getCountries($this->context->language->id);
array_unshift($countries,array('name'=>$this->l('Tous',$page_name),'id_country'=>0));

$this->fields_form[0]['form'] = array(
    'tinymce' => true,
    'legend' => array(
            'title' => $this->l('Edition',$page_name),
            'image' => $this->_path.'views/img/edit.png'
    ),
    'input' => array(
            array(
                    'type' => 'text',
                    'label' => $this->l('Titre de la newsletter',$page_name),
                    'name' => 'title',
                    'lang' => true,
                    'size' => 64,
                    'class'=>'title'
            ),
            array('type' => 'free','label' => '','name' => 'spacer_1_start'),
            array(
                    'type' => 'text',
                    'label' => $this->l('Date',$page_name),
                    'name' => 'date',
                    'size' => 10,
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Envoi par tâche planifée',$page_name),
                'name' => 'cron',
                'class' => 't',
                'required' => true,
                'is_bool' => true,
                'values' => array(
                        array(
                                'id' => 'cron_on',
                                'value' => 1,
                                'label' => $this->l('Yes',$page_name)),
                        array(
                                'id' => 'cron_off',
                                'value' => 0,
                                'label' => $this->l('No',$page_name)),
                ),
            ),
           array('type' => 'free','label' => '','name' => 'spacer_1_end'),
           array(
                    'type' => 'select',
                    'label' => $this->l('Thème graphique',$page_name),
                    'name' => 'id_supernewsletter_template',
                    'required' => true,
                    'options' => array(
                            'query' => $templates,
                            'id' => 'id_supernewsletter_template',
                            'name' => 'name',
                            'default' => array(
                                                'value' => '',
                                                'label' => $this->l('Sélectionner',$page_name)
                                               )
                            ),
            ),
            array(
                    'type' => 'select',
                    'label' => $this->l('Langue',$page_name),
                    'name' => 'id_lang_default',
                    'required' => true,
                    'default_value' => (int)$cookie->id_lang,
                    'options' => array(
                            'query' => $langs_send,
                            'id' => 'id_lang',
                            'name' => 'name',
                            'default' => array(
                                                'value' => '',
                                                'label' => $this->l('Sélectionner',$page_name)
                                               )
                            ),
            ),
            array(
                    'type' => 'select',
                    'label' => $this->l('Affichage des prix selon groupe',$page_name),
                    'name' => 'id_group_price',
                    'required' => true,
                    'default_value' => (int)$this->context->country->id,
                    'options' => array(
                            'query' => $groups_list_price,
                            'id' => 'id_group',
                            'name' => 'name',
                            )
            ), 
            array(
                    'type' => 'select',
                    'label' => $this->l('Monnaie',$page_name),
                    'name' => 'id_currency',
                    'default_value' => Configuration::get('PS_CURRENCY_DEFAULT'),
                    'options' => array(
                            'query' => Currency::getCurrencies(),
                            'id' => 'id_currency',
                            'name' => 'name',
                            ),
            ),
            array(
                    'type' => 'checkbox',
                    'label' => $this->l('Envoyer au groupe',$page_name).' : ',
                    'name' => 'ids_groups',
                    'values' => array(
                            'query' => $groups_list,
                            'id' => 'id_group',
                            'name' => 'name',
                            )
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Inclure les clients désactivés',$page_name),
                'name' => 'cust_disabled',
                'class' => 't',
                'required' => true,
                'is_bool' => true,
                'values' => array(
                        array(
                                'id' => 'cust_disabled_on',
                                'value' => 1,
                                'label' => $this->l('Yes',$page_name)),
                        array(
                                'id' => 'cust_disabled_off',
                                'value' => 0,
                                'label' => $this->l('No',$page_name)),
                ),
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Inclure les clients refusant la newsletter',$page_name),
                'name' => 'cust_no_news',
                'class' => 't',
                'required' => true,
                'is_bool' => true,
                'values' => array(
                        array(
                                'id' => 'cust_no_news_on',
                                'value' => 1,
                                'label' => $this->l('Yes',$page_name)),
                        array(
                                'id' => 'cust_no_news_off',
                                'value' => 0,
                                'label' => $this->l('No',$page_name)),
                ),
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Inclure les clients supprimés',$page_name),
                'name' => 'cust_deleted',
                'class' => 't',
                'required' => true,
                'is_bool' => true,
                'values' => array(
                        array(
                                'id' => 'cust_deleted_on',
                                'value' => 1,
                                'label' => $this->l('Yes',$page_name)),
                        array(
                                'id' => 'cust_deleted_off',
                                'value' => 0,
                                'label' => $this->l('No',$page_name)),
                ),
            ),
            array(
                    'type' => 'select',
                    'label' => $this->l('Basés dans le pays',$page_name),
                    'name' => 'id_country',
                    'required' => false,
                    'default_value' => (int)$this->context->country->id,
                    'options' => array(
                            'query' => $countries,
                            'id' => 'id_country',
                            'name' => 'name',
                            )
            ), 
            array(
                'type' => 'radio',
                'label' => $this->l('Envoyer aux',$page_name).' <span class="important">'.Db::getInstance()->getValue('SELECT count(id) FROM `'._DB_PREFIX_.'newsletter` WHERE `id_shop`="'.pSQL($id_shop).'" AND `active`=1').'</span> '.$this->l('clients enregistrés manuellement via le front-office',$page_name),
                'name' => 'register_front',
                'class' => 't',
                'required' => true,
                'is_bool' => true,
                'values' => array(
                        array(
                                'id' => 'register_front_on',
                                'value' => 1,
                                'label' => $this->l('Yes',$page_name)),
                        array(
                                'id' => 'register_front_off',
                                'value' => 0,
                                'label' => $this->l('No',$page_name)),
                ),
            ),
            array(
                    'type' => 'checkbox',
                    'label' => $this->l('Envoyer aux emails importés (via CSV) dans le groupe',$page_name),
                    'name' => 'groups',
                    'values' => array(
                            'query' => SupernewsletterEmail::getGroups(),
                            'id' => 'group',
                            'name' => 'name',
                            ),
            ),
            array(
                    'type' => 'textarea',
                    'label' => $this->l('Message',$page_name),
                    'name' => 'content',
                    'lang' => true,
                    'autoload_rte' => true,
                    'hint' => '',
                    'cols' => 60,
                    'rows' => 30
            ),
            array(
                    'type' => 'free',
                    'label' => $this->l('Alias',$page_name),
                    'name' => 'content_info',
            ),
            array(
                    'type' => 'free',
                    'label' => $this->l('Rechercher un produit',$page_name),
                    'name' => 'product_search',
            ),
    ),	
    'submit' => array(
            'name' => 'submitEditNewsletter',
            'title' => $this->l('Sauver ',$page_name),
            'class' => 'button'
    ),
);		

// spacer
$helper->fields_value['spacer_1_start'] = '<div class="separator"></div>';
$helper->fields_value['spacer_1_end'] = '<div class="separator"></div>';
    
// champ info contenu
$content_info = '';
foreach($this->getAlias() as $alias){
    $content_info.=$alias['label'].' => <span class="alias">{'.$alias['field_name'].'}</span>, ';
}
$content_info = Tools::substr($content_info,0,-2);
$helper->fields_value['content_info'] = 
$this->l('Vous pouvez utiliser les variables dynamiques suivantes dans le message',$page_name).' : <br/>
<span id="lst_alias">'.$content_info.'</span>';
    
// champ recherche
$helper->fields_value['product_search'] = '
<input type="text" value="" id="product_search" /> 
<a href="#"><img src="'.$this->_path.'views/img/add.png" class="icon_middle" id="add_product" /></a>
<input type="hidden" value="" id="id_product" /> 
<input type="hidden" value="" id="id_product_attribute" /> 
<input type="hidden" value="" id="img_link" />';

// charge la liste des produits existants
$products_lst = '';
$id_supernewsletter_content = Tools::getValue('id_supernewsletter_content');
if(!empty($id_supernewsletter_content)){
    
    $SupernewsletterContentProducts = new SupernewsletterContent($id_supernewsletter_content);
    
    $products = unserialize($SupernewsletterContentProducts->products);
    if(empty($products)){$products = array();}
    $image_type = $this->getImageType('medium');
    
    foreach($products as $p){  
        
         $Product = new Product($p['id_product'],false,(int)$cookie->id_lang);
         $image = Product::getCover($p['id_product']);
         $Link = new Link();
         
         if(Configuration::get('PS_LEGACY_IMAGES')){$id_image_for_link=$p['id_product'].'-'.$image['id_image'];}else{$id_image_for_link=$image['id_image'];}    
         if(!empty($image['id_image']) && $image['id_image']!=0){$img_link_princ='http://'.$Link->getImageLink($Product->link_rewrite,$id_image_for_link,$image_type);}else{$img_link_princ=$this->_path.'/views/img/no_picture_product.jpg';}
         
         // simple
         if(empty($p['id_product_attribute'])){
            $name = $Product->name;
            $img_link=$img_link_princ;
         // declinaison
         }else{ 
            $combArray = array();
            $assocNames = array();
            $combinations = $Product->getAttributeCombinations((int)$cookie->id_lang);
            $combinations_images = $Product->getCombinationImages($cookie->id_lang);

            foreach ($combinations as $k => $combination)
                    $combArray[$combination['id_product_attribute']][] = array('group' => $combination['group_name'], 'attr' => $combination['attribute_name']);
            foreach ($combArray as $id_product_attribute => $product_attribute)
            {
                $list = '';
                foreach ($product_attribute as $attribute)
                        $list .= trim($attribute['group']).' - '.trim($attribute['attr']).', ';
                $list = rtrim($list, ', ');
                $assocNames[$id_product_attribute] = $list;
                
                // declinaison
                if($p['id_product_attribute']==$id_product_attribute){
                    $name = $Product->name.' '.$list;
                    $id_image = @$combinations_images[$id_product_attribute][0]['id_image'];
                    if(Configuration::get('PS_LEGACY_IMAGES')){$id_image_for_link=$p['id_product'].'-'.$id_image;}else{$id_image_for_link=$id_image;}    
                    if(!empty($id_image) && $id_image!=0){$img_link_decl='http://'.$Link->getImageLink($Product->link_rewrite,$id_image_for_link,$image_type);}else{$img_link_decl=$img_link_princ;}
                    $img_link = $img_link_decl;
                }
            }
         }
        
         $id_unique_random = uniqid();
         $products_lst .= '
         <li id="li_'.$id_unique_random.'">
            <input type="hidden" name="id_product_lst[]" value="'.$p['id_product'].'" />
            <input type="hidden" name="id_product_attribute_lst[]" value="'.$p['id_product_attribute'].'" />
            <a href="#"><img src="'.$this->_path.'views/img/move.png" /></a>&nbsp;
            <img src="'.$img_link.'" class="img_product" style="width:58px" />'.$name.'
            <a href="" class="delete_product" id="'.$id_unique_random.'"><img src="'.$this->_path.'views/img/delete.png" /></a>
         </li>';
    }

}else{
    $products_lst = '';
}

$helper->fields_value['product_search'] .= '
<h3 id="h3_list_products">'.$this->l('Liste des produits à inclure',$page_name).'</h3>
<ul id="products_list">'.((!empty($products_lst))?$products_lst:'<span id="no_product">'.$this->l('Aucun produit',$page_name).'</span>').'</ul>';

// validation avec erreur
if(!empty($this->_errors)){
    $SupernewsletterContent = new SupernewsletterContent();
    $SupernewsletterContent->copyFromPost();
// nouvelle newsletter
}elseif(empty($id_supernewsletter_content) && $_POST){
    $SupernewsletterContent = new SupernewsletterContent();
// update newsletter
}else{
    $SupernewsletterContent = new SupernewsletterContent($id_supernewsletter_content);
}

// remplissage des champs classiques
foreach($this->fields_form[0]['form']['input'] as $input){
    if($input['name'] != 'content_info' && $input['name'] != 'product_search' && $input['name'] != 'spacer_1_start' && $input['name'] != 'spacer_1_end'){
        $helper->fields_value[$input['name']] = $SupernewsletterContent->{$input['name']};
    }
}

// remplissage des checkbox ids_groups
if(!empty($SupernewsletterContent->ids_groups)){
    $ids_groups = unserialize($SupernewsletterContent->ids_groups);
    foreach($ids_groups as $id){
        $helper->fields_value['ids_groups_'.$id] = 1;
    }
}

// remplissage des checkbox groups
if(!empty($SupernewsletterContent->groups)){
    $groups = unserialize($SupernewsletterContent->groups);
    foreach($groups as $group){
        $helper->fields_value['groups_'.$group] = 1;
    }
}

// Jquery
$this->context->controller->addJqueryUI('ui.datepicker');

$shop_domain = Configuration::get('PS_SHOP_DOMAIN');
$ssl = Configuration::get('PS_SSL_ENABLED');
$base_url_ajax = $Shop->getBaseURL();
if($ssl && Tools::strpos('https',$base_url_ajax)===false){
    $base_url_ajax = str_replace('http','https',$Shop->getBaseURL());
}

$this->_html.='
<script src="'.$base_url_ajax.'modules/'.$this->name.'/views/js/jquery-ui-1.10.3.js"></script>
<link rel="stylesheet" href="'.$base_url_ajax.'modules/'.$this->name.'/views/css/jquery-ui-1.10.3.css" />  
<style>
    .ui-autocomplete-loading {background: white url("images/ui-anim_basic_16x16.gif") right center no-repeat;}
</style>

<script type="text/javascript">
$(document).ready(function(){
    
    $("#date").datepicker({dateFormat:"yy-mm-dd"});

    $("#product_search").autocomplete({
        source: function(request,response){
            $.ajax({
                url: "'.$base_url_ajax.'modules/'.$this->name.'/ajax/product_search.php",
                dataType: "json",
                data:{
                    featureClass: "P",
                    style: "full",
                    maxRows: 12,
                    q:request.term,
                    id_shop:'.$id_shop.',
                    id_lang:'.$this->context->cookie->id_lang.',
                    token:"'._COOKIE_KEY_.'",
                },
                success: function(data){
                    response($.map(data,function(item){
                        return{
                            id_product:item.id_product,
                            id_product_attribute:item.id_product_attribute,
                            img_link:item.img_link,
                            value:item.name,
                        }
                    }));
                },
                error: function(jqXHR, exception) {
                    if (jqXHR.status === 0) {
                        alert("Not connect.\n Verify Network.");
                    } else if (jqXHR.status == 404) {
                        alert("Requested page not found. [404]");
                    } else if (jqXHR.status == 500) {
                        alert("Internal Server Error [500].");
                    } else if (exception === "parsererror") {
                        alert("Requested JSON parse failed.");
                    } else if (exception === "timeout") {
                        alert("Time out error.");
                    } else if (exception === "abort") {
                        alert("Ajax request aborted.");
                    } else {
                        alert("Uncaught Error.\n" + jqXHR.responseText);
                    }
                }
            })
        },
        select: function(event,data){
            $("#id_product").val(data.item.id_product);
            $("#id_product_attribute").val(data.item.id_product_attribute);
            $("#img_link").val(data.item.img_link);
        },
    });	

    $("#add_product").click(function(){
        id_product = $("#id_product").val();
        if(id_product!=""){
            id_unique_random = Math.floor((Math.random()*1000)+1); 
            $("#products_list").append("<li id=\"li_"+id_unique_random+"\"><input type=\"hidden\" name=\"id_product_lst[]\" value=\""+$("#id_product").val()+"\" /><input type=\"hidden\" name=\"id_product_attribute_lst[]\" value=\""+$("#id_product_attribute").val()+"\" /><a href=\"#\"><img src=\"'.$this->_path.'views/img/move.png\" /><\/a>&nbsp;<img style=\"width:58px\" src=\""+$("#img_link").val()+"\" class=\"img_product\" />"+$("#product_search").val()+" <a href=\"\" class=\"delete_product\" id=\""+id_unique_random+"\"><img src=\"'.$this->_path.'views/img/delete.png\" /></a></li>");
            $("#no_product").css("display","none");
            // vide les champs après ajout
            $("#product_search").val("");
            $("#id_product").val("");
            $("#id_product_attribute").val("");
            $("#img_link").val("");
        }else{
            alert("'.$this->l('Veuillez sélectionner un produit',$page_name).'");
        }
        return false;
    });
    
    $(".delete_product").live("click", function(){
        $("#li_"+this.id).remove();
        return false;  
    });
    
    /* trie des produits */
    $("#products_list").sortable();
    $("#products_list").disableSelection();
    
});
</script>';

$this->_html .= $helper->generateForm($this->fields_form);
		
?>