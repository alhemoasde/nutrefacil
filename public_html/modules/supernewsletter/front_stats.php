<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

// Effectue un tracking des statistiques
include(dirname(__FILE__).'/../../config/config.inc.php');

// check token
$token = Tools::getValue('token');
if($token!==_COOKIE_KEY_){die('Error : bad token');}

include(dirname(__FILE__).'/models/SupernewsletterContent.php');
include(dirname(__FILE__).'/models/SupernewsletterStats.php');
include(dirname(__FILE__).'/classes/ToolClass.php');

$controller=new FrontController();
$controller->init();

$id_supernewsletter_content = Tools::getValue('id_supernewsletter_content');
$id_product = Tools::getValue('id_product');
$id_product_attribute = Tools::getValue('id_product_attribute');
$stats_type = Tools::getValue('stats_type');
$id_lang = Tools::getValue('id_lang');
$preview = Tools::getValue('preview');

////////////////////////////
// création statistiques
////////////////////////////
    
$ip = $_SERVER['REMOTE_ADDR'];
$SupernewsletterStats = new SupernewsletterStats($id_supernewsletter_content);

// ouverture de newsletter
if($stats_type=='open'){
    // track stat ?
    if($preview!=1){
        $ips_history = unserialize($SupernewsletterStats->open_ip);
        if(!empty($ips_history) && !is_array($ips_history)){$ips_history=array($ips_history);}
        // stock l'ip si c'est la 1ère fois
        if(!in_array($ip,$ips_history)){
            $ips_history[]=$ip;
            $SupernewsletterStats->open_ip = serialize($ips_history);
            $SupernewsletterStats->update();
        }
    }
    $id_shop = Shop::getCurrentShop();
    $Shop = new Shop($id_shop);
    Tools::redirectLink($Shop->getBaseURL().'modules/supernewsletter/views/img/onepx.gif');
    exit();
// clique sur un lien produit de la newsletter
}elseif($stats_type=='product'){
    
    // track stat ?
    if($preview!=1){
        $ips_history = unserialize($SupernewsletterStats->redirect_ip);
        if(!empty($ips_history) && !is_array($ips_history)){$ips_history=array($ips_history);}
        // stock l'ip si c'est la 1ère fois
        if(!in_array($ip,$ips_history)){
            // trace l'ip
            $ips_history[]=$ip;
            $SupernewsletterStats->redirect_ip = serialize($ips_history);
            $SupernewsletterStats->update();
        } 

        // sauve toute la liste des produits (à vide)
        $clicked_products = unserialize($SupernewsletterStats->clicked_products);
        if(empty($clicked_products)){
            $SupernewsletterContent = new SupernewsletterContent($id_supernewsletter_content);
            foreach(unserialize($SupernewsletterContent->products) as $product){
                $clicked_products[] = array('id_product'=>$product['id_product'],
                                            'id_product_attribute'=>$product['id_product_attribute'],
                                            'ips' =>'',
                                            'nb_clicks'=>'',
                                            );
            }
        }
        // mets à jour les clics
        $clicked_products_update = array();
        foreach($clicked_products as $p){
            // pas encore cliqué ?
            if(is_array($p['ips'])){$ips=$p['ips'];}else{$ips=array();}
            if($p['id_product']==$id_product && $p['id_product_attribute']==$id_product_attribute && !in_array($ip,$ips)){

                    $clicked_products_update[] = array('id_product'=>$id_product,
                                                       'id_product_attribute'=>$p['id_product_attribute'],
                                                       'ips' => array_merge($ips,array($ip)),
                                                       'nb_clicks'=>$p['nb_clicks']+1,
                                                       );
            // si déjà cliqué
            }else{
                $clicked_products_update[] = array('id_product'=>$p['id_product'],
                                                   'id_product_attribute'=>$p['id_product_attribute'],
                                                   'ips' => $p['ips'],
                                                   'nb_clicks'=>$p['nb_clicks'],
                                                   );
            }
        }      
        $SupernewsletterStats = new SupernewsletterStats($id_supernewsletter_content);
        $SupernewsletterStats->clicked_products = serialize($clicked_products_update);
        $SupernewsletterStats->update();
    }

    // redirection fiche produit
    $Product = new Product($id_product,false,$id_lang);
    $Link = new Link();
    $link_product = $Link->getProductLink($Product).'?id_product='.$id_product.'&id_product_attribute='.$id_product_attribute.'&utm_source=supernewsletter&utm_medium=id_'.$id_supernewsletter_content;
    Tools::redirect($link_product,false);
    exit();

}elseif($stats_type=='special_link'){
    
    $link_type = Tools::getValue('link_type');
    $link_redirect = Tools::getValue('link_redirect');
    
    // track stat ?
    if($preview!=1){
        
        $links_types = array('view_online','img_header','logo','img_headerproducts','img_footer','facebook','twitter','rss','unsubscribe');
        $clicked_links = unserialize($SupernewsletterStats->clicked_links);

        if(empty($clicked_links)){
            $clicked_links = array();
            foreach($links_types as $lk_type){
                $clicked_links[$lk_type] = array('ips'=>'','nb_clicks'=>0);
            }
        }

        $clicked_links_update = array();
        foreach($clicked_links as $key_link_type=>$link){
           if(is_array($link['ips'])){$ips=$link['ips'];}else{$ips=array();}
           if($link_type==$key_link_type && !in_array($ip,$ips)){
               $clicked_links_update[$key_link_type] = array('ips'=>array_merge($ips,array($ip)),'nb_clicks'=>$link['nb_clicks']+1);
           }else{
               $clicked_links_update[$key_link_type] = array('ips'=>$link['ips'],'nb_clicks'=>$link['nb_clicks']);
           }
        }

        $SupernewsletterStats->clicked_links = serialize($clicked_links_update);
        $SupernewsletterStats->update();
    }
    
    if(strpos($link_redirect,'?')===false){
        $link_redirect.='?utm_source=supernewsletter&utm_medium=id_'.$id_supernewsletter_content;
    }else{
        $link_redirect.='&utm_source=supernewsletter&utm_medium=id_'.$id_supernewsletter_content;
    }
    
    Tools::redirect($link_redirect,false);
    exit();
}

?>
