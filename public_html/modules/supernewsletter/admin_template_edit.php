<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

if(!defined('_COOKIE_KEY_')){die('token error');}

// suppression image
if(Tools::getValue('del_img_header') || 
   Tools::getValue('del_img_headerproducts') || 
   Tools::getValue('del_img_facebook') || 
   Tools::getValue('del_img_twitter') || 
   Tools::getValue('del_img_rss') || 
   Tools::getValue('del_img_footer')){
    FileManager::deleteFile(dirname(__FILE__).'/templates/'.Tools::getValue('id_supernewsletter_template').'/'.Tools::getValue('filename'));
    $this->displayConfirmation('Image supprimée');
}

// soumission du formulaire
if(Tools::isSubmit('submitEditTemplate') || Tools::isSubmit('submitEditTemplateAndStay')){
    
    // Contrôle du formulaire
    $name = Tools::getValue('name');
    if(empty($name)){
        $this->_errors[] = $this->l('Veuillez indiquer le nom du thème',$page_name); 
    }
    
    $width = Tools::getValue('width');
    if(empty($width) || !is_numeric($width)){
        $this->_errors[] = $this->l('Veuillez indiquer la largeur de la newsletter',$page_name); 
    }
    
    $bg_body = Tools::getValue('bg_body');
    if(empty($bg_body)){
        $this->_errors[] = $this->l('Veuillez indiquer la couleur de fond de l\'arrière plan',$page_name); 
    }
    
    $product_img_size = Tools::getValue('product_img_size');
    if(empty($product_img_size)){
        $this->_errors[] = $this->l('Veuillez indiquer la taille des images produits',$page_name); 
    }
    
    $police = Tools::getValue('police');
    if(empty($police)){
        $this->_errors[] = $this->l('Veuillez sélectionner une police de caractères',$page_name); 
    }
    
    $police_size = Tools::getValue('police_size');
    if(empty($police_size) || $police_size==0){
        $this->_errors[] = $this->l('Veuillez indiquer une taille, pour la police de caractères (par ex. 10)',$page_name); 
    }
    
    $bg_newsletter = Tools::getValue('bg_newsletter');
    if(empty($bg_newsletter)){
        $this->_errors[] = $this->l('Veuillez indiquer la couleur de fond de la newsletter',$page_name); 
    }
    
    $bg_product = Tools::getValue('bg_product');
    if(empty($bg_product)){
        $this->_errors[] = $this->l('Veuillez indiquer la couleur de fond pour l\'ensemble des produits',$page_name); 
    }
    
    $col_content = Tools::getValue('col_content');
    if(empty($col_content)){
        $this->_errors[] = $this->l('Veuillez indiquer la couleur, pour le contenu',$page_name); 
    }
    
    $size_content = Tools::getValue('size_content');
    if(empty($size_content) || $size_content==0){
        $this->_errors[] = $this->l('Veuillez indiquer la taille du texte, pour le contenu',$page_name); 
    }
    
    $bdr_content = Tools::getValue('bdr_content');
    if(empty($bdr_content)){
        $this->_errors[] = $this->l('Veuillez indiquer la couleur, pour la bordure du contenu',$page_name); 
    }
    
    $bdr_width_content = Tools::getValue('bdr_width_content');
    if(empty($bdr_width_content)){
        $this->_errors[] = $this->l('Veuillez indiquer la taille de la bordure, pour la bordure du contenu',$page_name); 
    }
    
    $bdr_block_products = Tools::getValue('bdr_block_products');
    if(empty($bdr_block_products)){
        $this->_errors[] = $this->l('Veuillez indiquer la couleur de la bordure, pour le bloc des produits',$page_name); 
    }
    
    $col_separator_product = Tools::getValue('col_separator_product');
    if(empty($col_separator_product)){
        $this->_errors[] = $this->l('Veuillez indiquer la couleur du trait, pour le séparateur produit',$page_name); 
    }
    
    $bdr_width_separator_product = Tools::getValue('bdr_width_separator_product');
    if(empty($bdr_width_separator_product)){
        $this->_errors[] = $this->l('Veuillez indiquer la largeur du trait, pour le séparateur produit',$page_name); 
    }
    
    $col_title_product = Tools::getValue('col_title_product');
    if(empty($col_title_product)){
        $this->_errors[] = $this->l('Veuillez indiquer la couleur du texte, pour le titre du produit',$page_name); 
    }
    
    $size_title_product = Tools::getValue('size_title_product');
    if(empty($size_title_product) || $size_title_product==0){
        $this->_errors[] = $this->l('Veuillez indiquer la taille du texte, pour le titre du produit',$page_name); 
    }
    
    $col_desc_product = Tools::getValue('col_desc_product');
    if(empty($col_desc_product)){
        $this->_errors[] = $this->l('Veuillez indiquer la couleur de la description produit',$page_name); 
    }
    
    $size_desc_product = Tools::getValue('size_desc_product');
    if(empty($size_desc_product)){
        $this->_errors[] = $this->l('Veuillez indiquer la taille du texte, pour la description produit',$page_name); 
    }
    
    $size_desc_product = Tools::getValue('size_desc_product');
    if(empty($size_desc_product) || $size_desc_product==0){
        $this->_errors[] = $this->l('Veuillez indiquer la taille de la description du produit',$page_name); 
    }
    
    $col_pricelabel_product = Tools::getValue('col_pricelabel_product');
    if(empty($col_pricelabel_product)){
        $this->_errors[] = $this->l('Veuillez indiquer la couleur du texte, pour le label du prix',$page_name); 
    }
    
    $size_pricelabel_product = Tools::getValue('size_pricelabel_product');
    if(empty($size_pricelabel_product)){
        $this->_errors[] = $this->l('Veuillez indiquer la taille du texte, pour le label du prix',$page_name); 
    }
    
    $col_price_product = Tools::getValue('col_price_product');
    if(empty($col_price_product)){
        $this->_errors[] = $this->l('Veuillez indiquer la couleur du texte, pour le prix',$page_name); 
    }
    
    $size_price_product = Tools::getValue('size_price_product');
    if(empty($size_price_product)){
        $this->_errors[] = $this->l('Veuillez indiquer la taille du texte, pour le prix',$page_name); 
    }
          
    $col_promo_product = Tools::getValue('col_promo_product');
    if(empty($col_promo_product)){
        $this->_errors[] = $this->l('Veuillez indiquer la couleur du texte, pour la promotion',$page_name); 
    }
    
    $size_promo_product = Tools::getValue('size_promo_product');
    if(empty($size_promo_product) || $size_promo_product==0){
        $this->_errors[] = $this->l('Veuillez indiquer la taille du texte, pour la promotion',$page_name); 
    }
    
    $bdr_img_product = Tools::getValue('bdr_img_product');
    if(empty($bdr_img_product)){
        $this->_errors[] = $this->l('Veuillez indiquer la couleur, pour la bordure de l\'image produit',$page_name); 
    }
    
    $size_btn_view_product = Tools::getValue('size_btn_view_product');
    if(empty($size_btn_view_product) || $size_btn_view_product==0){
        $this->_errors[] = $this->l('Veuillez indiquer la taille du texte, pour le bouton "En savoir +"',$page_name); 
    }
    
    $bdr_width_btn_view_product = Tools::getValue('bdr_width_btn_view_product');
    if(empty($bdr_width_btn_view_product)){
        $this->_errors[] = $this->l('Veuillez indiquer la taille de la bordure, pour le bouton "En savoir +"',$page_name); 
    }
    
    $col_links_hf = Tools::getValue('col_links_hf');
    if(empty($col_links_hf)){
        $this->_errors[] = $this->l('Veuillez indiquer la couleur du texte pour les liens d\'en-tête & de pied de page',$page_name); 
    }
    
    $size_links_hf = Tools::getValue('size_links_hf');
    if(empty($size_links_hf) || $size_links_hf==0){
        $this->_errors[] = $this->l('Veuillez indiquer la taille du texte pour les liens d\'en-tête & de pied de page',$page_name); 
    }
    
    
    if(count($this->_errors)){
        foreach($this->_errors as $err){
            $this->_html .= '<div class="alert error module_error alert-danger">'.$err.'</div>';
        }
        $this->_html .= '<style>.confirm{display:none;}</style>';
    }
    
    // si le formulaire est ok
    if(empty($this->_errors)){

        $id = Tools::getValue('id_supernewsletter_template');
        
        // add
        if(empty($id)){
           $SupernewsletterTemplate = new SupernewsletterTemplate();
        // update
        }else{
            $SupernewsletterTemplate = new SupernewsletterTemplate($id);
        }
        
        // Met à jour l'image (header + footer)    
        $positions = array('header','headerproducts','facebook','twitter','rss','footer');
        foreach($positions as $position){
            $FileManager = new FileManager();
            if($FileManager->checkFormat('image',$_FILES['img_'.$position]['name'])){
                // création répertoire
                $folder_dir = dirname(__FILE__).'/templates/'.$id.'/';
                $FileManager->newFolder($folder_dir);
                // tranfert
                $type = Tools::strtolower(Tools::substr(strrchr($_FILES['img_'.$position]['name'],'.'),1));
                $file_dest = $folder_dir.'img_'.$position.'.'.$type;
                $FileManager->moveUploadedFile($_FILES['img_'.$position]['tmp_name'],$file_dest,Tools::getValue('width'));
            }
        }
        
        // global 
        $SupernewsletterTemplate->name = Tools::getValue('name');
        $SupernewsletterTemplate->width=Tools::getValue('width');
        $SupernewsletterTemplate->bg_body=Tools::getValue('bg_body');
        $SupernewsletterTemplate->link_img_header = Tools::getValue('link_img_header');
        
        $logo = Tools::getValue('logo_1');
        if($logo=='on'){$logo=1;}else{$logo=0;}
        $SupernewsletterTemplate->logo = $logo;
        $SupernewsletterTemplate->logo_position = Tools::getValue('logo_position');
        $SupernewsletterTemplate->link_img_headerproducts = Tools::getValue('link_img_headerproducts');
        
        $SupernewsletterTemplate->product_img_size = Tools::getValue('product_img_size');
        $SupernewsletterTemplate->product_title_len = Tools::getValue('product_title_len');
        $SupernewsletterTemplate->product_desc_type = Tools::getValue('product_desc_type');
        $SupernewsletterTemplate->product_desc_len = Tools::getValue('product_desc_len');
        $SupernewsletterTemplate->product_display_price = Tools::getValue('product_display_price');
        
        $SupernewsletterTemplate->link_facebook = Tools::getValue('link_facebook');
        $SupernewsletterTemplate->link_twitter = Tools::getValue('link_twitter');
        $SupernewsletterTemplate->link_rss = Tools::getValue('link_rss');
        $SupernewsletterTemplate->link_img_footer = Tools::getValue('link_img_footer');
        
        foreach($languages as $k => $language){
            $SupernewsletterTemplate->signing[$language['id_lang']] = Tools::getValue('signing_'.$language['id_lang']);
        }
        
        $SupernewsletterTemplate->police = Tools::getValue('police');
        $SupernewsletterTemplate->police_size = Tools::getValue('police_size');
        $SupernewsletterTemplate->bg_newsletter = Tools::getValue('bg_newsletter');
        $SupernewsletterTemplate->bg_product = Tools::getValue('bg_product');
        
        $SupernewsletterTemplate->col_content = Tools::getValue('col_content');
        $SupernewsletterTemplate->size_content = Tools::getValue('size_content');
        $SupernewsletterTemplate->bold_content = Tools::getValue('bold_content');
        $SupernewsletterTemplate->bdr_content = Tools::getValue('bdr_content');
        $SupernewsletterTemplate->bdr_width_content = Tools::getValue('bdr_width_content');
        
        $SupernewsletterTemplate->bdr_block_products = Tools::getValue('bdr_block_products');
        $SupernewsletterTemplate->bdr_width_block_products = Tools::getValue('bdr_width_block_products');
        
        $SupernewsletterTemplate->col_separator_product = Tools::getValue('col_separator_product');
        $SupernewsletterTemplate->bdr_width_separator_product = Tools::getValue('bdr_width_separator_product');
        
        $SupernewsletterTemplate->bdr_img_product = Tools::getValue('bdr_img_product');
        $SupernewsletterTemplate->bdr_width_img_product = Tools::getValue('bdr_width_img_product');
        
        $SupernewsletterTemplate->col_title_product = Tools::getValue('col_title_product');
        $SupernewsletterTemplate->size_title_product = Tools::getValue('size_title_product');
        $SupernewsletterTemplate->bold_title_product = Tools::getValue('bold_title_product');
        
        $SupernewsletterTemplate->col_desc_product = Tools::getValue('col_desc_product');
        $SupernewsletterTemplate->size_desc_product = Tools::getValue('size_desc_product');
        $SupernewsletterTemplate->bold_desc_product = Tools::getValue('bold_desc_product');
        
        $SupernewsletterTemplate->col_pricelabel_product = Tools::getValue('col_pricelabel_product');
        $SupernewsletterTemplate->size_pricelabel_product = Tools::getValue('size_pricelabel_product');
        $SupernewsletterTemplate->bold_pricelabel_product = Tools::getValue('bold_pricelabel_product');
        $SupernewsletterTemplate->col_price_product = Tools::getValue('col_price_product');
        $SupernewsletterTemplate->size_price_product = Tools::getValue('size_price_product');
        $SupernewsletterTemplate->bold_price_product = Tools::getValue('bold_price_product');
        
        $SupernewsletterTemplate->col_promo_product = Tools::getValue('col_promo_product');
        $SupernewsletterTemplate->size_promo_product = Tools::getValue('size_promo_product');
        $SupernewsletterTemplate->bold_promo_product = Tools::getValue('bold_promo_product');
           
        $SupernewsletterTemplate->bg_btn_view_product = Tools::getValue('bg_btn_view_product');
        $SupernewsletterTemplate->col_btn_view_product = Tools::getValue('col_btn_view_product');
        $SupernewsletterTemplate->size_btn_view_product = Tools::getValue('size_btn_view_product');
        $SupernewsletterTemplate->bold_btn_view_product = Tools::getValue('bold_btn_view_product');
        $SupernewsletterTemplate->bdr_btn_view_product = Tools::getValue('bdr_btn_view_product');
        $SupernewsletterTemplate->bdr_width_btn_view_product = Tools::getValue('bdr_width_btn_view_product');
        $SupernewsletterTemplate->pad_lft_btn_view_product = Tools::getValue('pad_lft_btn_view_product');
        
        $SupernewsletterTemplate->col_links_hf = Tools::getValue('col_links_hf');
        $SupernewsletterTemplate->size_links_hf = Tools::getValue('size_links_hf');
        
        // add 
        if(empty($id)){
           $SupernewsletterTemplate->add();
           $id = ToolClass::maxID(_DB_PREFIX_.'supernewsletter_template','id_supernewsletter_template');
           if(Tools::isSubmit('submitEditTemplateAndStay') || $this->ps_version==1.6){
               $url = $current_index.'&updatesupernewsletter_template&id_supernewsletter_template='.$id.'&msgtype=3';
           }else{
               $url = $current_index.'&templates&msgtype=3';
           }
        // update
        }else{
           $SupernewsletterTemplate->update(); 
           if(Tools::isSubmit('submitEditTemplateAndStay') || $this->ps_version==1.6){
               $url = $current_index.'&updatesupernewsletter_template&id_supernewsletter_template='.$id.'&msgtype=4';
           }else{
               $url = $current_index.'&templates&msgtype=4';
           }
        } 
        Tools::redirectAdmin($url);
    }
}

// Toolbar
$this->toolbar_btn['back'] =  array(
    'href' => $current_index.'&templates',
    'desc' => $this->l('Revenir à la liste des thèmes',$page_name),
    'icon' => 'icon-arrow-left'
);
$this->toolbar_btn['save'] = array(
    'href' => $_SERVER['REQUEST_URI'],
    'desc' => $this->l('Save',$page_name)

);
$this->toolbar_btn['save-and-stay'] = array(
    'short' => 'SaveAndStay',
    'href' => $_SERVER['REQUEST_URI'],
    'desc' => $this->l('Save and stay',$page_name),
);

// last id (for example)
$id_supernewsletter_content = ToolClass::maxID(_DB_PREFIX_.'supernewsletter_content','id_supernewsletter_content','`id_shop`='.$id_shop);
$SupernewsletterContentCustomLoad = new SupernewsletterContent($id_supernewsletter_content);
if(!empty($id_supernewsletter_content)){
    $this->toolbar_btn['preview'] = array(
        'short' => 'preview',
        'href' => $Shop->getBaseURL().'modules/supernewsletter/front_generate_newsletter.php?id_supernewsletter_content='.$id_supernewsletter_content.'&id_supernewsletter_template='.Tools::getValue('id_supernewsletter_template').'&id_shop='.$this->context->shop->id.'&id_lang='.$id_lang.'&id_currency='.$SupernewsletterContentCustomLoad->id_currency.'&preview=1&see_online=1&token='.md5($id_supernewsletter_content),
        'desc' => $this->l('Prévisualiser avec la dernière newsletter',$page_name),
        'target'=>'blank',
        'icon' => 'icon-eye-open'
    );
}

if($this->ps_version==1.6){$toolbarModule=$this->toolbarModule($this->toolbar_btn);}else{$toolbarModule='';}

// Polices
$polices = array(
                array('id_string'=>'Arial, Helvetica, Liberation Sans, FreeSans, sans-serif','name'=>'Arial'),
                array('id_string'=>'Courier New, Courier, FreeMono, Liberation Mono, monospace','name'=>'Courier New'),
                array('id_string'=>'Impact, Arial Black, sans-serif','name'=>'Impact'),
                array('id_string'=>'Georgia, DejaVu Serif, Norasi, serif','name'=>'Georgia'),
                array('id_string'=>'Lucida Sans, Lucida Grande, Lucida Sans Unicode','name'=>'Lucida Sans'),
                array('id_string'=>'Luxi Sans, Arial, sans-serif','name'=>'Luxi Sans'),
                array('id_string'=>'Monaco,DejaVu Sans Mono, Lucida Console, Andale Mono, monospace','name'=>'Monaco'),
                array('id_string'=>'Tahoma, Geneva, Kalimati, sans-serif','name'=>'Tahoma'),
                array('id_string'=>'Times New Roman, Times, Liberation Serif, FreeSerif, serif','name'=>'Times New Roman'),
                array('id_string'=>'Trebuchet MS, Arial, Helvetica, sans-serif','name'=>'Trebuchet MS'),
                array('id_string'=>'Verdana, DejaVu Sans, Bitstream Vera Sans, Geneva, sans-serif','name'=>'Verdana'),     
            );

// Formulaire
foreach($languages as $k => $language){$languages[$k]['is_default'] = (int)($language['id_lang'] == Configuration::get('PS_LANG_DEFAULT'));}

$helper = new HelperForm();
$helper->module = $this;
$helper->name_controller = 'supernewsletter';
$helper->identifier = $this->identifier;
$helper->token = Tools::getAdminTokenLite('AdminModules');
$helper->languages = $languages;
$helper->currentIndex = $_SERVER['REQUEST_URI'];
$helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
$helper->allow_employee_form_lang = true;
$helper->toolbar_scroll = true;
$helper->toolbar_btn = $this->toolbar_btn;
$helper->title = $this->l('Thème graphique',$page_name);
$helper->submit_action = 'submitEditTemplate';

$this->fields_form[0]['form'] = array(
    'legend' => array(
            'title' => $this->l('Paramètres',$page_name),
            'image' => $this->_path.'views/img/color_swatch.png'
    ),
    'input' => array(
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'label_configuration',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Nom du thème',$page_name),
                    'name' => 'name',
                    'size' => 50,
                    'hint' => '',
                    'required' => true,
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Largeur de la newsletter',$page_name),
                    'name' => 'width',
                    'size' => 5,
                    'hint' => '',
                    'required' => true,
                    'suffix' => 'px',
            ),
           array(
                    'type' => 'color',
                    'label' => $this->l('Couleur de fond de l\'arrière plan',$page_name),
                    'name' => 'bg_body',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'file',
                    'label' => $this->l('Image entête (newsletter)',$page_name),
                    'name' => 'img_header',
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'img_header_display',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Lien sur l\'image',$page_name),
                    'name' => 'link_img_header',
                    'size' => 133,
                    'hint' => '',
            ),
            array(
                    'type' => 'checkbox',
                    'label' => $this->l('Afficher le logo de la boutique',$page_name),
                    'name' => 'logo',
                    'values' => array(
                            'query' => array(array('id_logo'=>'1','name'=>'')),
                            'id' => 'id_logo',
                            'name' => 'name',
                            )
            ),
            array(
                    'type' => 'select',
                    'label' => $this->l('Position du logo',$page_name),
                    'name' => 'logo_position',
                    'options' => array(
                            'query' =>array(
                                        array('id'=>'center','name'=>$this->l('Centré',$page_name)),
                                        array('id'=>'left','name'=>$this->l('A gauche',$page_name)),
                                        array('id'=>'right','name'=>$this->l('A droite',$page_name),'value'=>'bim'),
                                    ),
                            'id' => 'id',
                            'name' => 'name',
                            ),
            ),
            array(
                    'type' => 'file',
                    'label' => $this->l('Image entête (produits)',$page_name),
                    'name' => 'img_headerproducts',
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'img_headerproducts_display',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Lien sur l\'image',$page_name),
                    'name' => 'link_img_headerproducts',
                    'size' => 133,
                    'hint' => '',
            ),
            array(
                        'type' => 'select',
                        'label' => $this->l('Taille des images (produit)',$page_name),
                        'name' => 'product_img_size',
                        'options' => array(
                                'query' =>ImageType::getImagesTypes('products'),
                                'id' => 'name',
                                'name' => 'name',
                                ),
                        'required' => true,
                        'desc'=> '<a href="index.php?controller=AdminImages&token='.Tools::getAdminTokenLite('AdminImages').'" class="link" target="_blank"><img src="'.$this->_path.'views/img/picture_add.png" class="icon_middle" />'.$this->l('Il est conseillé de créer un nouveau type d\'image de taille 125px / 125px  pour les produits',$page_name).'</a>'
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Longueur du titre (produit)',$page_name),
                    'name' => 'product_title_len',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> $this->l('caractères (0=illimité)',$page_name),
            ),
            array(
                    'type' => 'select',
                    'label' => $this->l('Type de description (produit)',$page_name),
                    'name' => 'product_desc_type',
                    'options' => array(
                            'query' => array(
                                             array('id'=>'description_short','name'=>$this->l('description courte',$page_name)),
                                             array('id'=>'description','name'=>$this->l('description longue',$page_name)),
                                            ),
                            'id' => 'id',
                            'name' => 'name',
                            ),
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Longueur de la description (produit)',$page_name),
                    'name' => 'product_desc_len',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> $this->l('caractères (0=illimité)',$page_name),
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Afficher le prix (produit)',$page_name),
                'name' => 'product_display_price',
                'class' => 't',
                'required' => true,
                'is_bool' => true,
                'values' => array(
                        array(
                                'id' => 'product_display_price_on',
                                'value' => 1,
                                'label' => $this->l('Yes',$page_name)),
                        array(
                                'id' => 'product_display_price_off',
                                'value' => 0,
                                'label' => $this->l('No',$page_name)),
                ), 
            ),
            array(
                    'type' => 'file',
                    'label' => $this->l('Image bannière Facebook',$page_name),
                    'name' => 'img_facebook',
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'img_facebook_display',
            ),
           array(
                    'type' => 'text',
                    'label' => $this->l('Lien vers votre page Facebook',$page_name),
                    'name' => 'link_facebook',
                    'size' => 133,
                    'hint' => '',
            ),
            array(
                    'type' => 'file',
                    'label' => $this->l('Image bannière Twitter',$page_name),
                    'name' => 'img_twitter',
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'img_twitter_display',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Lien vers votre page Twitter',$page_name),
                    'name' => 'link_twitter',
                    'size' => 133,
                    'hint' => '',
            ),
            array(
                    'type' => 'file',
                    'label' => $this->l('Image bannière RSS',$page_name),
                    'name' => 'img_rss',
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'img_rss_display',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Lien vers votre Flux RSS',$page_name),
                    'name' => 'link_rss',
                    'size' => 133,
                    'hint' => '',
            ),
            array(
                    'type' => 'file',
                    'label' => $this->l('Image pied de page',$page_name),
                    'name' => 'img_footer',
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'img_footer_display',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Lien sur l\'image de pied de page',$page_name),
                    'name' => 'link_img_footer',
                    'size' => 133,
                    'hint' => '',
            ),
            array(
                    'type' => 'textarea',
                    'label' => $this->l('Signature',$page_name),
                    'name' => 'signing',
                    'lang' => true,
                    'autoload_rte' => true,
                    'hint' => '',
                    'cols' => 60,
                    'rows' => 30
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'label_general',
            ),
            array(
                    'type' => 'select',
                    'label' => $this->l('Police de caractère de la newsletter',$page_name),
                    'name' => 'police',
                    'options' => array(
                            'query' => $polices,
                            'id' => 'id_string',
                            'name' => 'name',
                            'default' => array(
                                                'value' => '',
                                                'label' => $this->l('Sélectionner',$page_name)
                                               )
                            ),
                  'required' => true,
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Taille de la police de la newsletter',$page_name),
                    'name' => 'police_size',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
            ),
            array(
                    'type' => 'color',
                    'label' => $this->l('Couleur de fond de la newsletter',$page_name),
                    'name' => 'bg_newsletter',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'color',
                    'label' => $this->l('Couleur de fond pour l\'ensemble des produits',$page_name),
                    'name' => 'bg_product',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'label_content',
            ),
            array(
                    'type' => 'color',
                    'label' => $this->l('Couleur du contenu',$page_name),
                    'name' => 'col_content',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Taille du contenu',$page_name),
                    'name' => 'size_content',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
           ),
           array(
                'type' => 'radio',
                'label' => $this->l('Afficher en gras le contenu',$page_name),
                'name' => 'bold_content',
                'class' => 't',
                'required' => true,
                'is_bool' => true,
                'values' => array(
                        array(
                                'id' => 'bold_content_on',
                                'value' => 1,
                                'label' => $this->l('Yes',$page_name)),
                        array(
                                'id' => 'bold_content_off',
                                'value' => 0,
                                'label' => $this->l('No',$page_name)),
                ),
            ),
           array(
                    'type' => 'color',
                    'label' => $this->l('Couleur bordure',$page_name),
                    'name' => 'bdr_content',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Largeur bordure',$page_name),
                    'name' => 'bdr_width_content',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'label_block_products',
            ),
           array(
                    'type' => 'color',
                    'label' => $this->l('Couleur bordure',$page_name),
                    'name' => 'bdr_block_products',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Largeur bordure',$page_name),
                    'name' => 'bdr_width_block_products',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'label_separator_product',
            ),
            array(
                    'type' => 'color',
                    'label' => $this->l('Couleur du trait',$page_name),
                    'name' => 'col_separator_product',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Largeur du trait',$page_name),
                    'name' => 'bdr_width_separator_product',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
            ), 
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'label_img_product',
            ),
            array(
                    'type' => 'color',
                    'label' => $this->l('Couleur bordure',$page_name),
                    'name' => 'bdr_img_product',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Largeur bordure',$page_name),
                    'name' => 'bdr_width_img_product',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'label_title_product',
            ),
            array(
                    'type' => 'color',
                    'label' => $this->l('Couleur du texte',$page_name),
                    'name' => 'col_title_product',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Taille du texte',$page_name),
                    'name' => 'size_title_product',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
            ),
         array(
                'type' => 'radio',
                'label' => $this->l('Afficher en gras',$page_name),
                'name' => 'bold_title_product',
                'class' => 't',
                'required' => true,
                'is_bool' => true,
                'values' => array(
                        array(
                                'id' => 'bold_title_product_on',
                                'value' => 1,
                                'label' => $this->l('Yes',$page_name)),
                        array(
                                'id' => 'bold_title_product_off',
                                'value' => 0,
                                'label' => $this->l('No',$page_name)),
                ),
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'label_desc_product',
            ),
                    array(
                    'type' => 'color',
                    'label' => $this->l('Couleur du texte',$page_name),
                    'name' => 'col_desc_product',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Taille du texte',$page_name),
                    'name' => 'size_desc_product',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Afficher en gras',$page_name),
                'name' => 'bold_desc_product',
                'class' => 't',
                'required' => true,
                'is_bool' => true,
                'values' => array(
                        array(
                                'id' => 'bold_desc_product_on',
                                'value' => 1,
                                'label' => $this->l('Yes',$page_name)),
                        array(
                                'id' => 'bold_desc_product_off',
                                'value' => 0,
                                'label' => $this->l('No',$page_name)),
                ),
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'label_price_product',
            ),
            array(
                    'type' => 'color',
                    'label' => $this->l('Couleur du label',$page_name),
                    'name' => 'col_pricelabel_product',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Taille du label',$page_name),
                    'name' => 'size_pricelabel_product',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Afficher en gras le label',$page_name),
                'name' => 'bold_pricelabel_product',
                'class' => 't',
                'required' => true,
                'is_bool' => true,
                'values' => array(
                        array(
                                'id' => 'bold_price_product_on',
                                'value' => 1,
                                'label' => $this->l('Yes',$page_name)),
                        array(
                                'id' => 'bold_price_product_off',
                                'value' => 0,
                                'label' => $this->l('No',$page_name)),
                ),
            ),
            array(
                    'type' => 'color',
                    'label' => $this->l('Couleur du prix',$page_name),
                    'name' => 'col_price_product',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Taille du prix',$page_name),
                    'name' => 'size_price_product',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Afficher en gras le prix',$page_name),
                'name' => 'bold_price_product',
                'class' => 't',
                'required' => true,
                'is_bool' => true,
                'values' => array(
                        array(
                                'id' => 'bold_price_product_on',
                                'value' => 1,
                                'label' => $this->l('Yes',$page_name)),
                        array(
                                'id' => 'bold_price_product_off',
                                'value' => 0,
                                'label' => $this->l('No',$page_name)),
                ),
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'label_promo_product',
            ),
                array(
                    'type' => 'color',
                    'label' => $this->l('Couleur du prix original',$page_name),
                    'name' => 'col_promo_product',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Taille du prix original',$page_name),
                    'name' => 'size_promo_product',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Afficher en gras le prix original',$page_name),
                'name' => 'bold_promo_product',
                'class' => 't',
                'required' => true,
                'is_bool' => true,
                'values' => array(
                        array(
                                'id' => 'bold_promo_product_on',
                                'value' => 1,
                                'label' => $this->l('Yes',$page_name)),
                        array(
                                'id' => 'bold_promo_product_off',
                                'value' => 0,
                                'label' => $this->l('No',$page_name)),
                ),
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'label_btn_view_product',
            ),
            array(
                    'type' => 'color',
                    'label' => $this->l('Couleur de fond',$page_name),
                    'name' => 'bg_btn_view_product',
                    'size' => 5,
                    'hint' => '',
                    'before'=> 'test'
            ),
            array(
                    'type' => 'color',
                    'label' => $this->l('Couleur du texte',$page_name),
                    'name' => 'col_btn_view_product',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Taille du texte',$page_name),
                    'name' => 'size_btn_view_product',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
            ),
            array(
                'type' => 'radio',
                'label' => $this->l('Afficher en gras',$page_name),
                'name' => 'bold_btn_view_product',
                'class' => 't',
                'required' => true,
                'is_bool' => true,
                'values' => array(
                        array(
                                'id' => 'bold_btn_view_product_on',
                                'value' => 1,
                                'label' => $this->l('Yes',$page_name)),
                        array(
                                'id' => 'bold_btn_view_product_off',
                                'value' => 0,
                                'label' => $this->l('No',$page_name)),
                ),
            ),
            array(
                    'type' => 'color',
                    'label' => $this->l('Couleur bordure',$page_name),
                    'name' => 'bdr_btn_view_product',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Largeur bordure',$page_name),
                    'name' => 'bdr_width_btn_view_product',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
            ),
             array(
                    'type' => 'text',
                    'label' => $this->l('Espacement à gauche',$page_name),
                    'name' => 'pad_lft_btn_view_product',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
            ),
            array(
                    'type' => 'free',
                    'label' => '',
                    'name' => 'label_other',
            ),
            array(
                    'type' => 'color',
                    'label' => $this->l('Couleur des liens (en-tête & pied de page)',$page_name),
                    'name' => 'col_links_hf',
                    'size' => 5,
                    'hint' => '',
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Taille des liens (en-tête & pied de page)',$page_name),
                    'name' => 'size_links_hf',
                    'size' => 1,
                    'hint' => '',
                    'suffix'=> 'px'
            ),
    ),	
    'submit' => array(
            'name' => 'submitEditTemplate',
            'title' => $this->l('Sauver ',$page_name),
            'class' => 'button'
    ),
);		

// charge les champs personnalisés des images
$id_supernewsletter_template = Tools::getValue('id_supernewsletter_template');
$positions = array('header','headerproducts','facebook','twitter','rss','footer');
foreach($positions as $position){
    if(!empty($id_supernewsletter_template)){
        $types = array('jpg', 'gif', 'jpeg', 'png');
        foreach($types as $type){
            if(file_exists(dirname(__FILE__).'/templates/'.$id_supernewsletter_template.'/img_'.$position.'.'.$type)){
                $img = 'img_'.$position.'.'.$type;
                $div_img = '<div class="center"><img src="'.$this->_path.'templates/'.$id_supernewsletter_template.'/'.$img.'?'.time().'" /></div>';
                $div_link = '<div class="center"><a href="'.$_SERVER['REQUEST_URI'].'&del_img_'.$position.'=1&filename='.$img.'" onclick="if(confirm(\''.$this->l('Supprimer ?',$page_name).'\')){return true;}else{return false;}"><img src="'.$this->_path.'views/img/delete.png" />'.$this->l('Supprimer',$page_name).'</a></div>';
                if($position=='header'){$size='500px / 190px';}
                if($position=='headerproducts'){$size='500px / 25px';}
                if($position=='facebook' || $position=='twitter' || $position=='rss'){$size='500px / 45px';}
                if($position=='footer'){$size='500px / 55px';}
                $field = '<div class="block_img_h_f"><center>'.$div_img.$div_link.$this->l('Taille recommandée pour l\'image',$page_name).' : '.$size.'</center></div>';
                $helper->fields_value['img_'.$position.'_display'] = $field;
                break;
            }
        }
    }
    foreach($positions as $position){
        if(!isset($helper->fields_value['img_'.$position.'_display'])){$helper->fields_value['img_'.$position.'_display']=$this->l('Aucune image définie',$page_name);}
    }    
}

// validation avec erreur
if(!empty($this->_errors)){
    $SupernewsletterTemplate = new SupernewsletterTemplate();
    $SupernewsletterTemplate->copyFromPost();
// add template
}elseif(empty($id_supernewsletter_template) && $_POST){
    $new = true;
    $SupernewsletterTemplate = new SupernewsletterTemplate();
// update template
}else{
    $SupernewsletterTemplate = new SupernewsletterTemplate($id_supernewsletter_template);
}

// remplissage des champs classiques
foreach($this->fields_form[0]['form']['input'] as $input){
    if($input['name'] != 'img_header' && 
       $input['name'] != 'img_header_display' && 
       $input['name'] != 'img_headerproducts' && 
       $input['name'] != 'img_headerproducts_display' && 
       $input['name'] != 'img_footer' && 
       $input['name'] != 'img_footer_display' &&  
       $input['name'] != 'img_facebook' && 
       $input['name'] != 'img_facebook_display' &&   
       $input['name'] != 'img_twitter' && 
       $input['name'] != 'img_twitter_display' && 
       $input['name'] != 'img_rss' && 
       $input['name'] != 'img_rss_display' && 
       $input['name'] != 'label_separator_product' && 
       $input['name'] != 'label_block_products' && 
       $input['name'] != 'label_title_product' && 
       $input['name'] != 'label_desc_product' &&  
       $input['name'] != 'label_btn_view_product' &&   
       $input['name'] != 'label_general' && 
       $input['name'] != 'label_configuration' && 
       $input['name'] != 'label_img_product' && 
       $input['name'] != 'label_other' && 
       $input['name'] != 'label_content' && 
       $input['name'] != 'label_price_product' && 
       $input['name'] != 'label_promo_product'){
            $helper->fields_value[$input['name']] = $SupernewsletterTemplate->{$input['name']};
    }
}

// force chargement valeurs par défaut
if(isset($new)&& $new==true){
    $helper->fields_value['name'] = $this->l('Mon nouveau thème',$page_name);
    $helper->fields_value['width'] = '500';
    $helper->fields_value['logo_1'] = '1';
    $helper->fields_value['product_display_price'] = '1';
    $helper->fields_value['product_img_size'] = $this->getImageType('home');
    $helper->fields_value['product_title_len'] = '30';
    $helper->fields_value['product_desc_len'] = '200';
    $helper->fields_value['police'] = 'Verdana, DejaVu Sans, Bitstream Vera Sans, Geneva, sans-serif';
    $helper->fields_value['police_size'] = '12';
    $helper->fields_value['bg_newsletter'] = '#ffffff';
    $helper->fields_value['bg_product'] = '#f4f4f4';
    $helper->fields_value['col_content'] = '#333333';
    $helper->fields_value['size_content'] = '12';
    $helper->fields_value['bdr_content'] = '#606060';
    $helper->fields_value['bdr_width_content'] = '1';
    $helper->fields_value['bdr_block_products'] = '#606060';
    $helper->fields_value['bdr_width_block_products'] = '1';
    $helper->fields_value['col_separator_product'] = '#606060';
    $helper->fields_value['bdr_width_content'] = '1';
    $helper->fields_value['bdr_width_separator_product'] = '1';
    $helper->fields_value['bdr_img_product'] = '#838383';
    $helper->fields_value['bdr_width_img_product'] = '1';
    $helper->fields_value['col_title_product'] = '#333333';
    $helper->fields_value['size_title_product'] = '15';
    $helper->fields_value['col_desc_product'] = '#686868';
    $helper->fields_value['size_desc_product'] = '12';
    $helper->fields_value['col_pricelabel_product'] = '#646464';
    $helper->fields_value['size_pricelabel_product'] = '16';
    $helper->fields_value['col_price_product'] = '#d81300';
    $helper->fields_value['size_price_product'] = '20';
    $helper->fields_value['bold_price_product'] = '1';
    $helper->fields_value['col_promo_product'] = '#d81300';
    $helper->fields_value['size_promo_product'] = '12';
    $helper->fields_value['bold_promo_product'] = '1';
    $helper->fields_value['bg_btn_view_product'] = '#464646';
    $helper->fields_value['col_btn_view_product'] = '#ffffff';
    $helper->fields_value['size_btn_view_product'] = '12';
    $helper->fields_value['bdr_btn_view_product'] = '#222222';
    $helper->fields_value['bdr_width_btn_view_product'] = '1';
    $helper->fields_value['pad_lft_btn_view_product'] = '0';
    $helper->fields_value['col_links_hf'] = '1';
    $helper->fields_value['size_links_hf'] = '10';
 }

// champs spéciaux & séparateurs
$style = 'border:1px dashed #717195;background-color:#fafafa;text-align:center;font-size:16px;width:'.(($SupernewsletterTemplate->width>690)?$SupernewsletterTemplate->width:'690').'px;height:30px;line-height:25px;margin-bottom:10px;';
$helper->fields_value['label_configuration'] = '<label style="'.$style.'">'.$this->l('Configuration structure',$page_name).'</label><br/>';
$helper->fields_value['label_general'] = '<label style="'.$style.'">'.$this->l('Paramètres généraux',$page_name).'</label><br/>';
$helper->fields_value['label_content'] = '<label style="'.$style.'">'.$this->l('Contenu',$page_name).'</label><br/>';
$helper->fields_value['label_block_products'] = '<label style="'.$style.'">'.$this->l('Bloc produit',$page_name).'</label><br/>';
$helper->fields_value['label_separator_product'] = '<label style="'.$style.'">'.$this->l('Séparateur produit',$page_name).'</label><br/>';
$helper->fields_value['label_img_product'] = '<label style="'.$style.'">'.$this->l('Image du produit',$page_name).'</label><br/>';
$helper->fields_value['label_title_product'] = '<label style="'.$style.'">'.$this->l('Titre du produit',$page_name).'</label><br/>';
$helper->fields_value['label_desc_product'] = '<label style="'.$style.'">'.$this->l('Description du produit',$page_name).'</label><br/>';
$helper->fields_value['label_price_product'] = '<label style="'.$style.'">'.$this->l('Prix du produit',$page_name).'</label><br/>';
$helper->fields_value['label_promo_product'] = '<label style="'.$style.'">'.$this->l('Promotion',$page_name).'</label><br/>';
$helper->fields_value['label_btn_view_product'] = '<label style="'.$style.'">'.$this->l('Bouton en savoir +',$page_name).'</label><br/>';
$helper->fields_value['label_other'] = '<label style="'.$style.'">'.$this->l('Autres',$page_name).'</label><br/>';

// remplissage checkbox logo
if(!empty($SupernewsletterTemplate->logo)){$helper->fields_value['logo_1'] = 1;}

$this->_html .= $toolbarModule.$helper->generateForm($this->fields_form);

// ajustements CSS
$this->_html .= '
<style>
    .block_img_h_f{
        width:'.$SupernewsletterTemplate->width.'px!important;
    }
</style>'
		
?>
