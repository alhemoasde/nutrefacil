<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

if(!defined('_COOKIE_KEY_')){die('token error');}

$helper = new HelperForm(); 

// Toolbar
$helper->toolbar_btn['back'] =  array(
    'href' => $current_index,
    'desc' => $this->l('Revenir à la liste des newsletter',$page_name),
    'icon' => 'icon-arrow-left'
);
if($this->ps_version==1.6){$toolbarModule=$this->toolbarModule($helper->toolbar_btn);}else{$toolbarModule='';}

$helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
$helper->allow_employee_form_lang = true;
$helper->toolbar_scroll = true;
$helper->toolbar_btn = $helper->toolbar_btn;
$helper->show_toolbar = true;
$helper->title = $this->l('Statistiques',$page_name);
$this->_html .= $toolbarModule.$helper->generateForm(array());
// --

$id_supernewsletter_content = Tools::getValue('id_supernewsletter_content');
if(empty($id_supernewsletter_content)){$id_supernewsletter_content=ToolClass::maxID(_DB_PREFIX_.'supernewsletter_stats','id_supernewsletter_content');}

$news = SupernewsletterContent::getNewsletters();
$currentIndex = 'index.php?controller=AdminModules';

$this->_html .= '
<label>'.$this->l('Sélectionnez la newsletter',$page_name).'</label>
<select id="id_supernewsletter_content" name="id_supernewsletter_content" onchange="if(this.value) window.location.href=this.value">';
    foreach($news as $n){
        if($id_supernewsletter_content==$n['id_supernewsletter_content']){$selected='selected';}else{$selected='';}
        $this->_html .= '
        <option value="'.$currentIndex.'&token='.Tools::getValue('token').'&configure=supernewsletter&stats&id_supernewsletter_content='.$n['id_supernewsletter_content'].'" '.$selected.'>'.$n['title'];
    }
$this->_html .= '
</select>';

$SupernewsletterContent = new SupernewsletterContent($id_supernewsletter_content);
$SupernewsletterStats = new SupernewsletterStats($id_supernewsletter_content);

// correction - applique le comptage directement sur le nombre de mails
$SupernewsletterStats->nb_unsubscribe = count(explode("\n",nl2br($SupernewsletterStats->emails_unsubscribe)));

$this->_html .= '
<br/>
<br/>
<table class="table table_stats" cellspacing="0" cellpadding="0">
    <tr><th colspan="2" class="center">'.$this->l('Statistiques générales',$page_name).'</th></tr>
    <tr><td class="cell1"><img src="'.$this->_path.'views/img/email.png" class="icon_bottom" /> '.$this->l('Newsletters envoyées',$page_name).' : </td><td>'.(is_array(unserialize($SupernewsletterContent->emails_sent))?count(unserialize($SupernewsletterContent->emails_sent)):0).'</td></tr>  
    <tr><td><img src="'.$this->_path.'views/img/email_open.png" class="icon_bottom" /> '.$this->l('Newsletters ouvertes',$page_name).' : </td><td>'.(is_array(unserialize($SupernewsletterStats->open_ip))?count(unserialize($SupernewsletterStats->open_ip)):0).'</td></tr>  
    <tr><td><img src="'.$this->_path.'views/img/user.png" class="icon_bottom" /> '.$this->l('Clients redirigés sur la boutique',$page_name).' : </td><td>'.(is_array(unserialize($SupernewsletterStats->redirect_ip))?count(unserialize($SupernewsletterStats->redirect_ip)):0).'</td></tr>    
    <tr><td><img src="'.$this->_path.'views/img/user_delete.png" class="icon_bottom" /> '.$this->l('Désinscriptions à la newsletter',$page_name).' : </td><td>'.((!empty($SupernewsletterStats->nb_unsubscribe))?$SupernewsletterStats->nb_unsubscribe:0).'</td></tr>
</table>
<br/>';

$this->_html .= '
<table class="table table_stats" cellspacing="0" cellpadding="0">
    <tr><th colspan="2" class="center">'.$this->l('Statistiques produits',$page_name).'</th></tr>
    <tr>
        <th class="cell1">'.$this->l('Nom',$page_name).'</th>
        <th>'.$this->l('Clics',$page_name).'</th>
    </tr>';
    
    $products = unserialize($SupernewsletterContent->products);
    if(!empty($products)){
        foreach($products as $p){

            // product name
            $Product = new Product($p['id_product']);
            $combs = $Product->getAttributeCombinationsById($p['id_product_attribute'],$id_lang);
            $lst = '';
            foreach($combs as $c){
                $lst .= ' - '.$c['group_name'].' '.$c['attribute_name'];
            }
            $this->_html .= '<tr><td><img src="'.$this->_path.'/views/img/product.png" class="icon_top" /> '.$Product->name[$id_lang].' '.$lst.'</td>';

            // nb click ? 
            $find = false;
            if(is_array(unserialize($SupernewsletterStats->clicked_products))){
                foreach(unserialize($SupernewsletterStats->clicked_products) as $p_clicked){
                    if($p['id_product']==$p_clicked['id_product'] && $p['id_product_attribute']==$p_clicked['id_product_attribute']){
                        $nb_clicks = $p_clicked['nb_clicks'];
                        if(empty($nb_clicks)){$nb_clicks=0;}
                        break;
                    }
                }
            }
            $this->_html .= '<td>'.(isset($nb_clicks)?$nb_clicks:'0').'</td>
            </tr>';
        }
    }else{
        $this->_html .= '<tr><td colspan="2" align="center">'.$this->l('Aucun produit cliqué pour le moment',$page_name).'</td></tr>';
    }
$this->_html .= '
</table>
<br/>';

$clicked_links = unserialize($SupernewsletterStats->clicked_links);
$this->_html .= '
<table class="table table_stats" cellspacing="0" cellpadding="0">
    <tr><th colspan="2" class="center">'.$this->l('Statistiques complémentaires',$page_name).'</th></tr>
    <tr>
        <th class="cell1">'.$this->l('Lien',$page_name).'</th>
        <th>'.$this->l('Clics',$page_name).'</th>
    </tr>
    <tr>
        <td><img src="'.$this->_path.'/views/img/link.png" class="icon_top" /> '.$this->l('Afficher correctement la newsletter',$page_name).'</td>
        <td>'.(empty($clicked_links['view_online']['nb_clicks'])?0:$clicked_links['view_online']['nb_clicks']).'</td>
    </tr>
    <tr>
        <td><img src="'.$this->_path.'/views/img/link.png" class="icon_top" /> '.$this->l('Image entête',$page_name).'</td>
        <td>'.(empty($clicked_links['img_header']['nb_clicks'])?0:$clicked_links['img_header']['nb_clicks']).'</td>
    </tr>
    <tr>
        <td><img src="'.$this->_path.'/views/img/link.png" class="icon_top" /> '.$this->l('Logo',$page_name).'</td>
        <td>'.(empty($clicked_links['logo']['nb_clicks'])?0:$clicked_links['logo']['nb_clicks']).'</td>
    </tr>
    <tr>
        <td><img src="'.$this->_path.'/views/img/link.png" class="icon_top" /> '.$this->l('Image entête (produits)',$page_name).'</td>
        <td>'.(empty($clicked_links['img_headerproducts']['nb_clicks'])?0:$clicked_links['img_headerproducts']['nb_clicks']).'</td>
    </tr>
    <tr>
        <td><img src="'.$this->_path.'/views/img/link.png" class="icon_top" /> '.$this->l('Image pied de page',$page_name).'</td>
        <td>'.(empty($clicked_links['img_footer']['nb_clicks'])?0:$clicked_links['img_footer']['nb_clicks']).'</td>
    </tr>
    <tr>
        <td><img src="'.$this->_path.'/views/img/link.png" class="icon_top" /> '.$this->l('Page Facebook',$page_name).'</td>
        <td>'.(empty($clicked_links['facebook']['nb_clicks'])?0:$clicked_links['facebook']['nb_clicks']).'</td>
    </tr>
    <tr>
        <td><img src="'.$this->_path.'/views/img/link.png" class="icon_top" /> '.$this->l('Page Twitter',$page_name).'</td>
        <td>'.(empty($clicked_links['twitter']['nb_clicks'])?0:$clicked_links['twitter']['nb_clicks']).'</td>
    </tr>
    <tr>
        <td><img src="'.$this->_path.'/views/img/link.png" class="icon_top" /> '.$this->l('Flux RSS',$page_name).'</td>
        <td>'.(empty($clicked_links['rss']['nb_clicks'])?0:$clicked_links['rss']['nb_clicks']).'</td>
    </tr>
    <tr>
        <td>';
        $this->_html .= '
            <script type="text/javascript">
                // Fancybox (emails envoyés) 
                $(document).ready(function() {
                     $("a#single_image").fancybox();	
                     /* Using custom settings */
                     $("a.inline").fancybox({
                             "hideOnContentClick": false
                     });
                     /* Apply fancybox to multiple items */
                     $("a.group").fancybox({
                             "transitionIn"	:	"elastic",
                             "transitionOut"	:	"elastic",
                             "speedIn"		:	600, 
                             "speedOut"		:	200, 
                             "overlayShow"	:	false
                     });
                });
            </script>
            <img src="'.$this->_path.'/views/img/link.png" class="icon_top" /> '.$this->l('Désinscription à la newsletter',$page_name).'<br/>
            <a href="#data_emails" class="inline link"><img src="'.$this->_path.'views/img/mini_eye.png" class="icon_middle" /> '.$SupernewsletterStats->nb_unsubscribe.' '.$this->l('désinscriptions',$page_name).' / '.$this->l('voir la liste des désinscrits',$page_name).'</a>
            <div style="display:none"><div id="data_emails">
                '.(empty($SupernewsletterStats->emails_unsubscribe)?$this->l('Aucune adresse email',$page_name):nl2br($SupernewsletterStats->emails_unsubscribe)).'
            </div></div>
        </td>
        <td>';
            if($SupernewsletterStats->nb_unsubscribe>$clicked_links['unsubscribe']['nb_clicks']){
                $this->_html .= $SupernewsletterStats->nb_unsubscribe;
            }else{
                if(empty($clicked_links['unsubscribe']['nb_clicks'])){
                    $this->_html .= '0';
                }else{
                    $this->_html .= $clicked_links['unsubscribe']['nb_clicks'];
                }
            }
            $this->_html .= '
        </td>
    </tr>';
    
$this->_html .= '
</table>';

?>
