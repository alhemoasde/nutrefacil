<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

include(dirname(__FILE__).'/../../config/config.inc.php');

// token secure
$token = Tools::getValue('token');
if($token!=_COOKIE_KEY_){die('Error : bad token');}

require(dirname(__FILE__).'/supernewsletter.php');
$Supernewsletter = new Supernewsletter();
$context = Context::getContext(); 

if(Tools::isSubmit('email')){
    $find = 0;
    $email = Tools::getValue('email');
    
    if(!empty($email)){
        
        // désinscription client enregistré
        $id_customer = Db::getInstance()->getValue('SELECT `id_customer` FROM `'._DB_PREFIX_.'customer` WHERE `email`="'.pSQL($email).'" AND `id_shop`="'.pSQL($context->shop->id).'"');
        if(!empty($id_customer)){
            $Customer = New Customer($id_customer);
            $Customer->newsletter=0;
            $find=1;  
            $Customer->update();
        }
        
        // désinscription inscription manuelle (front)
        $email_ps_newsletter = Db::getInstance()->getValue('SELECT `email` FROM `'._DB_PREFIX_.'newsletter` WHERE `email`="'.pSQL($email).'" AND `id_shop`="'.pSQL($context->shop->id).'"');
        if(!empty($email_ps_newsletter)){
            Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'newsletter` WHERE `email`="'.pSQL($email).'"');
            $find=1; 
        }
        
        // désinscription emails importés (back)
        $email_ps_supernewletter_email = Db::getInstance()->getValue('SELECT `email` FROM `'._DB_PREFIX_.'supernewsletter_email` WHERE `email` LIKE "%'.pSQL($email).'%"');
        if(!empty($email_ps_supernewletter_email)){
            Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'supernewsletter_email` WHERE `email` LIKE "%'.pSQL($email).'%"');
            $find=1; 
        }
        
    }
    
    if(empty($find)){
        $errors[] = stripslashes($Supernewsletter->l('Désolé cette adresse n\'existe pas dans notre base'));
    }else{
        // sauve la statistique
        $id_supernewsletter_content = Tools::getValue('id_supernewsletter_content');
        if(!empty($id_supernewsletter_content)){
            // Modification de la statistique   
            $SupernewsletterStats = new SupernewsletterStats($id_supernewsletter_content);
            $SupernewsletterStats->nb_unsubscribe = $SupernewsletterStats->nb_unsubscribe+1;
            $SupernewsletterStats->emails_unsubscribe = $SupernewsletterStats->emails_unsubscribe.$email."\r\n";
            $SupernewsletterStats->update();
        }
        $success = $Supernewsletter->l('Merci, à l\'avenir vous ne recevrez plus notre newsletter');
    }
}

$smarty = $context->smarty;
$html = '';

$controller=new FrontController();
$controller->init();
$controller->setMedia();
@$controller->displayHeader();

$smarty->assign('ps_version',Tools::substr(_PS_VERSION_,0,3));
$smarty->assign('errors',(isset($errors)?$errors:''));
$smarty->assign('success',(isset($success)?$success:''));

$html.=$Supernewsletter->display(dirname(__FILE__),'views/templates/front/front_unsubscribe.tpl');
echo $html;

@$controller->displayFooter();

?>