<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

// mise à jour

// -------------------
// V1.2.0 
// -------------------

// ajout largeur perso. // theme
$column = Db::getInstance()->ExecuteS('SHOW COLUMNS FROM `'._DB_PREFIX_.'supernewsletter_template` LIKE "width"');
if(empty($column)){
    Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'supernewsletter_template` ADD `width` INT NOT NULL AFTER `name`'); 
    Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'supernewsletter_template` SET `width`=500'); 
}

// ajout fond body perso. // theme
$column = Db::getInstance()->ExecuteS('SHOW COLUMNS FROM `'._DB_PREFIX_.'supernewsletter_template` LIKE "bg_body"');
if(empty($column)){
    Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'supernewsletter_template` ADD `bg_body` VARCHAR( 7 ) NOT NULL AFTER `width`'); 
    Db::getInstance()->Execute('UPDATE `'._DB_PREFIX_.'supernewsletter_template` SET `bg_body`="#ffffff"'); 
}

// ajout option client désactivé // news edit
$column = Db::getInstance()->ExecuteS('SHOW COLUMNS FROM `'._DB_PREFIX_.'supernewsletter_content` LIKE "cust_disabled"');
if(empty($column)){
    Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'supernewsletter_content` ADD `cust_disabled` BOOLEAN NOT NULL AFTER `ids_groups`'); 
}

// ajout option client non-abonné // news edit
$column = Db::getInstance()->ExecuteS('SHOW COLUMNS FROM `'._DB_PREFIX_.'supernewsletter_content` LIKE "cust_no_news"');
if(empty($column)){
    Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'supernewsletter_content` ADD `cust_no_news` BOOLEAN NOT NULL AFTER `cust_disabled`'); 
}

// ajout option client supprimé  // news edit
$column = Db::getInstance()->ExecuteS('SHOW COLUMNS FROM `'._DB_PREFIX_.'supernewsletter_content` LIKE "cust_deleted"');
if(empty($column)){
    Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'supernewsletter_content` ADD `cust_deleted` BOOLEAN NOT NULL AFTER `cust_no_news`');
}

// délai entre les mails
$emails_sleep = Configuration::get('SUPERNEWS_EMAILS_SLEEP');
if(empty($emails_sleep)){
    Configuration::updateValue('SUPERNEWS_EMAILS_SLEEP',0);
}

// -------------------
// V1.2.2
// -------------------

// mise à jour du type de champ (problème pour stockage des données)
$column = Db::getInstance()->ExecuteS('SHOW COLUMNS FROM `'._DB_PREFIX_.'supernewsletter_content` LIKE "emails_sent"');
if(Tools::strtolower($column[0]['Type'])!='longtext'){
    Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'supernewsletter_content`
                               CHANGE `emails_sent` `emails_sent` LONGTEXT CHARACTER 
                               SET latin1 COLLATE latin1_swedish_ci NOT NULL'); 
}

// -------------------
// V1.3.1
// -------------------

// conserve les emails désinscrits
$column = Db::getInstance()->ExecuteS('SHOW COLUMNS FROM `'._DB_PREFIX_.'supernewsletter_stats` LIKE "emails_unsubscribe"');
if(empty($column)){
    Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'supernewsletter_stats` ADD `emails_unsubscribe` LONGTEXT NOT NULL AFTER `nb_unsubscribe`'); 
}

// -------------------
// V1.4.4
// -------------------

// prix par groupe client
$column = Db::getInstance()->ExecuteS('SHOW COLUMNS FROM `'._DB_PREFIX_.'supernewsletter_content` LIKE "id_group_price"');
if(empty($column)){
    Db::getInstance()->Execute('ALTER TABLE `'._DB_PREFIX_.'supernewsletter_content` ADD `id_group_price` INT NOT NULL AFTER `id_lang_default`'); 
}
    
?>