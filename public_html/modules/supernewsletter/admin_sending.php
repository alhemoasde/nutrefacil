<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

if(!defined('_COOKIE_KEY_')){die('token error');}

$helper = new HelperForm(); 
$this->context->controller->addJqueryPlugin('fancybox');

$id_supernewsletter_content = Tools::getValue('id_supernewsletter_content');
$SupernewsletterContentCustomLoad = new SupernewsletterContent($id_supernewsletter_content);

if(empty($id_supernewsletter_content)){$id_supernewsletter_content=ToolClass::maxID(_DB_PREFIX_.'supernewsletter_content','id_supernewsletter_content','`id_shop`="'.pSQL($id_shop).'"');}

// Toolbar
$url_generate_newsletter = $Shop->getBaseURL().'modules/supernewsletter/front_generate_newsletter.php?id_supernewsletter_content='.$id_supernewsletter_content.'&id_shop='.$this->context->shop->id.'&id_lang='.$id_lang.'&id_currency='.$SupernewsletterContentCustomLoad->id_currency.'&see_online=1&token='.md5($id_supernewsletter_content);
$helper->toolbar_btn['back'] =  array(
    'href' => $current_index,
    'desc' => $this->l('Revenir à la liste des newsletter',$page_name),
    'icon' => 'icon-arrow-left'
);
$helper->toolbar_btn['preview'] = array(
        'short' => 'preview',
        'href' => $url_generate_newsletter.'&preview=1',
        'desc' => $this->l('Prévisualiser',$page_name),
        'target'=>'blank',
        'icon' => 'icon-eye-open'
);
$helper->toolbar_btn['stats'] = array(
        'href' => $current_index.'&stats&id_supernewsletter_content='.$id_supernewsletter_content,
        'desc' => $this->l('Statistiques',$page_name),
        'icon' => 'icon-signal'
);
if($this->ps_version==1.6){$toolbarModule=$this->toolbarModule($helper->toolbar_btn);}else{$toolbarModule='';}

$helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
$helper->allow_employee_form_lang = true;
$helper->toolbar_scroll = true;
$helper->toolbar_btn = $helper->toolbar_btn;
$helper->show_toolbar = true;
$helper->title = $this->l('Envoi de la newsletter',$page_name);
$this->_html .= $toolbarModule.$helper->generateForm(array());
// --

$news = SupernewsletterContent::getNewsletters();
$currentIndex = 'index.php?controller=AdminModules';
$SupernewsletterContent = new SupernewsletterContent($id_supernewsletter_content);

// reset de l'envoi
if(Tools::isSubmit('reset_send_mails')){
    $SupernewsletterContent->statut = 0;
    $SupernewsletterContent->emails_sent = '';
    $SupernewsletterContent->update();
    $this->_html.=$this->displayConfirmation($this->l('Les compteurs ont été mis à zéro, vous pouvez recommencer votre envoi.',$page_name));
}

// comptage des emails
///////////////////////
// clients
$emails_customers = ToolClass::getArrayValues('email',$this->getCustomersEmails($id_supernewsletter_content));
$nb_emails_customers = count($emails_customers);
// inscrits
$emails_subscribes = ToolClass::getArrayValues('email',$this->getSubscribesEmails($SupernewsletterContent->register_front,$id_supernewsletter_content));
$nb_emails_subscribes = count($emails_subscribes);
// imports
$emails_import = ToolClass::getArrayValues('email',$this->getImportEmails(unserialize($SupernewsletterContent->groups)));
$nb_emails_import = count($emails_import);

// calculations globale
$emails_sent = @unserialize($SupernewsletterContent->emails_sent);
if(empty($emails_sent)){$emails_sent=array();}
$emails_with_duplicate = @array_merge($emails_customers,$emails_subscribes,$emails_import,$this->webbax_emails_debug);
$nb_emails_with_duplicate = count($emails_with_duplicate);
$emails_unique = @array_unique($emails_with_duplicate);
$nb_emails_unique = count($emails_unique);
$total_emails = $nb_emails_unique;

$shop_domain = Configuration::get('PS_SHOP_DOMAIN');
$ssl = Configuration::get('PS_SSL_ENABLED');
$base_url_ajax = $Shop->getBaseURL();
if($ssl && Tools::strpos('https',$base_url_ajax)===false){
    $base_url_ajax = str_replace('http','https',$base_url_ajax);
}

// Progressbar
$this->_html .= '
<script type="text/javascript">
    
   // Fancybox (emails envoyés) 
   $(document).ready(function() {
            $("a#single_image").fancybox();	
            /* Using custom settings */

            $("a.inline").fancybox({
                    "hideOnContentClick": false
            });
            /* Apply fancybox to multiple items */
            $("a.group").fancybox({
                    "transitionIn"	:	"elastic",
                    "transitionOut"	:	"elastic",
                    "speedIn"		:	600, 
                    "speedOut"		:	200, 
                    "overlayShow"	:	false
            });
   });
    
   $(document).ready(function () {  
        $("#progressBar").css("display","none");
        $("#stop_sending").css("display","none");    
        $("#success_msg").css("display","none");            
   });

   function ajaxSendPart(){
        $.ajax({
              url: "'.$base_url_ajax.'modules/'.$this->name.'/front_cron_send.php?identifier=id_supernewsletter_content&identifier_value='.$id_supernewsletter_content.'&emails_pack='.Configuration::get('SUPERNEWS_EMAILS_PACK').'&id_shop='.$this->context->shop->id.'",
              dataType: "json",
              error: function(jqXHR, textStatus, errorThrown){
                            alert(textStatus+" "+errorThrown);
              },
              success:  function(data){
                                
                            //debug - Webbax
                            /*
                            alert("emails_sent : "+data.nb_emails_sent);
                            alert("total_emails : "+'.$total_emails.');
                            var perc = (data.nb_emails_sent/'.$total_emails.')*100;
                            alert("percent : "+perc);
                            */

                            progress(data.progressbar_percent,$("#progressBar"));
                            if(data.nb_emails_sent<'.$total_emails.'){
                                ajaxSendPart();
                            }else{
                                $("#stop_sending").css("display","none")
                                $("#success_msg").css("display","block");  
                            }
                            $("#nb_emails_sent").html(data.nb_emails_sent);
              },
              complete:function(data){}
      });
    }

    function progress(percent, $element) {
        var progressBarWidth = percent*$element.width()/100;
        $element.find("div").animate({width:progressBarWidth},"fast").html(percent+"%&nbsp;");
    }
    
    function send_newsletters(){   
        
        $("#panel").css("display","none");
        $("#progressBar").css("display","block");
        $("#stop_sending").css("display","block");

        progress(0,$("#progressBar"));
        ajaxSendPart();

        return false;        
    }

</script>';  

$this->_html .= '
<div id="progressBar" style="display:none;">
    <div></div>
    <span id="nb_emails_sent">0</span><span id="separator">/</span><span id="total_emails">'.$total_emails.'</span>
</div>   
<div id="stop_sending">
    <a href="'.$_SERVER['REQUEST_URI'].'"><img src="'.$this->_path.'views/img/cancel.png" class="icon_top" /> '.$this->l('Stopper l\'envoi en cours',$page_name).'</a>
</div> 
<div id="success_msg" class="module_confirmation conf confirm alert alert-success">'.
    $this->l('Tous les emails ont été envoyés',$page_name).'<br/>
    <img src="'.$this->_path.'views/img/arrow_left.png" class="icon_top" /> <a href="'.$_SERVER['REQUEST_URI'].'" class="link">'.$this->l('Revenir à la newsletter et consulter le rapport d\'envoi',$page_name).'</a><br/>
</div>

<fieldset id="panel">
<label>'.$this->l('Sélectionnez la newsletter',$page_name).'</label>
<select id="id_supernewsletter_content" name="id_supernewsletter_content" onchange="if(this.value) window.location.href=this.value">
    <option value="" />'.$this->l('Sélectionner',$page_name);
    foreach($news as $n){
        if($id_supernewsletter_content==$n['id_supernewsletter_content']){$selected='selected';}else{$selected='';}
        $this->_html .= '
        <option value="'.$currentIndex.'&token='.Tools::getValue('token').'&configure=supernewsletter&sending&id_supernewsletter_content='.$n['id_supernewsletter_content'].'" '.$selected.'>'.$n['title'];
    }
$this->_html .= '
</select>';

$this->_html .= '
<br/><br/>
<table class="table" id="table_emails" cellspacing="0" cellpadding="0">
    <tr><th colspan="2">'.$this->l('Clients sélectionnés pour l\'envoi',$page_name).'</th></tr>';
        
    // client avec compte
    $this->_html .= '
    <tr><td>'.$this->l('Clients avec compte',$page_name).' : <br/>';
        $ids_groups = unserialize($SupernewsletterContent->ids_groups);
        if(!empty($ids_groups)){
            $this->_html .= '
            <span class="important">'.$this->l('Groupe(s)',$page_name).' : </span>';
            $list_groups = '<i>';
            foreach($ids_groups as $id){
                $Group = new Group($id);
                $list_groups .= $Group->name[$id_lang].',';
            }
            $list_groups = Tools::substr($list_groups,0,-1);
            $list_groups.='</i>';
            $this->_html.=$list_groups;
        }
    $this->_html .= '
    </td><td>';
    if(!empty($ids_groups)){
        $this->_html.= '<img src="'.$this->_path.'views/img/user.png" class="icon_top" /> '.$nb_emails_customers.' '.$this->l('email(s)',$page_name);
    }else{
        $this->_html.= '<img src="'.$this->_path.'views/img/cross.png" class="icon_top" /> '.$this->l('ne pas envoyer',$page_name);
    }     
    $this->_html .= '
    </td></tr>';
    
    // inscrits via le front
    $this->_html .= '
    <tr>
        <td>'.$this->l('Clients inscrits via le front-office',$page_name).' : </td>
        <td>';
            if($SupernewsletterContent->register_front){
                $this->_html.= '<img src="'.$this->_path.'views/img/user.png'.'" class="icon_top" /> '.$nb_emails_subscribes.' '.$this->l('email(s)',$page_name);
            }else{
                $this->_html.= '<img src="'.$this->_path.'views/img/cross.png'.'" class="icon_top" /> '.$this->l('ne pas envoyer',$page_name);
            }
        $this->_html .= '
        </td>
     </tr>';
    
    // emails importés
    $this->_html .= '
    <tr>
        <td>'.$this->l('Emails importés',$page_name).' : <br/>';
            $groups = unserialize($SupernewsletterContent->groups);
            if(!empty($groups)){
                $this->_html .= '
                <span class="important">'.$this->l('Groupe(s)',$page_name).' : </span>';
                $list_groups = '<i>';
                foreach($groups as $name){
                    if($name=='no_group'){$name=$this->l('Aucun groupe',$page_name);}
                    $list_groups .= $name.',';
                }
                $list_groups = Tools::substr($list_groups,0,-1);
                $list_groups.='</i>';
                $this->_html.=$list_groups;
            }
            $this->_html .= '
        </td>
        <td>';
            if(!empty($groups)){
                $this->_html.='<img src="'.$this->_path.'views/img/user.png" class="icon_top" /> '.$nb_emails_import.' '.$this->l('email(s)',$page_name);
            }else{
                $this->_html.= '<img src="'.$this->_path.'views/img/cross.png" class="icon_top" /> '.$this->l('ne pas envoyer',$page_name);
            }
         $this->_html.= '
        </td>       
    </tr>';
         
    // email debug
    if($this->webbax_debug){
        $this->_html .= '
        <tr>
            <td>'.$this->l('Emails Webbax debug',$page_name).' : </td>
            <td>
                <img src="'.$this->_path.'views/img/user.png" class="icon_top" /> '.count($this->webbax_emails_debug).' '.$this->l('email(s)',$page_name).'
            </td>
        </tr>';
    }
         
    // doublons
    $this->_html .= '
    <tr>
        <td>
            '.$this->l('Emails fusionnés (présents plusieurs fois)',$page_name).' : <br/>';
             $emails_duplicates = ToolClass::getDuplicatesValues($emails_with_duplicate);  
             $list_emails = '<span class="negative_emails"><i>';
             foreach($emails_duplicates as $email){
                 $list_emails.=$email.',';
             }
             $list_emails = Tools::substr($list_emails,0,-1);
             $list_emails .= '</i></span>';
             $this->_html .= $list_emails.'
        </td>
        <td>'.((count($emails_duplicates)==0)?$this->l('Aucune adresse email fusionnée',$page_name):'<img src="'.$this->_path.'views/img/user_delete.png" class="icon_top" /><span class="negative_number">-'.($nb_emails_with_duplicate-$nb_emails_unique).' '.$this->l('email(s)',$page_name).'</span>').'</td>
    </tr>';
         
    // total
    $this->_html .= '
    <tr>
        <td></td><td><img src="'.$this->_path.'views/img/user_red.png" class="icon_top" /><strong>'.$this->l('Total',$page_name).'</strong> : '.$total_emails.' '.$this->l('email(s)',$page_name).'</td>       
    </tr>
    </table>'; 
   
    // check AntiSpam
    if(Tools::isSubmit('submitTestSpam')){   
        
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_HEADER,1); 
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($ch,CURLOPT_URL,$url_generate_newsletter);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_USERAGENT,Configuration::get('PS_SHOP_NAME'));
        curl_setopt($ch,CURLOPT_HEADER,0);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $content = curl_exec($ch);
        
        $header_raw = '';
        $email_sender = Configuration::get('SUPERNEWS_EMAIL_SENDER');
        $headers = MailClass::Send($SupernewsletterContent->title[$id_lang],$content,Configuration::get('PS_SHOP_EMAIL'),null,null,$email_sender,null,null,null,false,null,true);
        // < PS 1.6.1.5
        if(is_array($headers)){
            foreach($headers as $k=>$v){
                if($k=='To'){$v=$headers['To'][0];}
                if($k=='Reply-To'){$v=$headers['To'][0];}
                $header_raw .= $k.': '.$v." \r\n";
            }
        // > PS 1.6.1.5
        }else{
            $header_raw = $headers;
        }
        
        $spamCheck = new PostmarkSpamClass();
        $spamCheck->_Email = $header_raw.$content; 
        $spamCheck->_Option = "short";
        $check = $spamCheck->checkSpam();
      
        if($check['success']==true) {
            if($check['score']<5){
                $statut = '<img src="'.$this->_path.'views/img/ok.png" class="icon_top" />'.
                          '<span class="txt_success bold">'.$this->l('Succès : la newsletter passe le filtre Anti-Spam',$page_name).'</span><br/>'.
                          $this->l('Note',$page_name).' : <strong>'.$check['score'].' / 10</strong><br/>';
            }else{
                $statut = '<img src="'.$this->_path.'views/img/cancel.png" class="icon_top" />'.
                          '<span class="txt_error bold">'.$this->l('Attention : la newsletter ne passe pas le filtre Anti-Spam',$page_name).'</span><br/>'.
                          $this->l('Note',$page_name).' : <strong>'.$check['score'].' / 10</strong><br/>';
            }
        }else{
            $statut = '<span class="txt_error">'.$this->l('Erreur de connexion avec l\'application distante Anti-Spam',$page_name).'</span><br/>';
        }
        
    }else{
        $statut = '';
    }
            
    $this->_html .= '
    <table class="table" id="table_antispam" cellspacing="0" cellpadding="0">
        <tr><th colspan="3" class="center">'.$this->l('Test Anti-Spam',$page_name).'</th></tr>
        <tr>
            <td>
                <table class="table" id="table_score""">
                    <tr><th colspan="3"><center>'.$this->l('Scores').'</center></th></tr>
                    <tr>
                        <td align="center" class="no_border">'.$this->l('inférieur à',$page_name).' 5  = <span class="txt_success">'.$this->l('Valide',$page_name).'</span><br/>'.
                             $this->l('supérieur à',$page_name).' 5 = <span class="txt_error">Spam</span><br/>
                             <div id="note"><img src="'.$this->_path.'views/img/info.png" class="icon_top" /><i>'.$this->l('Note : plus le score est faible, moins le risque de Spam est important',$page_name).'</i></div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center" class="no_border" style="width:100%">
                <form method="post">'.
                    $statut.'
                    <input type="submit" class="button btn btn-default" name="submitTestSpam" value="'.$this->l('Evaluer la note',$page_name).'" />
                </form>
            </td>
        </tr>
    </table>';
    
    // tableau statut
    $this->_html .= '
    <table class="table" id="table_stats" cellspacing="0" cellpadding="0">
        <tr><th colspan="3" class="center">'.$this->l('Rapport d\'envoi',$page_name).'</th></tr>
        <tr>
            <td colspan="3">'.$this->getStatutNewsletter($SupernewsletterContent->statut).'</td>       
        </tr>           
        <tr>
            <td>'.$this->l('Emails à envoyer',$page_name).'</td>
            <td><span class="solde_emails">'.((($total_emails-count($emails_sent))>0)?$total_emails-count($emails_sent):0).'</span></td>
            <td><a href="#data_emails" class="inline link"><img src="'.$this->_path.'views/img/mini_eye.png" class="icon_middle" /> '.$this->l('Voir la liste',$page_name).'</a>
                <div style="display:none"><div id="data_emails">';
                    $lst_emails = '';
                    if(is_array($emails_unique)){
                        foreach($emails_unique as $email){
                            if(!in_array($email,$emails_sent)){
                                $lst_emails .= $email.'<br/>';
                            }
                        } 
                    }
                    if(empty($lst_emails)){$lst_emails=$this->l('Vide',$page_name);}
                $this->_html .= $lst_emails.'
                </div></div>
            </td>
        </tr>
        <tr>
            <td>'.$this->l('Emails envoyés',$page_name).'</td>
            <td><span class="sent_emails">'.count($emails_sent).'</span></td>
            <td><a href="#data_emails_sent" class="inline link"><img src="'.$this->_path.'views/img/mini_eye.png" class="icon_middle" /> '.$this->l('Voir la liste',$page_name).'</a>
                <div style="display:none"><div id="data_emails_sent">';
                    $lst_emails = '';
                    foreach($emails_sent as $email){
                        $lst_emails .= $email.'<br/>';
                    } 
                    if(empty($lst_emails)){$lst_emails=$this->l('Vide',$page_name);}
                $this->_html .= $lst_emails.'
                </div></div>
            </td>
        </tr>
        <tr>
            <td>
                '.$this->l('Technique d\'envoi',$page_name).'
            </td>
            <td colspan="2">
                <img src="'.$this->_path.'views/img/server.png" class="icon_middle" />
                <i>';
                if(Configuration::get('SUPERNEWS_MAIL_METHOD')==2){
                    $this->_html .= 'SMTP via : '.Configuration::get('SUPERNEWS_SMTP_SERVER');
                }else{
                    $this->_html .= 'PHP mail - '.$this->l('depuis ce serveur',$page_name);
                }
                $this->_html .='</i>
            </td>
        </tr>
        <tr>
            <td id="cell_send_mails" colspan="3">';
                if($SupernewsletterContent->statut==0){
                    $this->_html .= '<a href="#" id="send_mails" class="button btn btn-default" onclick="if(confirm(\''.addslashes($this->l('Envoyer la newsletter aux',$page_name).' '.$total_emails.' '.$this->l('emails ?',$page_name)).'\')){javascript:{send_newsletters();}return true;}else{return false;};">'.$this->l('Lancer l\'envoi',$page_name).'</a>';
                }elseif($SupernewsletterContent->statut==1){
                    $this->_html .= '<a href="#" id="send_mails" class="button btn btn-default" onclick="if(confirm(\''.addslashes($this->l('Reprendre l\'envoi de la newsletter ?',$page_name)).'\')){javascript:{send_newsletters();}return true;}else{return false;};">'.$this->l('Reprendre l\'envoi',$page_name).'</a>
                                     <form method="post"><input type="submit" name="reset_send_mails" class="button btn btn-default" onclick="if(confirm(\''.addslashes($this->l('Remettre les compteurs à zéro ?')).'\')){return true;}else{return false;};" value="'.$this->l('Remettre à zéro').'" ></form>';
                }elseif($SupernewsletterContent->statut==2){
                    $this->_html .= '<a href="#" id="send_mails" class="button btn btn-default" onclick="if(confirm(\''.addslashes($this->l('Envoyer à nouveau la newsletter aux',$page_name).' '.$total_emails.' '.$this->l('emails ?',$page_name)).'\')){javascript:{send_newsletters();}return true;}else{return false;};">'.$this->l('Relancer l\'envoi complet',$page_name).'</a>';
                }
            $this->_html .= '
            </td>
        </tr>
    </table>';
    
$this->_html .= '   
</table>
</fieldset>';

?>