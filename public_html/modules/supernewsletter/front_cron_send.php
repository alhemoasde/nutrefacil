<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

// autoriser l'accès Ajax
header('Access-Control-Allow-Origin: *');

include(dirname(__FILE__).'/../../config/config.inc.php');
$controller=new FrontController();
$controller->init();

require_once(dirname(__FILE__).'/supernewsletter.php');
$Supernewsletter = new Supernewsletter();

// vars
$cron = false;
// vars GET
$identifier = Tools::getValue('identifier');
if(empty($identifier)){die('error parameter : identifier');}
$identifier_value = Tools::getValue('identifier_value');
if(empty($identifier_value)){die('error parameter : identifier_value');}
$emails_pack = Tools::getValue('emails_pack');
if(empty($emails_pack)){die('error parameter : emails_pack');}
if($emails_pack=='unlimited'){$emails_pack=100000;}
$id_shop = Tools::getValue('id_shop');
$Shop = new Shop($id_shop);
if(empty($id_shop)){die('error parameter : id_shop');}
$id_lang_default = Configuration::get('PS_LANG_DEFAULT');

$Context = Context::getContext();

// identifier la newsletter
// CRON - mode
if($identifier=='date'){
     $cron = true;
     
     if(Tools::getValue('token')!=_COOKIE_KEY_){die('error : token');}
     
     // change php.ini
     $max_execution_time = Configuration::get('SUPERNEWS_CRON_MAX_EX_TIME');
     $memory_limit = Configuration::get('SUPERNEWS_CRON_MEM_LIMIT');
     if(!empty($max_execution_time)){ini_set('max_execution_time',$max_execution_time);}
     if(!empty($memory_limit)){ini_set('memory_limit',$memory_limit);}
     
     $sql = 'SELECT `id_supernewsletter_content` FROM `'._DB_PREFIX_.'supernewsletter_content` WHERE `date`="'.pSQL(date('Y-m-d')).'" AND `id_shop`="'.pSQL($id_shop).'" ORDER BY `id_supernewsletter_content` DESC';
     $id_supernewsletter_content = Db::getInstance()->getValue($sql);
     
     // aucune newsletter trouvée ?
     if(empty($id_supernewsletter_content)){die('alert : no newsletter');}
     $SupernewsletterContent = new SupernewsletterContent($id_supernewsletter_content);
     // vérifie l'état du cron
     if($SupernewsletterContent->cron!=1){die('alert : cron disabled');}
     
// backend send - mode
}else{
     $id_supernewsletter_content = $identifier_value;
     $SupernewsletterContent = new SupernewsletterContent($id_supernewsletter_content);
}

// Vérifie le statut de la newsletter
// si c'est un renvoi (mis à zéro du statut + historique)
if($SupernewsletterContent->statut==2){
    // stats content
    $SupernewsletterContent = new SupernewsletterContent($id_supernewsletter_content);
    $SupernewsletterContent->statut=0;
    $SupernewsletterContent->emails_sent='';
    $SupernewsletterContent->update();
    // stats
    $SupernewsletterStats = new SupernewsletterStats($id_supernewsletter_content);
    $SupernewsletterStats->open_ip = '';
    $SupernewsletterStats->redirect_ip = '';
    $SupernewsletterStats->clicked_products = '';
    $SupernewsletterStats->clicked_links = '';
    $SupernewsletterStats->nb_unsubscribe = '';
    $SupernewsletterStats->update();
}

$shop_domain = Configuration::get('PS_SHOP_DOMAIN');
$ssl = Configuration::get('PS_SSL_ENABLED');
$base_url = $Shop->getBaseURL();
if($ssl && Tools::strpos('https',$base_url)===false){
    $base_url = str_replace('http','https',$base_url);
}

// Préparation des newsletter
$langs = Language::getLanguages();
foreach($langs as $lang){
    // stockage en cookie les newsletters dans les différentes langues
    $field_name = 'newsletter_content_id_lang_'.$lang['id_lang'];
    if(empty($Context->cookie->$field_name)){
        $url_generate_newsletter = $base_url.'modules/supernewsletter/front_generate_newsletter.php?id_supernewsletter_content='.$id_supernewsletter_content.'&id_lang='.$lang['id_lang'].'&id_currency='.$SupernewsletterContent->id_currency.'&id_shop='.$id_shop.'&token='.md5($id_supernewsletter_content);
        // curl
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_HEADER,1); 
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,0);
        curl_setopt($ch,CURLOPT_URL,$url_generate_newsletter);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch,CURLOPT_USERAGENT,Configuration::get('PS_SHOP_NAME'));
        curl_setopt($ch,CURLOPT_HEADER,0);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
        $content = curl_exec($ch);
        // error ?
        if(curl_errno($ch)){
            Tools::d('Curl error : '.curl_error($ch));
        }  
        curl_close($ch);
        //$Context->cookie->$field_name = $content;
        // 06.02.17 - Webbax - retrait cookie
        ${$field_name} = $content;
    }
}

$Supernewsletter = new Supernewsletter();
///////////////////////
// comptage des emails
///////////////////////
// clients
$emails_customers = ToolClass::getArrayValues('email',$Supernewsletter->getCustomersEmails($id_supernewsletter_content));
if(!is_array($emails_customers) && !empty($emails_customers)){$emails_customers=array($emails_customers);}
// inscrits
$emails_subscribes = ToolClass::getArrayValues('email',$Supernewsletter->getSubscribesEmails($SupernewsletterContent->register_front,$id_supernewsletter_content));
if(!is_array($emails_subscribes) && !empty($emails_subscribes)){$emails_subscribes=array($emails_subscribes);}
// imports
$emails_import = ToolClass::getArrayValues('email',$Supernewsletter->getImportEmails(unserialize($SupernewsletterContent->groups)));
if(!is_array($emails_import) && !empty($emails_import)){$emails_import=array($emails_import);}
// emails debug
if($Supernewsletter->webbax_debug){$emails_debug=$Supernewsletter->webbax_emails_debug;}else{$emails_debug=array();}
// emails à envoyer
$emails = array_unique(array_merge($emails_customers,$emails_subscribes,$emails_import,$emails_debug));

$emails_sent = unserialize($SupernewsletterContent->emails_sent);
if(empty($emails_sent)){$emails_sent=array();}
$emails_add_history = array();

$email_sender = Configuration::get('SUPERNEWS_EMAIL_SENDER');
$email_bounces = Configuration::get('SUPERNEWS_EMAIL_BOUNCES');

// 1 seule adresse disponible ?
foreach($emails as $email){
    if(!in_array($email,$emails_sent)){
        
        // contrôle la validité de l'email
        $email = $Supernewsletter->cleanEmail($email);
        
        // php vérifie la langue pour chaque email
        // check dans ps_customer  
        $is_customer=false;
        $column = Db::getInstance()->ExecuteS('SHOW COLUMNS FROM `'._DB_PREFIX_.'customer` LIKE "id_lang"');      
        if(!empty($column)){       
            $id_lang = Db::getInstance()->getValue('SELECT `id_lang` FROM `'._DB_PREFIX_.'customer` WHERE `email`="'.pSQL($email).'"');
            if(!empty($id_lang)){$is_customer=true;}
            // check dans ps_supernewsletter_email
            if(empty($id_lang) || $id_lang==0){
                $id_lang = Db::getInstance()->getValue('SELECT `id_lang` FROM `'._DB_PREFIX_.'supernewsletter_email` WHERE `email`="'.pSQL($email).'" AND `id_shop`="'.pSQL($id_shop).'"');
            }
        }
        
        if(empty($id_lang) || $id_lang==0){
            $id_lang = $id_lang_default;
        }
                
        // Webbax - 29.10.13 - force la langue par défaut
        if($SupernewsletterContent->id_lang_default!=9999){$id_lang=$SupernewsletterContent->id_lang_default;}
        $newsletter_content_id_lang_xx = 'newsletter_content_id_lang_'.$id_lang;
        
        //$content = $Context->cookie->$newsletter_content_id_lang_xx;
        // 06.02.17 - Webbax - retrait cookie
        $content = ${'newsletter_content_id_lang_'.$id_lang};

        // remplacement des alias par le contenu de la variable 
        $aliases = $Supernewsletter->getAlias();
        if($is_customer){
            $id_customer = Db::getInstance()->getValue('SELECT `id_customer` FROM `'._DB_PREFIX_.'customer` WHERE `email`="'.pSQL($email).'"');
            $Customer = new Customer($id_customer);
            foreach($aliases as $alias){
                $content = @str_replace('{'.$alias['field_name'].'}',$Customer->$alias['field_name'],$content);
            }
        }else{
            foreach($aliases as $alias){
                $content = @str_replace('{'.$alias['field_name'].'}','',$content);
            }
        }
		
        if(in_array($email,$emails_add_history)){break;} // saute les emails (doublons) déjà présents dans le pack
      	
        // envoie le mail
        if(!$Supernewsletter->webbax_debug){
            
            if(Validate::isEmail($email)){
                // envoi réel
                if(!empty($email_bounces)){
                    MailClass::Send($SupernewsletterContent->title[$id_lang],$content,$email,$email_bounces,null,$email_sender);
                }else{
                    MailClass::Send($SupernewsletterContent->title[$id_lang],$content,$email,null,null,$email_sender);
                }
            }  

        }else{
           // envoi test (sur 1 seul mail) pour éviter le spam
           if(in_array($email,$emails_debug)){
               MailClass::Send($SupernewsletterContent->title[$id_lang],$content,$email,$email_bounces,null,$email_sender);
           }
        }
        
        // fait une pause entre l'envoie des mails
        $emails_sleep = Configuration::get('SUPERNEWS_EMAILS_SLEEP');
        if(!empty($emails_sleep)){
            sleep($emails_sleep);
        }
        
        // met dans l'historique l'email
        $emails_add_history[] = $email;
        if(count($emails_add_history)>=$emails_pack || 
		   (count(array_merge($emails_sent,$emails_add_history))>=count($emails))){
		   break;
		}

    }  
}

$emails_sent = array_merge($emails_sent,$emails_add_history);
$SupernewsletterContent->emails_sent = serialize($emails_sent);
$SupernewsletterContent->update();

// Ajuste la progressbar
$progressbar_percent = round((count($emails_sent)/count($emails))*100);
if(count($emails_sent)>=count($emails)){
    $last_pack = true;
    $progressbar_percent=100;
}else{
    $last_pack = false;
}

// Dernier passage ? Maj. statut
if(!$last_pack){ // en cours d'envoi
    $SupernewsletterContent = new SupernewsletterContent($id_supernewsletter_content);
    $SupernewsletterContent->statut = 1;
    $SupernewsletterContent->update();
}else{ // envoyé
    $SupernewsletterContent = new SupernewsletterContent($id_supernewsletter_content);
    $SupernewsletterContent->statut = 2;
    $SupernewsletterContent->update();
    // vidage du cookie
    foreach($langs as $lang){
        $field_name = 'newsletter_content_id_lang_'.$lang['id_lang'];
        unset($Context->cookie->$field_name);
    }
}

if(!$cron){
    
echo Tools::jsonEncode(array('nb_emails_sent'=>count($emails_sent),
                       'progressbar_percent'=>$progressbar_percent,
                       ));
}else{
    die('cron success');
}

?>