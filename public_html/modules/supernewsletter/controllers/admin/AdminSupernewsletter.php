<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

class AdminSupernewsletterController extends ModuleAdminController {
    
    public $template = 'content.tpl';
    public function __construct(){
        
        $this->table = 'supernewsletter_content';
        $this->className = 'Supernewsletter';
        
        parent::__construct();
            if(!$this->module->active)
                Tools::redirectAdmin($this->context->link->getAdminLink('AdminHome'));
            
        // redirige dans la configuration du module
        $Controller = new Supernewsletter();
        $current_index = 'index.php?controller=AdminModules';
        $token = Tools::getAdminTokenLite('AdminModules');
        $url_mod_token = $current_index.'&configure='.$Controller->name.'&token='.$token;
        Tools::redirectAdmin($url_mod_token);
    }

}

?>
