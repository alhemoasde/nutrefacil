<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

if(!defined('_COOKIE_KEY_')){die('token error');}

$this->fields_list = 
array(
    'id_supernewsletter_template' => array(
            'title' => $this->l('ID',$page_name),
            'width' => 120,
            'type' => 'text',
    ),
    'name' => array(
            'title' => $this->l('Nom',$page_name),
            'width' => 140,
            'type' => 'text',
            'callback' => 'admin_templates_colPreview', 
            'callback_object' => $this,
    ),
 );

$helper = new HelperList();

// Toolbar
$helper->toolbar_btn['back'] =  array(
    'href' => $current_index,
    'desc' => $this->l('Revenir à la liste des newsletter',$page_name),
     'icon' => 'icon-arrow-left'
);
$helper->toolbar_btn['new'] = array(
    'href' => $current_index.'&updatesupernewsletter_template',
    'desc' => $this->l('Créer un nouveau thème',$page_name),
    'icon' => 'icon-plus-sign'
);

$helper->shopLinkType = '';
$helper->simple_header = true;
$helper->identifier = 'id_supernewsletter_template';
$helper->actions = array('edit','duplicate','delete');
$helper->show_toolbar = true;
$helper->imageType = 'jpg';
$helper->title = $this->l('Liste des thèmes',$page_name);
$helper->table = 'supernewsletter_template';
$helper->token = Tools::getAdminTokenLite('AdminModules');
$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;

if($this->ps_version==1.6){$toolbarModule=$this->toolbarModule($helper);}else{$toolbarModule='';}

// dupliquer la newsletter
if(Tools::isSubmit('duplicatesupernewsletter_template')){
    // copy db
    $id_supernewsletter_template = Tools::getValue('id_supernewsletter_template');
    $SupernewsletterTemplate = new SupernewsletterTemplate($id_supernewsletter_template);
    $O = $SupernewsletterTemplate->duplicateObject();
    $data = array('name'=>$this->l('Copie de',$page_name).' '.$O->name);
    Db::getInstance()->autoExecute(_DB_PREFIX_.'supernewsletter_template',$data,'UPDATE','id_supernewsletter_template='.pSQL($O->id));
    // copy files
    $src = dirname(__FILE__).'/templates/'.$id_supernewsletter_template;
    $dst = dirname(__FILE__).'/templates/'.$O->id;
    $statut = ToolClass::copyFolder($src,$dst);
    if($statut=='error'){$this->_html = $this->displayError($this->l('Impossible de copier le thème vers l\'emplacement indiqué, veuillez vérifier que le CHMOD est bien en 777 sur',$page_name).' : '.dirname(__FILE__).'/templates/');}
}

// suppression newsletter 
if(Tools::isSubmit('deletesupernewsletter_template')){
    // del db
    $id = Tools::getValue('id_supernewsletter_template');
    $SupernewsletterTemplate = new SupernewsletterTemplate($id);
    $SupernewsletterTemplate->delete();
    // del files
    @Tools::deleteDirectory(dirname(__FILE__).'/templates/'.$id);
    $this->_html .= $this->displayConfirmation('Suppression effectuée');
}

$templates = SupernewsletterTemplate::getTemplates();
$this->_html .= '
<div id="admin_templates">
    <div class="info alert alert-info">
        <span><img src="'.$this->_path.'views/img/picture_save.png" /> <a href="http://www.webbax.ch/shop/download_public/supernewsletter/supernewsletter_psd.zip" target="_blank">'.$this->l('Télécharger les thèmes au format PSD',$page_name).' ('.$this->l('mot de passe',$page_name).' : <strong>"webbaxSupernewsletter"</strong>)</a></span>
    </div>'.
    $toolbarModule.
    $helper->generateList($templates,$this->fields_list).'
</div>';
        
?>
