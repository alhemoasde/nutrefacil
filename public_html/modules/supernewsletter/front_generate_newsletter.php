<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

// autoriser l'accès Ajax
header('Access-Control-Allow-Origin: *');

include(dirname(__FILE__).'/../../config/config.inc.php');
$controller=new FrontController();

$Context = Context::getContext();
$cookie = $Context->cookie;
$cookie->id_currency = Tools::getValue('id_currency');

// assure l'envoi même en maintenance
if(!(int)Configuration::get('PS_SHOP_ENABLE')){
    $ips = @explode(',', Configuration::get('PS_MAINTENANCE_IP'));
    if(is_array($ips)){$_SERVER['REMOTE_ADDR'] = $ips[0];}
}

$controller->init();
require_once(dirname(__FILE__).'/supernewsletter.php');
require_once(dirname(__FILE__).'/classes/ToolClass.php');
$Supernewsletter = new Supernewsletter();

$token = Tools::getValue('token');
$id_supernewsletter_content = Tools::getValue('id_supernewsletter_content');
$id_supernewsletter_template = Tools::getValue('id_supernewsletter_template');
$id_lang = Tools::getValue('id_lang');
$preview = Tools::getValue('preview');

$id_shop = Tools::getValue('id_shop');
if(empty($id_shop)){
    $Context = Context::getContext();
    $id_shop = $Context->shop->id;
}
$Shop = new Shop($id_shop);

$see_online = Tools::getValue('see_online');
$filename = 'front_generate_newsletter'; // utilisé pour les traductions
$ext_imgs = array('jpg', 'gif', 'jpeg', 'png');
        
// document en général
$html = '
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html>';

if($token==md5($id_supernewsletter_content)){
    
    $SupernewsletterContent = new SupernewsletterContent($id_supernewsletter_content);
    // force avec un template spécifique
    if(empty($id_supernewsletter_template)){
        $id_supernewsletter_template=$SupernewsletterContent->id_supernewsletter_template;
    }else{
        $SupernewsletterContent->id_supernewsletter_template = $id_supernewsletter_template;
    }
    $SupernewsletterTemplate = new SupernewsletterTemplate($id_supernewsletter_template);
    
    // constant
    $width_page = $SupernewsletterTemplate->width; // px
    // css
    $cellspacing_cellpadding = 'border="0" cellspacing="0" cellpadding="0"';
    $css_font_family = 'font-family:'.$SupernewsletterTemplate->police.';font-size:'.$SupernewsletterTemplate->police_size.'px';
    
    // title
    $html.= '
    <head>
        <title>'.$SupernewsletterContent->title[$id_lang].'</title>
    </head>
    <body>';
    
    // centre la newsletter (start)
    if($see_online){$html.='<div align="center">';}
    
    // tracking stats - open newsletter ?
    if($see_online!=1){
        $html.='<img src="'.$Shop->getBaseURL().'modules/supernewsletter/front_stats.php?id_supernewsletter_content='.$id_supernewsletter_content.'&preview='.$preview.'&stats_type=open&token='._COOKIE_KEY_.'" style="height:1px;width:1px">';
    }
    
    // preview ?
    if($preview){    
       $html.='
       <table '.$cellspacing_cellpadding.' style="width:'.$width_page.'px;border:1px solid #e7e7e7;">
           <tr style="background-color:#F8F8F8;">
            <td style="'.$css_font_family.';padding:5px">
                <img src="'.$Shop->getBaseURL().'modules/supernewsletter/views/img/eye.png" style="vertical-align:middle" /> <strong>'.$Supernewsletter->l('Mode prévisualisation',$filename).'</strong>
            </td>
            <td style="'.$css_font_family.';padding-left:10px;padding:5px">'; 
                $html.= '<table><tr><td>';
                $html.= $Supernewsletter->l('Langue en cours',$filename).' : ';
                $html.= '</td><td>';
                    $langs = Language::getLanguages();
                    $html.= '<a href="'.$_SERVER['REQUEST_URI'].'"><img src="'.$Shop->getBaseURL().'img/l/'.$id_lang.'.jpg" /></a><br/>';
                $html.='</td></tr>
                        <tr><td>';
                $html.= $Supernewsletter->l('Changer de langue',$filename).' : ';
                $html.= '</td><td>';
                foreach($langs as $l){
                   $uri = str_replace('id_lang='.$id_lang,'id_lang='.$l['id_lang'],$_SERVER['REQUEST_URI']);
                   $html.= '<a href="'.Tools::getHttpHost(true).$uri.'"><img src="'.$Shop->getBaseURL().'img/l/'.$l['id_lang'].'.jpg" /></a> ';
                }
                $html.='</td></tr></table>';
            $html.=
            '</td>
           </tr>
       </table>';
    }
     
    $base_special_link = $Shop->getBaseURL().'modules/supernewsletter/front_stats.php?id_supernewsletter_content='.$id_supernewsletter_content.'&id_lang='.$id_lang.'&preview='.$preview.'&stats_type=special_link&token='._COOKIE_KEY_;   
    
    // see online newsletter
    $url_newsletter = urlencode($Shop->getBaseURL().'modules/supernewsletter/front_generate_newsletter.php?id_supernewsletter_content='.$id_supernewsletter_content.'&id_lang='.$id_lang.'&preview=0&see_online=1&token='.md5($id_supernewsletter_content));   
    $html .= '<table style="width:100%;background-color:'.$SupernewsletterTemplate->bg_newsletter.';"><tr><td style="text-align:center;'.$css_font_family.';padding-bottom:10px;"><a href="'.$base_special_link.'&id_shop='.$Shop->id.'&link_type=view_online&link_redirect='.$url_newsletter.'" style="color:'.$SupernewsletterTemplate->col_links_hf.';font-size:'.$SupernewsletterTemplate->size_links_hf.'px" target="_blank">'.stripslashes($Supernewsletter->l("Cliquez ici si la newsletter ne s'affiche pas correctement",$filename)).'</a></td></tr></table>';
   
    
    // mail center (start)
    $html .= '<table '.$cellspacing_cellpadding.' style="width:100%;"><tr><td align="center" style="background-color:'.$SupernewsletterTemplate->bg_body.'">';
    // encaps. page (start)
    $html .= '
    <table '.$cellspacing_cellpadding.' id="page_content" style="width:'.$width_page.'px;"><tr><td>';
    
    // encaps content (start)
    $html .= '<table '.$cellspacing_cellpadding.' style="background-color:'.$SupernewsletterTemplate->bg_newsletter.';width:100%;">';
    
    // img header
    foreach($ext_imgs as $ext_img){
        if(file_exists(dirname(__FILE__).'/templates/'.$SupernewsletterContent->id_supernewsletter_template.'/img_header.'.$ext_img)){
            $html .= '
            <tr><td>';
                $img='<img src="'.$Shop->getBaseURL().'modules/supernewsletter/templates/'.$SupernewsletterContent->id_supernewsletter_template.'/img_header.'.$ext_img.'" style="display:block;" />';
                if(empty($SupernewsletterTemplate->link_img_header)){
                    $html .= $img;
                }else{
                    $html .= '<a href="'.$base_special_link.'&link_type=img_header&link_redirect='.$SupernewsletterTemplate->link_img_header.'" target="_blank">'.$img.'</a>'; 
                }
            $html .= '
            </td></tr>';
            break;
        }
    }
    
    // logo & content
    $html .= 
    '<tr><td style="'.$css_font_family.';padding:10px 10px 0 10px;border:'.$SupernewsletterTemplate->bdr_width_content.'px solid '.$SupernewsletterTemplate->bdr_content.'">';
        if($SupernewsletterTemplate->logo==1){
            $html .= 
            '<table '.$cellspacing_cellpadding.' style="width:100%;">
                <tr><td style="width:100%;text-align:'.$SupernewsletterTemplate->logo_position.';">';
                    if(file_exists(_PS_IMG_DIR_.Configuration::get('PS_LOGO'))){
                        $img='<img src="'.$Shop->getBaseURL().'img/'.Configuration::get('PS_LOGO').'" />';
                    }
                $html .= '<a href="'.$base_special_link.'&link_type=logo&link_redirect='.$Shop->getBaseURL().'" target="_blank">'.$img.'</a> 
                </td></tr>
            </table>';
        }
        $html .= '<div style="color:'.$SupernewsletterTemplate->col_content.';font-size:'.$SupernewsletterTemplate->size_content.'px;'.(($SupernewsletterTemplate->bold_content)?'font-weight:bold;':'').'">'.$SupernewsletterContent->content[$id_lang].'</div>
    </td></tr>';
             
    // img products list
    foreach($ext_imgs as $ext_img){    
        if(file_exists(dirname(__FILE__).'/templates/'.$SupernewsletterContent->id_supernewsletter_template.'/img_headerproducts.'.$ext_img)){
            $exists_headerproducts_img = true;
            $html .= '
            <tr><td style="background-color:'.$SupernewsletterTemplate->bg_product.';">'; 
                $img = '<img src="'.$Shop->getBaseURL().'modules/supernewsletter/templates/'.$SupernewsletterContent->id_supernewsletter_template.'/img_headerproducts.'.$ext_img.'" style="display:block;" />';
                if(empty($SupernewsletterTemplate->link_img_headerproducts)){
                   $html .= $img;
                }else{
                   $html .= '<a href="'.$base_special_link.'&link_type=img_headerproducts&link_redirect='.$SupernewsletterTemplate->link_img_headerproducts.'" target="_blank">'.$img.'</a>'; 
                }
             $html .= '
            </td></tr>';
            break;
        }else{
            $exists_headerproducts_img = false;
        }
    }
    
    // récupère un client lambda (utile pour récupérer la bonne table de prix)
    $ps_customer_group = Configuration::get('PS_CUSTOMER_GROUP');
    $id_customer = Db::getInstance()->getValue('SELECT `id_customer` FROM `'._DB_PREFIX_.'customer` WHERE `active`="1" AND `id_shop`="'.pSQL($id_shop).'"');
    $Context->customer = new Customer($id_customer);
    $Context->customer->id_default_group = $SupernewsletterContent->id_group_price;
    
    // products 
    $products_lst = '';
    $products = unserialize($SupernewsletterContent->products);
    $nb_products = count($products);
    if(!empty($products)){
        
        $product_no = 0;
        foreach($products as $p){  
            
             $Product = new Product($p['id_product'],false,(int)$id_lang);
             $image = Product::getCover($p['id_product']);
             $Link = new Link();
             
             if(Configuration::get('PS_LEGACY_IMAGES')){$id_image_for_link=$p['id_product'].'-'.$image['id_image'];}else{$id_image_for_link=$image['id_image'];}
             if(!empty($image['id_image']) && $image['id_image']!=0){$img_link_princ='http://'.$Link->getImageLink($Product->link_rewrite,$id_image_for_link,$SupernewsletterTemplate->product_img_size);}else{$img_link_princ=$Shop->getBaseURL().'modules/supernewsletter/views/img/no_picture_product.jpg';}

             // simple
             if(empty($p['id_product_attribute'])){
                 
                $name = $Product->name;
                $img_link=$img_link_princ;    
                $price = Product::getPriceStatic($p['id_product'],true,0);          
                $price_without_reduction = Product::getPriceStatic($p['id_product'],true,0,6,null,false,false);
                if($price_without_reduction==$price || $ps_customer_group!==$SupernewsletterContent->id_group_price){$price_without_reduction='';}
                $product_no++;
                
             // declinaison
             }else{ 
                $combArray = array();
                $assocNames = array();
                $combinations = $Product->getAttributeCombinations((int)$id_lang);
                $combinations_images = $Product->getCombinationImages($id_lang);

                foreach ($combinations as $k => $combination)
                        $combArray[$combination['id_product_attribute']][] = array('group' => $combination['group_name'], 'attr' => $combination['attribute_name']);
                foreach ($combArray as $id_product_attribute => $product_attribute)
                {
                    $list = '';
                    foreach ($product_attribute as $attribute)
                            $list .= trim($attribute['group']).' - '.trim($attribute['attr']).', ';
                    $list = rtrim($list, ', ');
                    $assocNames[$id_product_attribute] = $list;

                    // declinaison
                    if($p['id_product_attribute']==$id_product_attribute){
                        $name = $Product->name.' '.$list;
                        $id_image = @$combinations_images[$id_product_attribute][0]['id_image'];
                        if(Configuration::get('PS_LEGACY_IMAGES')){$id_image_for_link=$p['id_product'].'-'.$id_image;}else{$id_image_for_link=$id_image;}
                        if(!empty($id_image) && $id_image!=0){$img_link_decl='http://'.$Link->getImageLink($Product->link_rewrite,$id_image_for_link,$SupernewsletterTemplate->product_img_size);}else{$img_link_decl=$img_link_princ;}
                        $img_link = $img_link_decl;
                        $price = Product::getPriceStatic($p['id_product'],true,$id_product_attribute);
                        $price_without_reduction = Product::getPriceStatic($p['id_product'],true,$id_product_attribute,6,null,false,false);     
                        if($price_without_reduction==$price || $ps_customer_group!==$SupernewsletterContent->id_group_price){$price_without_reduction='';}
                        $product_no++;
                    }
                }
             }
             
             // nom produit réduire ?
             if($SupernewsletterTemplate->product_title_len>0 && Tools::strlen($name)>$SupernewsletterTemplate->product_title_len){
                $name = Tools::substr($name,0,$SupernewsletterTemplate->product_title_len).'...';
             }                       
             
             $link_product = $Shop->getBaseURL().'modules/supernewsletter/front_stats.php?id_supernewsletter_content='.$id_supernewsletter_content.'&id_product='.$p['id_product'].'&id_product_attribute='.$p['id_product_attribute'].'&id_lang='.$id_lang.'&preview='.$preview.'&stats_type=product&token='._COOKIE_KEY_;
             $id_unique_random = uniqid();
             
             $css_td_first_product = '';
             if($product_no==1){
                $css_td_first_product='padding-top:5px;';
                if($exists_headerproducts_img==true){
                    $css_td_first_product.='border-top:'.$SupernewsletterTemplate->bdr_width_block_products.'px solid '.$SupernewsletterTemplate->bdr_block_products.';';
                }
             }
             if($nb_products==$product_no){$css_td_last_product='padding-bottom:10px;border-bottom:1px solid '.$SupernewsletterTemplate->bdr_block_products.';';}else{$css_td_last_product='';}
             
             $products_lst .= '
             <tr id="li_'.$id_unique_random.'">
                 <td style="'.$css_td_first_product.$css_td_last_product.'background-color:'.$SupernewsletterTemplate->bg_product.';border-right:'.$SupernewsletterTemplate->bdr_width_block_products.'px solid '.$SupernewsletterTemplate->bdr_block_products.';border-left:'.$SupernewsletterTemplate->bdr_width_block_products.'px solid '.$SupernewsletterTemplate->bdr_block_products.';">
                    <table style="width:100%">';
                        if($product_no!=1){
                            $products_lst .= '<tr><td colspan="2"><table style="border-top:'.$SupernewsletterTemplate->bdr_width_separator_product.'px solid '.$SupernewsletterTemplate->col_separator_product.';width:100%;"><tr><td></td></tr></table></td></tr>';
                        }
                        $products_lst .= '
                        <tr>
                            <td style="'.$css_font_family.';padding-left:10px;"><table '.$cellspacing_cellpadding.' style="border:'.$SupernewsletterTemplate->bdr_width_img_product.'px solid '.$SupernewsletterTemplate->bdr_img_product.'"><tr><td><a href="'.$link_product.'" target="_blank"><img height="125" width="125" src="'.$img_link.'" /></a></td></tr></table></td>
                            <td style="'.$css_font_family.';padding-left:10px;padding-right:10px;width:100%;">
                                <a href="'.$link_product.'" style="font-size:'.$SupernewsletterTemplate->size_title_product.'px;color:'.$SupernewsletterTemplate->col_title_product.';'.(($SupernewsletterTemplate->bold_title_product)?'font-weight:bold;':'').'text-decoration:none;" target="_blank">'.$name.'</a><br/>
                                <br/>';
                                $product_desc = ToolClass::cleanText($Product->{$SupernewsletterTemplate->product_desc_type},Configuration::get('SUPERNEWS_UTF8ENCODE'));
                                if($product_desc){
                                    if($SupernewsletterTemplate->product_desc_len>0 && Tools::strlen($product_desc)>$SupernewsletterTemplate->product_desc_len){
                                        $product_desc = Tools::substr($product_desc,0,$SupernewsletterTemplate->product_desc_len).'...';
                                    }
                                }
                                $products_lst.= '<span style="color:'.$SupernewsletterTemplate->col_desc_product.';font-size:'.$SupernewsletterTemplate->size_desc_product.'px;'.(($SupernewsletterTemplate->bold_desc_product)?'font-weight:bold;':'').'">'.$product_desc.'</span>
                                <table style="width:100%;padding-top:10px;">
                                    <tr>
                                        <td style="padding-left:'.$SupernewsletterTemplate->pad_lft_btn_view_product.'px">
                                            <table><tr><td style="'.$css_font_family.';padding:2px;background-color:'.$SupernewsletterTemplate->bg_btn_view_product.'; border:1px solid '.$SupernewsletterTemplate->bdr_btn_view_product.';">
                                                <table><tr>
                                                <td>
                                                    <img style="vertical-align:middle;" src="'.$Shop->getBaseURL().'modules/supernewsletter/views/img/more.png" />
                                                </td>
                                                <td style="'.$css_font_family.';">
                                                    <a href="'.$link_product.'" style="color:'.$SupernewsletterTemplate->col_btn_view_product.';font-size:'.$SupernewsletterTemplate->size_btn_view_product.'px;'.(($SupernewsletterTemplate->bold_btn_view_product)?'font-weight:bold;':'').'text-decoration:none;display:block;padding:2px 0px 2px 0px" target="_blank">
                                                        '.$Supernewsletter->l('En savoir +',$filename).'
                                                    </a>
                                                </td>
                                                </tr></table>
                                            </td></tr></table>
                                        </td>
                                        <td style="'.$css_font_family.'" align="right">';
                                            if($SupernewsletterTemplate->product_display_price){
                                                $products_lst.='
                                                <span style="color:'.$SupernewsletterTemplate->col_pricelabel_product.';font-size:'.$SupernewsletterTemplate->size_pricelabel_product.'px;'.(($SupernewsletterTemplate->bold_pricelabel_product)?'font-weight:bold;':'').'">'.$Supernewsletter->l('Prix',$filename).' :  </span>
                                                <span style="color:'.$SupernewsletterTemplate->col_price_product.';font-size:'.$SupernewsletterTemplate->size_price_product.'px;'.(($SupernewsletterTemplate->bold_price_product)?'font-weight:bold;':'').'">'.Tools::displayPrice($price).'</span>';
                                            }
                                            $products_lst.='
                                        </td>
                                    </tr>';
                                   
                                    // promo ?
                                    if(!empty($price_without_reduction) && $SupernewsletterTemplate->product_display_price){
                                        $products_lst.='
                                        <tr>
                                            <td></td>
                                            <td style="text-align:right;">
                                                <table '.$cellspacing_cellpadding.' align="right">
                                                    <tr>
                                                        <td style="'.$css_font_family.'" align="center"><img src="'.$Shop->getBaseURL().'modules/supernewsletter/views/img/sale.png" style="vertical-align:middle" /></td> 
                                                        <td style="'.$css_font_family.'" align="center"><span style="color:'.$SupernewsletterTemplate->col_promo_product.';font-size:'.$SupernewsletterTemplate->size_promo_product.'px;'.(($SupernewsletterTemplate->bold_price_product)?'font-weight:bold;':'').'">'.$Supernewsletter->l('au lieu de',$filename).' : '.Tools::displayPrice($price_without_reduction).'</span></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>';
                                    }
                                    
                                $products_lst.='
                                </table>
                            </td>
                        </tr>
        
                    </table>
                </td>
             </tr>';
        }
    }
    
    $html .= $products_lst;
    
    // encaps. fond (start)
    $html .= '<tr><td>';
    // mail center (end)
    $html .= '</td></tr></table>';
    
    // social 
    $html.='
    <table '.$cellspacing_cellpadding.' style="background-color:'.$SupernewsletterTemplate->bg_newsletter.';">';
        foreach($ext_imgs as $ext_img){
            if(file_exists(dirname(__FILE__).'/templates/'.$SupernewsletterContent->id_supernewsletter_template.'/img_facebook.'.$ext_img)){
                if(!empty($SupernewsletterTemplate->link_facebook)){
                    $link_a_start = '<a href="'.$base_special_link.'&link_type=facebook&link_redirect='.$SupernewsletterTemplate->link_facebook.'" target="_blank">';
                    $link_a_end = '</a>';
                }else{
                    $link_a_start = '';
                    $link_a_end = '';
                }
                $html.='<tr><td style="padding-top:5px;">'.$link_a_start.'<img src="'.$Shop->getBaseURL().'modules/supernewsletter/templates/'.$SupernewsletterContent->id_supernewsletter_template.'/img_facebook.'.$ext_img.'" title="'.$Supernewsletter->l('Devenez fan notre page Facebook',$filename).'" style="display:block;" />'.$link_a_end.'</td></tr>';
                break;
            }
        }
        foreach($ext_imgs as $ext_img){
            if(file_exists(dirname(__FILE__).'/templates/'.$SupernewsletterContent->id_supernewsletter_template.'/img_twitter.'.$ext_img)){
                if(!empty($SupernewsletterTemplate->link_twitter)){
                    $link_a_start = '<a href="'.$base_special_link.'&link_type=twitter&link_redirect='.$SupernewsletterTemplate->link_twitter.'" target="_blank">';
                    $link_a_end = '</a>';
                }else{
                    $link_a_start = '';
                    $link_a_end = '';
                }
                $html.='<tr><td style="padding-top:5px;">'.$link_a_start.'<img src="'.$Shop->getBaseURL().'modules/supernewsletter/templates/'.$SupernewsletterContent->id_supernewsletter_template.'/img_twitter.'.$ext_img.'" title="'.$Supernewsletter->l('Abonnez-vous à notre compte Twitter',$filename).'" style="display:block;" />'.$link_a_end.'</td></tr>'; 
                break;   
            }
        }
        foreach($ext_imgs as $ext_img){
            if(file_exists(dirname(__FILE__).'/templates/'.$SupernewsletterContent->id_supernewsletter_template.'/img_rss.'.$ext_img)){
                if(!empty($SupernewsletterTemplate->link_rss)){
                    $link_a_start = '<a href="'.$base_special_link.'&link_type=rss&link_redirect='.$SupernewsletterTemplate->link_rss.'" target="_blank">';
                    $link_a_end = '</a>';
                }else{
                    $link_a_start = '';
                    $link_a_end = '';
                }
                $html.='<tr><td style="padding-top:5px;">'.$link_a_start.'<img src="'.$Shop->getBaseURL().'modules/supernewsletter/templates/'.$SupernewsletterContent->id_supernewsletter_template.'/img_rss.'.$ext_img.'" title="'.$Supernewsletter->l('Abonnez-vous à notre flux RSS',$filename).'" style="display:block;" />'.$link_a_end.'</td></tr>';
                break;  
            }
        }
    $html.='
    </table>';
    
    // img footer
    foreach($ext_imgs as $ext_img){
        if(file_exists(dirname(__FILE__).'/templates/'.$SupernewsletterContent->id_supernewsletter_template.'/img_footer.'.$ext_img)){
            $html .= 
            '<table '.$cellspacing_cellpadding.' style="background-color:'.$SupernewsletterTemplate->bg_newsletter.';padding-top:5px;">
                <tr><td>';
                    $img = '<img src="'.$Shop->getBaseURL().'modules/supernewsletter/templates/'.$SupernewsletterContent->id_supernewsletter_template.'/img_footer.'.$ext_img.'" style="display:block;" />';
                    if(empty($SupernewsletterTemplate->link_img_footer)){
                       $html .= $img;
                    }else{
                       $html .= '<a href="'.$base_special_link.'&link_type=img_footer&link_redirect='.$SupernewsletterTemplate->link_img_footer.'" target="_blank">'.$img.'</a>'; 
                    }
                 $html .= '
                </td></tr>
            </table>';
        }
    }
    
    // signature
    if(!empty($SupernewsletterTemplate->signing[$id_lang])){
        $html.= '
        <table '.$cellspacing_cellpadding.' style="background-color:'.$SupernewsletterTemplate->bg_newsletter.';width:100%;">
                <tr><td style="'.$css_font_family.'">'.$SupernewsletterTemplate->signing[$id_lang].'</td></tr>
        </table>';
    }
    
   
    // encaps. fond (end)
    $html .= '</td></tr>';
    
    // encaps content (end)
    $html .= '</table>';
    
    // encaps. page (end)
    $html .= '
    </td></tr></table>';
    
    // unsubscribe
    $html .= '<table style="width:100%;background-color:'.$SupernewsletterTemplate->bg_newsletter.';padding-bottom:5px;"><tr><td style="text-align:center;'.$css_font_family.';"><a href="'.$base_special_link.'&link_type=unsubscribe&link_redirect='.urlencode($Shop->getBaseURL().'modules/supernewsletter/front_unsubscribe.php?id_supernewsletter_content='.$SupernewsletterContent->id.'&token='._COOKIE_KEY_).'" target="_blank" style="color:'.$SupernewsletterTemplate->col_links_hf.';font-size:'.$SupernewsletterTemplate->size_links_hf.'px">'.$Supernewsletter->l('Cliquez ici pour vous désinscrire',$filename).'</a><td></tr></table>';
    
}else{
    $html.=$Supernewsletter->l('Hack : jeton incorrect',$filename);
};

// centre la newsletter (end)
if($see_online){$html.='</div>';}


// Webbax - force le Favicon
$html.= "
<script>
    window.onload =function ()
    {
        var link = document.createElement('link');
        link.type = 'image/x-icon';
        link.rel = 'shortcut icon';
        link.href = '".$Shop->getBaseURL()."/img/".Configuration::get('PS_FAVICON')."';
        document.getElementsByTagName('head')[0].appendChild(link);
    }
</script>";

$html .= '
</body>
</html>';

// Webbax - 29.04.16 - patch
$html = str_replace('|','&#124;',$html);

echo $html;

?>