{*
* NOTICE OF LICENSE
*
* Module for Prestashop
*
* 100% Swiss development
* @author Webbax <contact@webbax.ch>
* @copyright -
* @license   -
*}

<div id="front_unsubscribe">
    
    {if $ps_version=='1.6'}
        <style>
            {literal}.breadcrumb {display:none;}{/literal}
        </style>
    {/if}

    <link rel="stylesheet" type="text/css" href="{$base_uri|escape:'htmlall':'UTF-8'}modules/supernewsletter/views/css/front_unsubscribe.css" media="screen" />
    <h1>{l s='Désinscription' mod='supernewsletter'}</h1>

    {if $errors!=''}
        <div class="error alert alert-danger">
            <ol>
            {foreach from=$errors item=error}
                <li>{$error|escape:'htmlall':'UTF-8'}</li>
            {/foreach}
            </ol>
        </div>
    {/if}
    
    {if $success!=''}
        <div class="success alert alert-success">
            {$success|escape:'htmlall':'UTF-8'}
        </div>
    {/if}
    
    <form action="" method="post" class="std">
        <div class="row">
            <div class="col-md-3 col-sm-4 col-xs-12">
                <div class="form-group">
                    <label>{l s='Votre email' mod='supernewsletter'} : </label>
                    <input type="text" name="email" class="text form-control" />
                </div>
                <div class="form-group">
                    <input type="submit" name="" value="{l s='Valider' mod='supernewsletter'}" class="btn btn-default">
                </div>
            </div>
        </div>
    </form>
        
</div>
