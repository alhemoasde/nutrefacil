<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

// Interdiction inclusion
if(!class_exists('Language')){die('Error : no direct access for this file');}

// employé connected - start
$cookie = $this->context->cookie;
if(!empty($cookie->id_employee)){

    // vars
    $langs = Language::getLanguages();
    $id_shop = Shop::getContextShopID();

    // -----------------------------
    // Structure 
    // -----------------------------

    // supernewsletter_content
    Db::getInstance()->Execute('
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'supernewsletter_content` (
      `id_supernewsletter_content` int(11) NOT NULL AUTO_INCREMENT,
      `date` date NOT NULL,
      `cron` tinyint(1) NOT NULL,
      `id_supernewsletter_template` int(11) NOT NULL,
      `id_lang_default` int(11) NOT NULL,
      `id_currency` int(11) NOT NULL,
      `ids_groups` text NOT NULL,
      `register_front` tinyint(1) NOT NULL,
      `groups` text NOT NULL,
      `id_country` int(11) NOT NULL,
      `products` text NOT NULL,
      `statut` int(11) NOT NULL,
      `emails_sent` text NOT NULL,
      `id_shop` int(11) NOT NULL,
      PRIMARY KEY (`id_supernewsletter_content`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
    ');

    // supernewsletter_content_lang
    Db::getInstance()->Execute('
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'supernewsletter_content_lang` (
      `id_supernewsletter_content` int(11) NOT NULL,
      `title` varchar(255) CHARACTER SET UTF8 NOT NULL,
      `content` text NOT NULL,
      `id_lang` int(11) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    ');

    // supernewsletter_email
    Db::getInstance()->Execute('
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'supernewsletter_email` (
      `id_supernewsletter_email` int(11) NOT NULL AUTO_INCREMENT,
      `email` varchar(255) NOT NULL,
      `firstname` varchar(255) NOT NULL,
      `lastname` varchar(255) NOT NULL,
      `group` varchar(64) NOT NULL,
      `date_add` datetime NOT NULL,
      `id_lang` int(11) NOT NULL,
      `id_shop` int(11) NOT NULL,
      PRIMARY KEY (`id_supernewsletter_email`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
    ');

    // ps_supernewsletter_stats
    Db::getInstance()->Execute('
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'supernewsletter_stats` (
      `id_supernewsletter_content` int(11) NOT NULL,
      `open_ip` text NOT NULL,
      `redirect_ip` text NOT NULL,
      `clicked_products` text NOT NULL,
      `clicked_links` text NOT NULL,
      `nb_unsubscribe` int(11) NOT NULL,
      KEY `id_supernewsletter_content` (`id_supernewsletter_content`)
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    ');

    // supernewsletter_template
    Db::getInstance()->Execute('
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'supernewsletter_template` (
      `id_supernewsletter_template` int(11) NOT NULL AUTO_INCREMENT,
      `name` varchar(64) NOT NULL,
      `link_img_header` varchar(255) NOT NULL,
      `logo` tinyint(1) NOT NULL,
      `logo_position` varchar(16) NOT NULL,
      `link_img_headerproducts` varchar(255) NOT NULL,
      `product_img_size` varchar(64) NOT NULL,
      `product_title_len` int(11) NOT NULL,
      `product_desc_type` varchar(32) NOT NULL,
      `product_desc_len` int(11) NOT NULL,
      `product_display_price` tinyint(1) NOT NULL,
      `link_facebook` varchar(255) NOT NULL,
      `link_twitter` varchar(255) NOT NULL,
      `link_rss` varchar(255) NOT NULL,
      `link_img_footer` varchar(255) NOT NULL,
      `police` varchar(255) NOT NULL,
      `police_size` int(11) NOT NULL,
      `bg_product` varchar(7) NOT NULL,
      `col_content` varchar(7) NOT NULL,
      `size_content` int(11) NOT NULL,
      `bold_content` tinyint(1) NOT NULL,
      `bdr_content` varchar(7) NOT NULL,
      `bdr_width_content` int(11) NOT NULL,
      `bg_newsletter` varchar(7) NOT NULL,
      `bdr_block_products` varchar(7) NOT NULL,
      `bdr_width_block_products` int(11) NOT NULL,
      `col_separator_product` varchar(7) NOT NULL,
      `bdr_width_separator_product` int(11) NOT NULL,
      `bdr_img_product` varchar(7) NOT NULL,
      `bdr_width_img_product` int(11) NOT NULL,
      `col_title_product` varchar(7) NOT NULL,
      `size_title_product` int(11) NOT NULL,
      `bold_title_product` tinyint(1) NOT NULL,
      `col_desc_product` varchar(7) NOT NULL,
      `size_desc_product` int(11) NOT NULL,
      `bold_desc_product` tinyint(1) NOT NULL,
      `col_pricelabel_product` varchar(7) NOT NULL,
      `size_pricelabel_product` int(11) NOT NULL,
      `bold_pricelabel_product` tinyint(1) NOT NULL,
      `col_price_product` varchar(7) NOT NULL,
      `size_price_product` int(11) NOT NULL,
      `bold_price_product` tinyint(1) NOT NULL,
      `col_promo_product` varchar(7) NOT NULL,
      `size_promo_product` int(11) NOT NULL,
      `bold_promo_product` tinyint(1) NOT NULL,
      `bg_btn_view_product` varchar(7) NOT NULL,
      `col_btn_view_product` varchar(7) NOT NULL,
      `size_btn_view_product` int(11) NOT NULL,
      `bold_btn_view_product` tinyint(1) NOT NULL,
      `bdr_btn_view_product` varchar(7) NOT NULL,
      `bdr_width_btn_view_product` int(11) NOT NULL,
      `pad_lft_btn_view_product` int(11) NOT NULL,
      `col_links_hf` varchar(7) NOT NULL,
      `size_links_hf` int(11) NOT NULL,
      `id_shop` int(11) NOT NULL,
      PRIMARY KEY (`id_supernewsletter_template`)
    ) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
    ');

    // supernewsletter_template_lang
    Db::getInstance()->Execute('
    CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'supernewsletter_template_lang` (
      `id_supernewsletter_template` int(11) NOT NULL,
      `signing` text NOT NULL,
      `id_lang` int(11) NOT NULL
    ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
    ');

    // -----------------------------
    // Contenu - 1x newsletter demo
    // -----------------------------

    $nb_rows = count(Db::getInstance()->ExecuteS('SELECT `id_supernewsletter_content` FROM  `'._DB_PREFIX_.'supernewsletter_content`'));
    if($nb_rows==0){

        // groups
        $groups = Group::getGroups($this->context->cookie->id_lang);
        $a_groups = array();
        foreach($groups as $g){$a_groups[]=$g['id_group'];}

        // 3 x products rand
        $products = Db::getInstance()->ExecuteS('SELECT p.`id_product` FROM  `'._DB_PREFIX_.'product` p
                                                 LEFT JOIN `'._DB_PREFIX_.'product_shop` ps ON (p.`id_product`=ps.`id_product`) 
                                                 WHERE p.`active`=1 AND ps.`id_shop`='.pSQL($id_shop).' ORDER BY RAND() LIMIT 3');
        $a_products = array();
        foreach($products as $p){
            $a_products[] = array(
                'id_product'=>$p['id_product'],
                'id_product_attribute'=>0,
            );
        }    

        // supernewsletter_content
        Db::getInstance()->Execute("
            INSERT INTO `"._DB_PREFIX_."supernewsletter_content` (`id_supernewsletter_content`, `date`, `cron`, `id_supernewsletter_template`, `id_lang_default`, `id_currency`, `ids_groups`, `id_country`, `register_front`, `groups`, `products`, `statut`, `emails_sent`, `id_shop`) VALUES
            (1, '".date('Y-m-d')."', 0, 5, 9999,".Configuration::get('PS_CURRENCY_DEFAULT').",'".pSQL(serialize($a_groups))."', 0, 1, '','".pSQL(serialize($a_products))."', 0, '',".$id_shop.");
        ");

        // supernewsletter_content_lang
        foreach($langs as $l){
            if($l['iso_code']!='fr'){$legend_lang = ' ('.$l['iso_code'].')';}else{$legend_lang = '';}
            Db::getInstance()->Execute("
                INSERT INTO `"._DB_PREFIX_."supernewsletter_content_lang` (`id_supernewsletter_content`, `title`, `content`, `id_lang`) VALUES
                (1, 'Newsletter d''exemple".$legend_lang."', '<p>Bonjour à tous".$legend_lang.",<br /><br />Profitez de nos actions exceptionnelles, vous trouverez ci-dessous notre nouvelle gamme de produits à des prix imbattables. Même nos concurrents ne peuvent rivaliser avec nos offres exclusives !<br /><br />Précipitez-vous et faites-vous plaisir !</p><br/><br/>',".$l['id_lang'].")
            ");
        }

        // supernewsletter_stats
        Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'supernewsletter_stats` (`id_supernewsletter_content`) VALUES(1)');
    }

    // -----------------------------
    // Contenu - 10 thèmes exemples
    // -----------------------------

    $nb_rows = count(Db::getInstance()->ExecuteS('SELECT `id_supernewsletter_template` FROM  `'._DB_PREFIX_.'supernewsletter_template`'));  
    $image_type = $this->getImageType('home');
    
    if($nb_rows==0){
        Db::getInstance()->Execute("
            INSERT INTO `"._DB_PREFIX_."supernewsletter_template` (`id_supernewsletter_template`, `name`, `link_img_header`, `logo`, `logo_position`, `link_img_headerproducts`, `product_img_size`, `product_title_len`, `product_desc_type`, `product_desc_len`, `product_display_price`, `link_img_footer`, `link_facebook`, `link_twitter`, `link_rss`, `police`, `police_size`, `bg_product`, `col_content`, `size_content`, `bold_content`, `bdr_content`, `bdr_width_content`, `bg_newsletter`, `bdr_block_products`, `bdr_width_block_products`, `col_separator_product`, `bdr_width_separator_product`, `bdr_img_product`, `bdr_width_img_product`, `col_title_product`, `size_title_product`, `bold_title_product`, `col_desc_product`, `size_desc_product`, `bold_desc_product`, `col_pricelabel_product`, `size_pricelabel_product`, `bold_pricelabel_product`, `col_price_product`, `size_price_product`, `bold_price_product`, `col_promo_product`, `size_promo_product`, `bold_promo_product`, `bg_btn_view_product`, `col_btn_view_product`, `size_btn_view_product`, `bold_btn_view_product`, `bdr_btn_view_product`, `bdr_width_btn_view_product`, `pad_lft_btn_view_product`, `col_links_hf`, `size_links_hf`, `id_shop`) VALUES
            (1, '03 > Shopping Girls', '', 1, 'center', '', '".$image_type."', 30, 'description_short', 200, 1, '','http://www.facebook.com', 'http://www.twitter.com', 'http://www.rss.com', 'Verdana, DejaVu Sans, Bitstream Vera Sans, Geneva, sans-serif', 12, '#fff9e3', '#363636', 12, 0, '#E57900', 1, '#ffffff', '#E57900', 1, '#d8d8d8', 1, '#d8d8d8', 1, '#965800', 15, 1, '#363636', 12, 0, '#525252', 16, 1, '#e20d00', 20, 1, '#e20d00', 12, 0, '#ff551e', '#ffffff', 12, 0, '#c73d00', 3, 0, '#004fe0', 10, 1),
            (2, '02 > Addiction', '', 1, 'center', '', '".$image_type."', 30, 'description_short', 200, 1, '','http://www.facebook.com', 'http://www.twitter.com', 'http://www.rss.com', 'Verdana, DejaVu Sans, Bitstream Vera Sans, Geneva, sans-serif', 12, '#fdffff', '#363636', 12, 0, '#088eeb', 1, '#ffffff', '#088eeb', 1, '#d8d8d8', 1, '#d8d8d8', 1, '#0089ef', 15, 1, '#363636', 12, 0, '#525252', 16, 1, '#e20d00', 20, 1, '#e20d00', 12, 0, '#0057aa', '#ffffff', 12, 0, '#002682', 3, 0, '#004fe0', 10, 1),
            (3, '05 > Nature', '', 1, 'center', '', '".$image_type."', 30, 'description_short', 200, 1, '','http://www.facebook.com', 'http://www.twitter.com', 'http://www.rss.com', 'Verdana, DejaVu Sans, Bitstream Vera Sans, Geneva, sans-serif', 12, '#fdffff', '#363636', 12, 0, '#029c02', 1, '#ffffff', '#029c02', 1, '#d8d8d8', 1, '#d8d8d8', 1, '#31aa00', 15, 1, '#363636', 12, 0, '#525252', 16, 1, '#e20d00', 20, 1, '#e20d00', 12, 0, '#196200', '#ffffff', 12, 0, '#002601', 3, 0, '#004fe0', 10, 1),
            (4, '06 > Rétro Love', '', 1, 'center', '', '".$image_type."', 30, 'description_short', 200, 1, '','http://www.facebook.com', 'http://www.twitter.com', 'http://www.rss.com', 'Verdana, DejaVu Sans, Bitstream Vera Sans, Geneva, sans-serif', 12, '#fdffff', '#363636', 12, 0, '#d21002', 1, '#ffffff', '#d21002', 1, '#d8d8d8', 1, '#d8d8d8', 1, '#2c241d', 15, 1, '#363636', 12, 0, '#525252', 16, 1, '#e20d00', 20, 1, '#e20d00', 12, 0, '#41352e', '#ffffff', 12, 0, '#29221d', 3, 0, '#004fe0', 10, 1),
            (5, '01 > Business', '', 1, 'center', '', '".$image_type."', 30, 'description_short', 200, 1, '','http://www.facebook.com', 'http://www.twitter.com', 'http://www.rss.com', 'Verdana, DejaVu Sans, Bitstream Vera Sans, Geneva, sans-serif', 12, '#fdffff', '#363636', 12, 0, '#1b283d', 1, '#ffffff', '#1b283d', 1, '#d8d8d8', 1, '#d8d8d8', 1, '#0a2a49', 15, 1, '#363636', 12, 0, '#525252', 16, 1, '#e20d00', 20, 1, '#e20d00', 12, 0, '#1b283d', '#ffffff', 12, 0, '#080f1b', 3, 0, '#004fe0', 10, 1),
            (6, '07 > Erotica', '', 1, 'center', '', '".$image_type."', 30, 'description_short', 200, 1, '','http://www.facebook.com', 'http://www.twitter.com', 'http://www.rss.com', 'Verdana, DejaVu Sans, Bitstream Vera Sans, Geneva, sans-serif', 12, '#fdffff', '#363636', 12, 0, '#9b046f', 1, '#ffffff', '#9b046f', 1, '#d8d8d8', 1, '#d8d8d8', 1, '#58013f', 15, 1, '#363636', 12, 0, '#525252', 16, 1, '#e20d00', 20, 1, '#e20d00', 12, 0, '#9b046f', '#ffffff', 12, 0, '#700350', 3, 0, '#004fe0', 10, 1),
            (7, '08 > Pâques', '', 1, 'center', '', '".$image_type."', 30, 'description_short', 200, 1, '','http://www.facebook.com', 'http://www.twitter.com', 'http://www.rss.com', 'Verdana, DejaVu Sans, Bitstream Vera Sans, Geneva, sans-serif', 12, '#fdffff', '#363636', 12, 0, '#a27a5f', 1, '#ffffff', '#a27a5f', 1, '#d8d8d8', 1, '#d8d8d8', 1, '#72533f', 15, 1, '#363636', 12, 0, '#525252', 16, 1, '#e20d00', 20, 1, '#e20d00', 12, 0, '#b8957d', '#ffffff', 12, 0, '#987258', 3, 0, '#004fe0', 10, 1),
            (8, '04 > Vacances', '', 1, 'center', '', '".$image_type."', 30, 'description_short', 200, 1, '','http://www.facebook.com', 'http://www.twitter.com', 'http://www.rss.com', 'Verdana, DejaVu Sans, Bitstream Vera Sans, Geneva, sans-serif', 12, '#fdffff', '#363636', 12, 0, '#48cfda', 1, '#ffffff', '#48cfda', 1, '#d8d8d8', 1, '#d8d8d8', 1, '#048981', 15, 1, '#363636', 12, 0, '#525252', 16, 1, '#e20d00', 20, 1, '#e20d00', 12, 0, '#0d9aa1', '#ffffff', 12, 0, '#07666b', 3, 0, '#004fe0', 10, 1),
            (9, '09 > Fête', '', 1, 'center', '', '".$image_type."', 30, 'description_short', 200, 1, '','http://www.facebook.com', 'http://www.twitter.com', 'http://www.rss.com', 'Verdana, DejaVu Sans, Bitstream Vera Sans, Geneva, sans-serif', 12, '#fdffff', '#363636', 12, 0, '#d70001', 1, '#ffffff', '#d70001', 1, '#d8d8d8', 1, '#d8d8d8', 1, '#a91313', 15, 1, '#363636', 12, 0, '#525252', 16, 1, '#bd7004', 20, 1, '#bd7004', 12, 0, '#d81922', '#ffffff', 12, 0, '#a1090a', 3, 0, '#004fe0', 10, 1),
            (10, '10 > Halloween', '', 1, 'center', '', '".$image_type."', 30, 'description_short', 200, 1, '','http://www.facebook.com', 'http://www.twitter.com', 'http://www.rss.com', 'Verdana, DejaVu Sans, Bitstream Vera Sans, Geneva, sans-serif', 12, '#fdffff', '#363636', 12, 0, '#00030d', 1, '#ffffff', '#00030d', 1, '#d8d8d8', 1, '#d8d8d8', 1, '#08162a', 15, 1, '#363636', 12, 0, '#525252', 16, 1, '#e20d00', 20, 1, '#e20d00', 12, 0, '#ef5d0d', '#ffffff', 12, 0, '#ad440a', 3, 0, '#004fe0', 10, 1);
        ");
        for($i=1;$i<=10;$i++){
            foreach($langs as $l){
                if($l['iso_code']!='fr'){$legend_lang = '('.$l['iso_code'].') ';}else{$legend_lang = '';}
                $signing = '
                <table cellspacing="0" cellpadding="0" style="width:100%;margin-top:5px;">
                    <tr>
                        <td style="font-family:Verdana;font-size:11px;text-align:center">
                            '.$legend_lang.'Entreprise nom exemple - Rue de la Gare exemple - 1000 Ville exemple</span><br />
                            contact@exemple.com - 0800 800 800 - http://www.exemple.com
                        </td>
                    </tr>
                </table>';
                Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'supernewsletter_template_lang` (`id_supernewsletter_template`,`signing`,`id_lang`) VALUES ('.pSQL($i).',"'.pSQL($signing,true).'",'.pSQL($l['id_lang']).')');
            }
        }
    }

// employé connected - end
}

?>
