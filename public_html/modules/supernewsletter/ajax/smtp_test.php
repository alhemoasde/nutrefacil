<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

// autoriser l'accès Ajax
header('Access-Control-Allow-Origin: *');

require_once(dirname(__FILE__).'/../../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../../init.php');

// V1
if(Tools::version_compare(_PS_VERSION_, '1.6.1.5','<')){
    include_once(_PS_SWIFT_DIR_.'Swift.php');
    include_once(_PS_SWIFT_DIR_.'Swift/Connection/SMTP.php');
    include_once(_PS_SWIFT_DIR_.'Swift/Connection/NativeMail.php');
    include_once(_PS_SWIFT_DIR_.'Swift/Plugin/Decorator.php');
// V2
}else{
    include_once(_PS_SWIFT_DIR_.'swift_required.php');
}

require_once(dirname(__FILE__).'/../supernewsletter.php');
$Spnews = new Supernewsletter();

try
{
    $smtpMailDomain = Tools::getValue('mail_domain');
    $smtpServer = Tools::getValue('smtp_server');
    $smtpPort = Tools::getValue('smtp_port');
    $smtpEncryption = Tools::getValue('smtp_encryption');
    $smtpLogin = Tools::getValue('smtp_user');
    $smtpPassword = Tools::getValue('smtp_passwd');
    $email_test = Tools::getValue('email_test');
    
    $subject = 'Mail Supernewlsetter - Smtp test';
    $content = $Spnews->l('Le message a été envoyé correctement via Smtp','smtp_test');
    
    // V1
    if(Tools::version_compare(_PS_VERSION_, '1.6.1.5','<')){
        $smtp = new Swift_Connection_SMTP($smtpServer, $smtpPort, ($smtpEncryption == 'off') ? 
        Swift_Connection_SMTP::ENC_OFF : (($smtpEncryption == 'tls') ? Swift_Connection_SMTP::ENC_TLS : Swift_Connection_SMTP::ENC_SSL));
        $smtp->setUsername($smtpLogin);
        $smtp->setpassword($smtpPassword);
        $smtp->setTimeout(5);
        $swift = new Swift($smtp,$smtpMailDomain);
        $message = new Swift_Message($subject,$content,'text/html');
        if($swift->send($message,$email_test,Configuration::get('PS_SHOP_EMAIL'))){
            $result = true;
        }
        $swift->disconnect();
    }else{
        if(Tools::strtolower($smtpEncryption)==='off'){$smtpEncryption = false;}
        $connection = Swift_SmtpTransport::newInstance($smtpServer,$smtpPort,$smtpEncryption)
                      ->setUsername($smtpLogin)
                      ->setPassword($smtpPassword);
        $swift = Swift_Mailer::newInstance($connection);
        $message = new Swift_Message($subject,$content,'text/html');  
        $message->setFrom(array(Configuration::get('PS_SHOP_EMAIL') => Configuration::get('PS_SHOP_NAME')));
        $message->setTo($email_test);                  
        if($swift->send($message)){
            $result = true;
        }
    }
    
    if($result==true){$result=$Spnews->l('Email envoyé avec succès','smtp_test');}

}

catch (Swift_ConnectionException $e)
{
    $result = $e->getMessage();
}

catch (Swift_Message_MimeException $e)
{
    $result = $e->getMessage();
}

echo Tools::jsonEncode(array('msg'=>$result));

?>