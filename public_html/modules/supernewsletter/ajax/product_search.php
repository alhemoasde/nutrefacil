<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

// autoriser l'accès Ajax
header('Access-Control-Allow-Origin: *');

require_once(dirname(__FILE__).'/../../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../../init.php');
require_once(dirname(__FILE__).'/../classes/ToolClass.php');
require_once(dirname(__FILE__).'/../supernewsletter.php');
$Supernewsletter = new Supernewsletter();

ob_clean();

$token = Tools::getValue('token');
if($token==_COOKIE_KEY_){

    $context = Context::getContext();
    $cookie = $context->cookie;

    $id_shop = Tools::getValue('id_shop');
    if(empty($id_shop)){
        $Context = Context::getContext();
        $id_shop = $Context->shop->id;
    }
    $Shop = new Shop($id_shop);

    $q = Tools::getValue('q');
    $id_lang = Tools::getValue('id_lang');

    $sql = '
    SELECT p.`id_product`,p.`supplier_reference`,pl.`name` FROM `'._DB_PREFIX_.'product` p
    LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON p.`id_product` = pl.`id_product`
    LEFT JOIN `'._DB_PREFIX_.'product_shop` ps ON p.`id_product` = ps.`id_product`
    WHERE
        ps.`id_shop` = '.pSQL($id_shop).' AND
        pl.`id_shop` = '.pSQL($id_shop).' AND
        pl.`id_lang` = '.pSQL($id_lang).'
    AND
        (name LIKE "%'.pSQL($q).'%"
    OR reference LIKE "%'.pSQL($q).'%"
    OR supplier_reference LIKE "%'.pSQL($q).'%")';

    $res = Db::getInstance()->ExecuteS($sql);

    $products = array();
    $image_type = $Supernewsletter->getImageType('medium');

    $key = 0;
    foreach($res as $p){

        $Product = new Product($p['id_product'],false,(int)$id_lang);
        $image = Product::getCover($p['id_product']);
        $Link = new Link();

        // simple
        $products[$key]['id_product'] = $Product->id;
        $products[$key]['name'] = $Product->name;
        $products[$key]['id_product_attribute'] = 0;
            if(Configuration::get('PS_LEGACY_IMAGES')){$id_image_for_link=$p['id_product'].'-'.$image['id_image'];}else{$id_image_for_link=$image['id_image'];}    
            if(!empty($image['id_image']) && $image['id_image']!=0){$img_link_princ='http://'.$Link->getImageLink($Product->link_rewrite,$id_image_for_link,$image_type);}else{$img_link_princ=$Shop->getBaseURL().'modules/supernewsletter/views/img/no_picture_product.jpg';}
        $products[$key]['img_link'] = $img_link_princ;
        $key++;

        $combArray = array();
        $assocNames = array();
        $combinations = $Product->getAttributeCombinations((int)$id_lang);
        $combinations_images = $Product->getCombinationImages($id_lang);
        
        // Si le produit est décliné on garde uniquement le produit principal
        if(!empty($combinations)){
            $key_delete_p_princ = $key-1;
            unset($products[$key_delete_p_princ]);
        }

        foreach ($combinations as $k => $combination)
                $combArray[$combination['id_product_attribute']][] = array('group' => $combination['group_name'], 'attr' => $combination['attribute_name']);
        foreach ($combArray as $id_product_attribute => $product_attribute)
        {
            $list = '';
            foreach ($product_attribute as $attribute)
                    $list .= trim($attribute['group']).' - '.trim($attribute['attr']).', ';
            $list = rtrim($list, ', ');
            $assocNames[$id_product_attribute] = $list;

            // declinaison
            $products[$key]['id_product'] = $Product->id;
            $products[$key]['name'] = $Product->name.' '.$list;
            $products[$key]['id_product_attribute'] = $id_product_attribute; 
            $id_image = @$combinations_images[$id_product_attribute][0]['id_image'];
            if(Configuration::get('PS_LEGACY_IMAGES')){$id_image_for_link=$p['id_product'].'-'.$id_image;}else{$id_image_for_link=$id_image;}    
            if(!empty($id_image) && $id_image!=0){$img_link_decl='http://'.$Link->getImageLink($Product->link_rewrite,$id_image_for_link,$image_type);}else{$img_link_decl=$img_link_princ;}
            $products[$key]['img_link'] = $img_link_decl;
            $key++;
        }

    }

    echo Tools::jsonEncode($products);

}else{
    echo 'Error: bad token';
}

?>
