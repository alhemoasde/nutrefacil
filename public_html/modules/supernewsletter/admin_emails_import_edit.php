<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

if(!defined('_COOKIE_KEY_')){die('token error');}

// soumission du formulaire
if(Tools::isSubmit('submitEditNewsletter')){
    
    // Contrôle du formulaire
    $email = Tools::getValue('email');
    if(empty($email)){
        $this->_errors[] = $this->l('Veuillez indiquer l\'adresse email',$page_name); 
    }
    if(!Validate::isEmail($email)){
        $this->_errors[] = $this->l('L\'adresse email n\'est pas valide',$page_name); 
    }
    if(count($this->_errors)){
        foreach($this->_errors as $err){
            $this->_html .= '<div class="alert error">'.$err.'</div>';
        }
    }
    
    // Si le formulaire est ok
    if(empty($this->_errors)){     
        $id = Tools::getValue('id_supernewsletter_email');
        // update
        if(!empty($id)){
            $SupernewsletterEmail= new SupernewsletterEmail($id);
            $SupernewsletterEmail->email = Tools::getValue('email');
            $SupernewsletterEmail->firstname = Tools::getValue('firstname');
            $SupernewsletterEmail->lastname = Tools::getValue('lastname');
            $SupernewsletterEmail->group = Tools::getValue('group');
            $SupernewsletterEmail->id_lang = Tools::getValue('id_lang');
            $SupernewsletterEmail->update();
            $this->_html.=$this->displayConfirmation($this->l('Modification effectuée',$page_name));
        }
    }
}

// Toolbar
$this->toolbar_btn['back'] =  array(
    'href' => $current_index.'&importemails',
    'desc' => $this->l('Revenir à la liste des emails',$page_name),
    'icon' => 'icon-arrow-left'
);
if($this->ps_version==1.6){$this->_html.=$this->toolbarModule($this->toolbar_btn);}
		
// Formulaire
foreach($languages as $k => $language){$languages[$k]['is_default'] = (int)($language['id_lang'] == Configuration::get('PS_LANG_DEFAULT'));}

$helper = new HelperForm();
$helper->module = $this;
$helper->name_controller = 'supernewsletter';
$helper->identifier = $this->identifier;
$helper->token = Tools::getAdminTokenLite('AdminModules');
$helper->languages = $languages;
$helper->currentIndex = $_SERVER['REQUEST_URI'];
$helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
$helper->allow_employee_form_lang = true;
$helper->toolbar_scroll = true;
$helper->toolbar_btn = $this->toolbar_btn;
$helper->title = $this->displayName;
$helper->submit_action = 'submitEditEmail';

$this->fields_form[0]['form'] = array(
    'tinymce' => true,
    'legend' => array(
            'title' => $this->l('Modification email',$page_name),
            'image' => $this->_path.'logo.gif'
    ),
    'input' => array(
            array(
                    'type' => 'text',
                    'label' => $this->l('Email',$page_name),
                    'name' => 'email',
                    'size' => 255,
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Prénom',$page_name),
                    'name' => 'firstname',
                    'size' => 64,
            ),
            array(
                    'type' => 'text',
                    'label' => $this->l('Nom',$page_name),
                    'name' => 'lastname',
                    'size' => 64,
            ),
            array(
                    'type' => 'select',
                    'label' => $this->l('Groupe',$page_name).' : ',
                    'name' => 'group',
                    'options' => array(
                            'query' => SupernewsletterEmail::getGroups(),
                            'id' => 'group',
                            'name' => 'name',
                            )
            ),
            array(
                    'type' => 'select',
                    'label' => $this->l('Langue',$page_name).' : ',
                    'name' => 'id_lang',
                    'options' => array(
                            'query' => Language::getLanguages(),
                            'id' => 'id_lang',
                            'name' => 'iso_code',
                            )
            ),
    ),	
    'submit' => array(
            'name' => 'submitEditNewsletter',
            'title' => $this->l('Sauver ',$page_name),
            'class' => 'button'
    ),
);		

// Charge l'affichage du formulaire
$id_supernewsletter_email = Tools::getValue('id_supernewsletter_email');

// validation avec erreur
if(!empty($this->_errors)){
    $SupernewsletterEmail = new SupernewsletterEmail();
    $SupernewsletterEmail->copyFromPost();
// nouvelle newsletter
}elseif(empty($id_supernewsletter_email) && $_POST){
    $SupernewsletterEmail = new SupernewsletterEmail();
// update newsletter
}else{
    $SupernewsletterEmail = new SupernewsletterEmail($id_supernewsletter_email);
}

// remplissage des champs classiques
foreach($this->fields_form[0]['form']['input'] as $input){
    if($input['name'] != 'img_header' && $input['name'] != 'img_header_display' && $input['name'] != 'img_footer' && $input['name'] != 'img_footer_display' && $input['name'] != 'product_search'){
        $helper->fields_value[$input['name']] = $SupernewsletterEmail->{$input['name']};
    }
}

// remplissage des checkbox ids_groups
if(!empty($SupernewsletterEmail->ids_groups)){
    $ids_groups = unserialize($SupernewsletterEmail->ids_groups);
    foreach($ids_groups as $id){
        $helper->fields_value['ids_groups_'.$id] = 1;
    }
}

$this->_html .= $helper->generateForm($this->fields_form);
		
?>
