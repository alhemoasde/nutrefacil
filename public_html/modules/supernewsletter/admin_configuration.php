<?php
/**
*  NOTICE OF LICENSE
* 
*  Module for Prestashop
*  100% Swiss development
* 
*  @author    Webbax <contact@webbax.ch>
*  @copyright -
*  @license   -
*/

if(!defined('_COOKIE_KEY_')){die('token error');}

// soumission du formulaire
if(Tools::isSubmit('submitEditConfiguration') || Tools::isSubmit('submitEditConfigurationAndStay')){
    
    // si le formulaire est ok
    if(empty($this->_errors)){
        // POST
        Configuration::updateValue('SUPERNEWS_UTF8ENCODE',Tools::getValue('utf8encode'));
        Configuration::updateValue('SUPERNEWS_EMAILS_PACK',Tools::getValue('emails_pack'));
        Configuration::updateValue('SUPERNEWS_EMAILS_SLEEP',Tools::getValue('emails_sleep'));
        // Smtp
        Configuration::updateValue('SUPERNEWS_MAIL_METHOD',Tools::getValue('mail_method'));
        Configuration::updateValue('SUPERNEWS_MAIL_DOMAIN',Tools::getValue('mail_domain'));
        Configuration::updateValue('SUPERNEWS_SMTP_SERVER',Tools::getValue('smtp_server'));
        Configuration::updateValue('SUPERNEWS_SMTP_USER',Tools::getValue('smtp_user'));
        Configuration::updateValue('SUPERNEWS_SMTP_PASSWD',Tools::getValue('smtp_passwd'));
        Configuration::updateValue('SUPERNEWS_SMTP_ENCRYPTION',Tools::getValue('smtp_encryption'));
        Configuration::updateValue('SUPERNEWS_SMTP_PORT',Tools::getValue('smtp_port'));
        
        Configuration::updateValue('SUPERNEWS_EMAIL_SENDER',Tools::getValue('email_sender'));
        Configuration::updateValue('SUPERNEWS_EMAIL_BOUNCES',Tools::getValue('email_bounces'));
        Configuration::updateValue('SUPERNEWS_CRON_MAX_EX_TIME',Tools::getValue('cron_max_execution_time'));
        Configuration::updateValue('SUPERNEWS_CRON_MEM_LIMIT',Tools::getValue('cron_memory_limit'));
        Configuration::updateValue('SUPERNEWS_WEBBAX_DEBUG',Tools::getValue('webbax_debug'));
        
        if(Tools::isSubmit('submitEditConfigurationAndStay') || $this->ps_version==1.6){
            $url = $current_index.'&configuration&msgtype=5';
        }else{
            $url = $current_index.'&msgtype=5';
        } 
        Tools::redirectAdmin($url);
    }
}

$shop_domain = Configuration::get('PS_SHOP_DOMAIN');
$ssl = Configuration::get('PS_SSL_ENABLED');
$base_url_ajax = $Shop->getBaseURL();
if($ssl && Tools::strpos('https',$base_url_ajax)===false){
    $base_url_ajax = str_replace('http','https',$Shop->getBaseURL());
}

// Toolbar
$this->toolbar_btn['back'] =  array(
    'href' => $current_index,
    'desc' => $this->l('Revenir à la liste des newsletter',$page_name),
    'icon' => 'icon-arrow-left',
);
$this->toolbar_btn['save'] = array(
    'href' => $_SERVER['REQUEST_URI'],
    'desc' => $this->l('Save',$page_name)

);
$this->toolbar_btn['save-and-stay'] = array(
    'short' => 'SaveAndStay',
    'href' => $_SERVER['REQUEST_URI'],
    'desc' => $this->l('Save and stay',$page_name),
);
if($this->ps_version==1.6){$toolbarModule=$this->toolbarModule($this->toolbar_btn);}else{$toolbarModule='';}
		
// Formulaire
foreach($languages as $k => $language){$languages[$k]['is_default'] = (int)($language['id_lang'] == Configuration::get('PS_LANG_DEFAULT'));}

$helper = new HelperForm();
$helper->module = $this;
$helper->name_controller = 'supernewsletter';
$helper->identifier = $this->identifier;
$helper->token = Tools::getAdminTokenLite('AdminModules');
$helper->languages = $languages;
$helper->currentIndex = $_SERVER['REQUEST_URI'];
$helper->default_form_language = (int)Configuration::get('PS_LANG_DEFAULT');
$helper->allow_employee_form_lang = true;
$helper->toolbar_scroll = true;
$helper->toolbar_btn = $this->toolbar_btn;
$helper->title = $this->l('Configuration avancée',$page_name);
$helper->submit_action = 'submitEditConfiguration';

$this->fields_form[0]['form'] = array(
    'legend' => array(
            'title' => $this->l('Paramètres',$page_name),
            'image' => $this->_path.'views/img/tools.png'
    ),
    'input' => array(
            array(
                    'type' => 'free',
                    'label' => $this->l('Forcer l\'encodage des descriptions en UTF8',$page_name),
                    'name' => 'utf8encode',
            ),
            array(
                    'type' => 'free',
                    'label' => $this->l('Envoi par pack de',$page_name),
                    'desc' =>  $this->l('Valeur conseillée',$page_name).' : 20',
                    'name' => 'emails_pack',
            ),
            array(
                    'type' => 'free',
                    'label' => $this->l('Délai d\'attente entre les mails',$page_name),
                    'name' => 'emails_sleep',
            ),
            array(
                    'type' => 'free',
                    'label' => $this->l('Configuration Smtp',$page_name),
                    'name' => 'conf_smtp',
            ),
            array(
                    'type' => 'free',
                    'label' => $this->l('Email de l\'expéditeur',$page_name),
                    'desc' =>  $this->l('Adresse à utiliser pour l\'expédition de vos mails',$page_name).' (reply:to)',
                    'name' => 'email_sender',
            ),
            array(
                    'type' => 'free',
                    'label' => $this->l('Email pour la récupération des erreurs du type "bounces"',$page_name),
                    'desc' =>  $this->l('Retourne les emails échoués (à l\'adresse ci-dessus) lors de l\'envoi de la newsletter',$page_name),
                    'name' => 'email_bounces',
            ),
            array(
                    'type' => 'free',
                    'label' => $this->l('Tâches planifiées forcer la configuration',$page_name),
                    'desc' =>  $this->l('Fonctionne uniquement si votre serveur supporte la fonction',$page_name).' : "ini_set"',
                    'name' => 'cron_config',
            ),
            array(
                    'type' => 'free',
                    'label' => $this->l('Webbax Debug',$page_name),
                    'name' => 'webbax_debug',
            ),
    ),	
    'submit' => array(
            'name' => 'submitEditConfiguration',
            'title' => $this->l('Sauver ',$page_name),
            'class' => 'button'
    ),
);	

// Charge l'affichage du formulaire
$id_supernewsletter_template = Tools::getValue('id_supernewsletter_template');

// validation avec erreur
if(!empty($this->_errors)){
    $SupernewsletterTemplate = new SupernewsletterTemplate();
    $SupernewsletterTemplate->copyFromPost();
// add template
}elseif(empty($id_supernewsletter_template) && $_POST){
    $SupernewsletterTemplate = new SupernewsletterTemplate();
// update template
}else{
    $SupernewsletterTemplate = new SupernewsletterTemplate($id_supernewsletter_template);
}

// remplissage des champs classiques
foreach($this->fields_form[0]['form']['input'] as $input){
    if($input['name']!='utf8encode' && $input['name']!='emails_pack' && $input['name']!='emails_sleep' && $input['name']!='conf_smtp' && $input['name']!='email_sender' && $input['name']!='email_bounces' && $input['name']!='cron_config' && $input['name']!='webbax_debug'){
        $helper->fields_value[$input['name']] = $SupernewsletterTemplate->{$input['name']};
    }
}

// champ encodage utf8
$utf8encode = Configuration::get('SUPERNEWS_UTF8ENCODE');
if($utf8encode){$utf8encode_yes='selected';$utf8encode_no='';}else{$utf8encode_yes='';$utf8encode_no='selected';}
$helper->fields_value['utf8encode'] = '
<select name="utf8encode">
    <option value="1" '.$utf8encode_yes.'>'.$this->l('Oui',$page_name).'
    <option value="0" '.$utf8encode_no.'>'.$this->l('Non',$page_name).'
</select>';

// emails pack
$emails_pack = Configuration::get('SUPERNEWS_EMAILS_PACK');
$helper->fields_value['emails_pack'] = '
<input type="text" name="emails_pack" id="emails_pack" value="'.$emails_pack.'" /> '.$this->l('email(s)',$page_name);

// emails sleep
$emails_sleep = Configuration::get('SUPERNEWS_EMAILS_SLEEP');
$helper->fields_value['emails_sleep'] = '
<input type="text" name="emails_sleep" id="emails_sleep" value="'.$emails_sleep.'" /> '.$this->l('sec.',$page_name);


// smtp
$helper->fields_value['conf_smtp'] = '
    
<input type="checkbox" name="mail_method" id="mail_method" value="2" '.((Configuration::get('SUPERNEWS_MAIL_METHOD')==2)?'checked':'').'> <span id="label_mail_method">'.$this->l('Désactiver',$page_name).'</span>

<script type="text/javascript">
    $(document).ready(function(){ 
        function check_form(){
           if($("#mail_method").prop("checked")){
                $("#conf_smtp").show();
                $("#label_mail_method").html("'.$this->l('Désactiver',$page_name).'");
            }else{
                $("#conf_smtp").hide();
                $("#label_mail_method").html("'.$this->l('Activer',$page_name).'");    
            }
        }
        // init
        check_form();
        // on click
        $("#mail_method").click(function(){
            check_form();
        });
    });
</script>
    
<table id="conf_smtp">
    <tr>
        <td>'.$this->l('Nom de domaine',$page_name).' : </td><td><input type="text" id="mail_domain" name="mail_domain" value="'.Configuration::get('SUPERNEWS_MAIL_DOMAIN').'" />
            '.(($this->ps_version<1.6)?'<br/>':'').'
            <i>
            '.$this->l('Nom de domaine pour l\'envoi de mail',$page_name).'<br/>
            '.$this->l('laissez vide si vous ne savez pas',$page_name).'
            </i>
        </td>
    </tr>
    <tr>
        <td>'.$this->l('Serveur',$page_name).' : </td><td><input type="text" id="smtp_server" name="smtp_server" value="'.Configuration::get('SUPERNEWS_SMTP_SERVER').'" /></td>
    </tr>
    <tr>
        <td>'.$this->l('Utilisateur',$page_name).' : </td><td><input type="text" id="smtp_user" name="smtp_user" value="'.Configuration::get('SUPERNEWS_SMTP_USER').'" /></td>
    </tr>
    <tr>
        <td>'.$this->l('Mot de passe',$page_name).' : </td><td><input type="password" id="smtp_passwd" name="smtp_passwd" value="'.Configuration::get('SUPERNEWS_SMTP_PASSWD').'" /></td>
    </tr>
    <tr>
        <td>'.$this->l('Cryptage',$page_name).' : </td>
        <td>
            <select id="smtp_encryption" name="smtp_encryption">
                <option selected="selected" value="off" '.((Configuration::get('SUPERNEWS_SMTP_ENCRYPTION')=='off')?'selected':'').'>'.$this->l('Aucun',$page_name).'</option>
                <option value="tls" '.((Configuration::get('SUPERNEWS_SMTP_ENCRYPTION')=='tls')?'selected':'').'>TLS</option>
                <option value="ssl" '.((Configuration::get('SUPERNEWS_SMTP_ENCRYPTION')=='ssl')?'selected':'').'>SSL</option>
            </select>
        </td>
    </tr>
    <tr>
        <td>'.$this->l('Port',$page_name).' : </td><td><input type="text" id="smtp_port" name="smtp_port" value="'.Configuration::get('SUPERNEWS_SMTP_PORT').'" size="5"></td>
    </tr>
    <tr>
        <td colspan="2">
        <script type="text/javascript">
            $(document).ready(function(){

                $("#sendMailtest").click(function(){
                
                    var mail_domain = $("#mail_domain").val();   
                    var smtp_server = $("#smtp_server").val(); 
                    var smtp_user = $("#smtp_user").val(); 
                    var smtp_passwd = $("#smtp_passwd").val(); 
                    var smtp_encryption = $("#smtp_encryption").val(); 
                    var smtp_port = $("#smtp_port").val(); 
                    var email_test = $("#email_test").val(); 
                    
                    if(validateEmail(email_test)==false){
                        alert("'.$this->l('Adresse email incorrect',$page_name).'");
                        return false;
                    }

                    $.ajax({
                        url: "'.$base_url_ajax.'modules/'.$this->name.'/ajax/smtp_test.php",
                        dataType: "json",
                        data:{
                            mail_domain:mail_domain,
                            smtp_server:smtp_server,
                            smtp_user:smtp_user,
                            smtp_passwd:smtp_passwd,
                            smtp_encryption:smtp_encryption,
                            smtp_port:smtp_port,
                            email_test:email_test,
                        },
                        success: function(data){
                                alert(data.msg);
                        },
                        error: function(jqXHR, exception) {
                            if (jqXHR.status === 0) {
                                alert("Not connect.\n Verify Network.");
                            } else if (jqXHR.status == 404) {
                                alert("Requested page not found. [404]");
                            } else if (jqXHR.status == 500) {
                                alert("Internal Server Error [500].");
                            } else if (exception === "parsererror") {
                                alert("Requested JSON parse failed.");
                            } else if (exception === "timeout") {
                                alert("Time out error.");
                            } else if (exception === "abort") {
                                alert("Ajax request aborted.");
                            } else {
                                alert("Uncaught Error.\n" + jqXHR.responseText);
                            }
                        }
                    })
                });
                
               function validateEmail(email){ 
                    var re = /\S+@\S+\.\S+/;
                    return re.test(email);
               } 

            });
            </script>
            
            <span id="span_email_test">'.$this->l('Votre email',$page_name).' :</span> <input id="email_test" type="text" value="" name="email_test">
            <input type="button" class="button btn btn-default" name="sendMailtest" id="sendMailtest" value="'.$this->l('Tester l\'envoi SMTP').'" />
            <p id="mailResultCheck" style="display:none;"></p>
        </td>
    </tr>
</table>';

$email_bounces = Configuration::get('SUPERNEWS_EMAIL_BOUNCES');
$helper->fields_value['email_bounces'] = '
<input type="text" name="email_bounces" id="email_bounces" value="'.$email_bounces.'" />';

$email_sender = Configuration::get('SUPERNEWS_EMAIL_SENDER');
$helper->fields_value['email_sender'] = '
<input type="text" name="email_sender" id="email_sender" value="'.$email_sender.'" />';

// cron values
$cron_max_execution_time = Configuration::get('SUPERNEWS_CRON_MAX_EX_TIME');
$cron_memory_limit = Configuration::get('SUPERNEWS_CRON_MEM_LIMIT');
$helper->fields_value['cron_config'] = '
<table id="table_cron_config">
    <tr><td>max_execution_time : </td><td><input type="text" name="cron_max_execution_time" id="cron_max_execution_time" value="'.$cron_max_execution_time.'" /> ('.$this->l('par défaut',$page_name).' '.ini_get('max_execution_time').' '.$this->l('secondes',$page_name).')</td></tr>
    <tr><td>memory_limit : </td><td><input type="text" name="cron_memory_limit" id="cron_memory_limit" value="'.$cron_memory_limit.'" style="margin-top:2px" /> ('.$this->l('par défaut',$page_name).' '.ini_get('memory_limit').')</td></tr>
</table>';

// webbax  debug
$webbax_debug = Configuration::get('SUPERNEWS_WEBBAX_DEBUG');
if($webbax_debug){$webbax_debug_yes='selected';$webbax_debug_no='';}else{$webbax_debug_yes='';$webbax_debug_no='selected';}
$helper->fields_value['webbax_debug'] = '
<select name="webbax_debug">
    <option value="1" '.$webbax_debug_yes.'>'.$this->l('Oui',$page_name).'
    <option value="0" '.$webbax_debug_no.'>'.$this->l('Non',$page_name).'
</select>';

$this->_html .= $toolbarModule.$helper->generateForm($this->fields_form);
		
?>