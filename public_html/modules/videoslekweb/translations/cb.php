<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{videoslekweb}prestashop>videoslekweb_30884fbfe7cf856c36176513942460d4'] = 'Añadir un vídeo en una columna, en la página de inicio o en una página de producto';
$_MODULE['<{videoslekweb}prestashop>videoslekweb_6fe27449c044581563a5804c25d83733'] = 'Video Borrado';
$_MODULE['<{videoslekweb}prestashop>videoslekweb_0c4879ec0d9c6523592e1fc6d2d8fcbb'] = 'Columna de vídeo derecha, izquierda o página de inicio.';
$_MODULE['<{videoslekweb}prestashop>videoslekweb_fe574536622ab9aa59f13a8137f09a4d'] = 'Enlace de vídeo';
$_MODULE['<{videoslekweb}prestashop>videoslekweb_622258dd67b833d8697b2997335bb606'] = 'Ancho del vídeo';
$_MODULE['<{videoslekweb}prestashop>videoslekweb_f6cafd3b306b2fe645c50810390a3d7a'] = 'Altura del Video';
$_MODULE['<{videoslekweb}prestashop>videoslekweb_05b1dc77590105a18fc4c02526fe0c7a'] = 'Autoplay';
$_MODULE['<{videoslekweb}prestashop>videoslekweb_89d7b10cb4238977d2b523dfd9ea7745'] = 'Bucke';
$_MODULE['<{videoslekweb}prestashop>videoslekweb_8b200a3d779e29198697a2a2639386b0'] = 'Página de productos en vídeo';
$_MODULE['<{videoslekweb}prestashop>videoslekweb_577efdca12836bf637af0e252b1e9009'] = 'Lista de productos';
$_MODULE['<{videoslekweb}prestashop>videoslekweb_f2a6c498fb90ee345d997f888fce3b18'] = 'Borrar';
