<?php
/*
*
* http://www.slekweb.fr
*
*/
if (!defined('_PS_VERSION_')) exit;
	
class VideoSlekweb extends Module
{
	function __construct()
	{
		$this->name = 'videoslekweb';
		$this->tab = 'others';
		$this->version = '1.1';
		$this->author = 'Slekweb.fr';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('Vidéo Slekweb');
		$this->description = $this->l('Ajoute une vidéo dans une colonne, sur l\'accueil ou sur une page produit');

		$this->slkvideo=Configuration::get('VIDEOSLK_V');
		$this->slkwidth=Configuration::get('VIDEOSLK_W');
		$this->slkheight=Configuration::get('VIDEOSLK_H');
		$this->slkautoplay=Configuration::get('VIDEOSLK_A');
		$this->slkloop=Configuration::get('VIDEOSLK_L');
	}

	function install()
	{
		if(!parent::install() || !$this->registerHook('leftColumn') || !$this->registerHook('productFooter') || !$this->createTable())
			return false;
		Configuration::updateValue('VIDEOSLK_V', '//www.youtube.com/embed/_-U2xcuQk-U');
		Configuration::updateValue('VIDEOSLK_W', '200');
		Configuration::updateValue('VIDEOSLK_H', '165');
		Configuration::updateValue('VIDEOSLK_A', '0');
		Configuration::updateValue('VIDEOSLK_L', '0');
		return true;
	}

	public function createTable()
	{
		if (Db::getInstance()->Execute('
		CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'video_slekweb` (
			`id_produit` int(10) unsigned NOT NULL,
			`link_video` varchar(255) NOT NULL,
			`actif` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT "1",
			PRIMARY KEY (`id_produit`)
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8'))
			return true;
	}

	function uninstall()
	{
		return (parent::uninstall() && Configuration::deleteByName('VIDEOSLK_V') && Configuration::deleteByName('VIDEOSLK_W') && Configuration::deleteByName('VIDEOSLK_H') && Configuration::deleteByName('VIDEOSLK_A') && Configuration::deleteByName('VIDEOSLK_L') && $this->deleteTables());
	}

	protected function deleteTables()
	{
		return Db::getInstance()->execute('
			DROP TABLE IF EXISTS `'._DB_PREFIX_.'video_slekweb`;
		');
	}

	function postProcess()
	{
		if (Tools::isSubmit('add'))
		{
			$this->slkautoplay=(!isset($_POST['autoplay']) ? 0 : 1);
			$this->slkloop=(!isset($_POST['loop']) ? 0 : 1);
			Configuration::updateValue('VIDEOSLK_V', Tools::getValue('videolink'));
			Configuration::updateValue('VIDEOSLK_W', Tools::getValue('videow'));
			Configuration::updateValue('VIDEOSLK_H', Tools::getValue('videoh'));
			Configuration::updateValue('VIDEOSLK_A', $this->slkautoplay);
			Configuration::updateValue('VIDEOSLK_L', $this->slkloop);
		}elseif (Tools::isSubmit('add2')){
			if (!Db::getInstance()->Execute('INSERT INTO `'._DB_PREFIX_.'video_slekweb` (`id_produit`,`link_video`) VALUES ('.(int)Tools::getValue('id_produit').',"'.Tools::getValue('link_video').'") ON DUPLICATE KEY UPDATE `link_video`="'.Tools::getValue('link_video').'",actif=1')){
				//die(Tools::displayError('Error when Insert : doublon ?'));
			}
		}elseif (Tools::isSubmit('delete')){
			if(Db::getInstance()->execute('UPDATE `'._DB_PREFIX_.'video_slekweb` SET actif=0 WHERE id_produit='.Tools::getValue('delete').' LIMIT 1;'))
				$this->_html .= $this->displayConfirmation($this->l('Video deleted'));
		}
	}

	function getContent()
	{
		$this->postProcess();
		$output = '';
		$output .= '
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<fieldset>
				<legend>'.$this->l('Vidéo colonne droite, gauche ou page accueil)').'</legend>
				<label>'.$this->l('Lien Video :').' </label>
				<div class="margin-form">
					<input id="videolink" type="text" name="videolink" value="'.Tools::htmlentitiesUTF8(Tools::getValue('videolink', $this->slkvideo)).'" size="80" /> '.$this->l('ex: //www.youtube.com/embed/_-U2xcuQk-U').'
				</div>
				<label>'.$this->l('Largeur Video :').' </label>
				<div class="margin-form">
					<input id="videow" type="text" name="videow" value="'.Tools::htmlentitiesUTF8(Tools::getValue('videow', $this->slkwidth)).'" /> '.$this->l('ex: 200').'
				</div>
				<label>'.$this->l('Hauteur Video :').' </label>
				<div class="margin-form">
					<input id="videoh" type="text" name="videoh" value="'.Tools::htmlentitiesUTF8(Tools::getValue('videoh', $this->slkheight)).'" /> '.$this->l('ex: 165').'
				</div>
				<div class="margin-form">
					<p>
						<input type="checkbox" '.($this->slkautoplay==1 ? ' checked="checked" ' : '').' name="autoplay" id="autoplay" value="1" />
						<label class="t" for="autoplay"> '.$this->l('Autoplay').'</label>
					</p>
				</div>
				<div class="margin-form">
					<p>
						<input type="checkbox" '.($this->slkloop==1 ? ' checked="checked" ' : '').' name="loop" id="loop" value="1" />
						<label class="t" for="loop"> '.$this->l('Loop').'</label>
					</p>
				</div>
				<div class="margin-form">
					<input class="button" type="submit" name="add" value="'.$this->l('OK').'" />
				</div> 
			</fieldset>
		</form>';

		$output .= '
		<br />
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<fieldset>
				<legend>'.$this->l('Vidéo page produit').'</legend>
				<label>'.$this->l('Lien Video :').' </label>
				<div class="margin-form">
					<input id="link_video" type="text" name="link_video" value="'.Tools::htmlentitiesUTF8(Tools::getValue('link_video', $this->link_video)).'" size="80" /> '.$this->l('ex: //www.youtube.com/embed/_-U2xcuQk-U').'
				</div>
				<label>'.$this->l('ID produit :').' </label>
				<div class="margin-form">
					<input id="id_produit" type="text" name="id_produit" value="'.Tools::htmlentitiesUTF8(Tools::getValue('id_produit', $this->id_produit)).'" /> '.$this->l('ex: 50').'
				</div>';
				/*<label>'.$this->l('Largeur Video :').' </label>
				<div class="margin-form">
					<input id="videow" type="text" name="videow" value="'.Tools::htmlentitiesUTF8(Tools::getValue('videow', $this->slkwidth)).'" /> '.$this->l('ex: 200').'
				</div>
				<label>'.$this->l('Hauteur Video :').' </label>
				<div class="margin-form">
					<input id="videoh" type="text" name="videoh" value="'.Tools::htmlentitiesUTF8(Tools::getValue('videoh', $this->slkheight)).'" /> '.$this->l('ex: 165').'
				</div>*/
				$output .= '
				<div class="margin-form">
					<input class="button" type="submit" name="add2" value="'.$this->l('OK').'" />
				</div> 
			</fieldset>
		</form>';

		$sql = 'SELECT id_produit,link_video
				FROM `'._DB_PREFIX_.'video_slekweb` WHERE `actif`=1';
		$rq = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
		
		$output .= '
		<br />
		<fieldset>
			<legend>'.$this->l('Liste produits').'</legend>';
		$tab=array();
		foreach($rq as $value)
		{
			$output .= '<div class="margin-form">'.$value['id_produit'].' : '.$value['link_video'].' <a href="'.AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&delete='.(int)($value['id_produit']).'" title="'.$this->l('Delete').'"><img src="'._PS_ADMIN_IMG_.'delete.gif" alt="" /></a></div>';
		}
		$output .= '</fieldset>';

		return $output;
	}

	function hookLeftColumn()
	{
		global $smarty;
		$condition=0;
		$options='';
		if($this->slkautoplay==1){
			if($condition==0) $options.='?'; else $options.='&';
			$options.='autoplay=1';
			$condition++;
		}
			
		if($this->slkloop==1){
			if($condition==0) $options.='?'; else $options.='&';
			$options.='loop=1';
			$condition++;
		}
		$smarty->assign(array(
			'this_path' => $this->_path,
			'slkvideo' => $this->slkvideo.$options,
			'slkwidth' => $this->slkwidth,
			'slkheight' => $this->slkheight
		));

		return $this->display(__FILE__, 'videoslekweb.tpl');
	}

	function getLinkVideo()
	{
		$sql = 'SELECT link_video
				FROM `'._DB_PREFIX_.'video_slekweb` WHERE `id_produit`='.Tools::getValue('id_product');
		$rq = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);

		if(!empty($rq)) return $rq[0]['link_video']; else return false;
	}

	function hookRightColumn()
	{
		return $this->hookLeftColumn();
	}

	function hookHome()
	{
		return $this->hookLeftColumn();
	}	

	function hookProductFooter()
	{
		global $smarty;
		$video=$this->getLinkVideo();
		if(!empty($video)){
			$smarty->assign(array(
				'this_path' => $this->_path,
				'slkvideo' => $video.$options,
				'slkwidth' => $this->slkwidth,
				'slkheight' => $this->slkheight
			));
			return ($this->display(__FILE__, '/videoproduct.tpl'));
		}
	}
}