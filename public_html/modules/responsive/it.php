<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{responsive}prestashop>responsive-account_4394c8d8e63c470de62ced3ae85de5ae'] = 'Logout';
$_MODULE['<{responsive}prestashop>responsive_bd55c8a8a81cea7c239bb51f776ae688'] = 'Layout fluido reattivo';
$_MODULE['<{responsive}prestashop>responsive_e605cf1aeb90701696e960de896c5323'] = 'Aggiungere layout fluido al tema';
$_MODULE['<{responsive}prestashop>responsive_f4f70727dc34561dfde1a3c529b6205c'] = 'Impostazioni';
$_MODULE['<{responsive}prestashop>responsive_67dea5fbf899bee41027413fc1c9fa7f'] = 'Codice mappe';
$_MODULE['<{responsive}prestashop>responsive_9887a4451812854f0f1b6f669a874307'] = 'Contribuire';
$_MODULE['<{responsive}prestashop>responsive_e8928d746f7cf273fa76aabf3f906216'] = 'Puoi contribuire con una donazione se i nostri moduli gratuiti e temi sono utili per voi. Clicca sul link e sostenerci!';
$_MODULE['<{responsive}prestashop>responsive_4ad5cd8425d9ac520903c1819517f0e1'] = 'Per ulteriori moduli & temi visitare: www.catalogo-onlinersi.com.ar';
