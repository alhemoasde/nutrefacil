<?php
/**
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * No redistribute in other sites, or copy.
 *
 * @author    RSI <demo@demo.com>
 * @copyright 2007-2015 RSI
 * @license   http://localhost
 */

ini_set('allow_url_fopen', true);

class Responsive extends Module
{
	private $_html = '';
	private $_postErrors = array();

	public function __construct()
	{
		$this->name = 'responsive';
		if (_PS_VERSION_ < '1.4.0.0')
			$this->tab = 'Home';
		if (_PS_VERSION_ > '1.4.0.0' && _PS_VERSION_ < '1.5.0.0')
		{
			$this->tab           = 'front_office_features';
			$this->author        = 'RSI';
			$this->need_instance = 0;
		}
		if (_PS_VERSION_ > '1.5.0.0')
		{
			$this->tab    = 'front_office_features';
			$this->author = 'RSI';
		}
if (_PS_VERSION_ > '1.6.0.0')

			$this->bootstrap = true;

		$this->version = '1.1.0';

		parent::__construct();

		$this->displayName = $this->l('Responsive-Fluid layout');
		$this->description = $this->l('Add fluid layout to the theme');
		if (_PS_VERSION_ < '1.5')
			require(_PS_MODULE_DIR_.$this->name.'/backward_compatibility/backward.php');

	}

	public function install()
	{
		if (!parent::install() || !$this->registerHook('header'))
			return false;
		return true;
	}

	public function postProcess()
	{
		$errors = false;

		if ($errors)
			echo $this->displayError($errors);
	}

	public function displayForm()
	{
		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		$languages       = Language::getLanguages();
		$iso             = Language::getIsoById($defaultLanguage);
		$divLangName     = 'link_label';

		$this->_html .= '
	<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset><legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Settings').'</legend>
				<label>'.$this->l('Maps code').'</label>
				
	
	
						<center><a href="../modules/responsive/moduleinstall.pdf">README</a></center><br/>
						<center><a href="../modules/responsive/termsandconditions.pdf">TERMS</a></center><br/>
						 <center>  <p>Follow  us:</p></center>
     <center><p><a href="https://www.facebook.com/ShackerRSI" target="_blank"><img src="../modules/responsive/views/img/facebook.png" style="  width: 64px;margin: 5px;" /></a>
        <a href="https://twitter.com/prestashop_rsi" target="_blank"><img src="../modules/responsive/views/img/twitter.png" style="  width: 64px;margin: 5px;" /></a>
         <a href="https://www.pinterest.com/prestashoprsi/" target="_blank"><img src="../modules/responsive/views/img/pinterest.png" style="  width: 64px;margin: 5px;" /></a>
           <a href="https://plus.google.com/+shacker6/posts" target="_blank"><img src="../modules/responsive/views/img/googleplus.png" style="  width: 64px;margin: 5px;" /></a>
            <a href="https://www.linkedin.com/profile/view?id=92841578" target="_blank"><img src="../modules/responsive/views/img/linkedin.png" style="  width: 64px;margin: 5px;" /></a></p></center>
			<br/>
			<p>Video:</p>
	<iframe width="640" height="360" src="https://www.youtube.com/embed/6AZfhaIQnNY?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			
			<p>Recommended:</p>
<object type="text/html" data="http://catalogo-onlinersi.net/modules/productsanywhere/images.php?idproduct=&desc=yes&buy=yes&type=home_default&price=yes&style=false&color=10&color2=40&bg=ffffff&width=800&height=310&lc=000000&speed=5&qty=15&skip=29,14,42,44,45&sort=1" width="800" height="310" style="border:0px #066 solid;"></object>	
			</fieldset>
		</form>
	
		<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<fieldset><legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Contribute').'</legend>
				<p class="clear">'.$this->l('You can contribute with a donation if our free modules and themes are usefull for you. Clic on the link and support us!').'</p>
				<p class="clear">'.$this->l('For more modules & themes visit: www.catalogo-onlinersi.com.ar').'</p>
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="HMBZNQAHN9UMJ">
<input type="image" src="https://www.paypalobjects.com/WEBSCR-640-20110401-1/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/WEBSCR-640-20110401-1/en_US/i/scr/pixel.gif" width="1" height="1">
	</fieldset>
</form>
';






		return $this->_html;
	}

	public function getContent()
	{
		$errors = '';
		$this->postProcess();
if (_PS_VERSION_ < '1.6.0.0')
		{
		if (Tools::isSubmit('submitResponsive'))
		{
			$nbr  = Tools::getValue('nbr');
			$text = Tools::getValue('text');
			Configuration::updateValue('RESPONSIVE_NBR', $nbr);

			Configuration::updateValue('RESPONSIVE_TEXT', $text);
			

	
			$this->_html .= @$errors == '' ? $this->displayConfirmation('Settings updated successfully') : @$errors;

		}

		return $this->displayForm();
		}
		else
			return $this->postProcess().$this->_displayInfo().$this->_displayAdds();
	}
	private function _displayInfo()
	{
		return $this->display(__FILE__, 'views/templates/hook/infos.tpl');
	}

	private function _displayAdds()
	{
		return $this->display(__FILE__, 'views/templates/hook/adds.tpl');
	}
	

	public function hookHeader($params)
	{
		$psversion = _PS_VERSION_;
		if (_PS_VERSION_ > '1.4.0.0' && _PS_VERSION_ < '1.5.0.0')
		{

Tools::addCSS(__PS_BASE_URI__.'modules/responsive/views/css/fluid.css', 'all');
Tools::addJS(__PS_BASE_URI__.'modules/responsive/views/js/layout.js');
		}
		if (_PS_VERSION_ > '1.5.0.0')
		{
			/*onlycss*/
			$this->context->controller->addCSS(($this->_path).'views/css/fluid.css', 'all');
			/**/
			$this->context->controller->addJS(($this->_path).'views/js/layout.js');

		}

		$psversion = _PS_VERSION_;
		$this->context->smarty->assign(array('psversion' => $psversion));


		//return $this->display(__FILE__, 'views/templates/front/responsive-header.tpl');
	}

}

?>