<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{responsive}prestashop>responsive-account_4394c8d8e63c470de62ced3ae85de5ae'] = 'Deconecta';
$_MODULE['<{responsive}prestashop>responsive_b2b2df378a7c7ade199bbaf944cb3f35'] = 'Receptiv';
$_MODULE['<{responsive}prestashop>responsive_0f7384d43ce777c89b826c17e972ef15'] = 'Adăugaţi efect receptiv la tema';
$_MODULE['<{responsive}prestashop>responsive_f4f70727dc34561dfde1a3c529b6205c'] = 'Setări';
$_MODULE['<{responsive}prestashop>responsive_b6342c548d0a61e142bc25bb46abf1fc'] = 'Culoare de strălucire';
$_MODULE['<{responsive}prestashop>responsive_141f7897a85174ba350ff4b4d2b1a712'] = 'Culoare de strălucire (implicit 000000)';
$_MODULE['<{responsive}prestashop>responsive_c9cc8cce247e49bae79f15173ce97354'] = 'Salvare';
