<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{responsive}prestashop>responsive-account_4394c8d8e63c470de62ced3ae85de5ae'] = '注销';
$_MODULE['<{responsive}prestashop>responsive_bd55c8a8a81cea7c239bb51f776ae688'] = '有反应的流体布局';
$_MODULE['<{responsive}prestashop>responsive_e605cf1aeb90701696e960de896c5323'] = '将流体布局添加到主题';
$_MODULE['<{responsive}prestashop>responsive_f4f70727dc34561dfde1a3c529b6205c'] = '设置';
$_MODULE['<{responsive}prestashop>responsive_67dea5fbf899bee41027413fc1c9fa7f'] = '映射代码';
$_MODULE['<{responsive}prestashop>responsive_9887a4451812854f0f1b6f669a874307'] = '贡献';
$_MODULE['<{responsive}prestashop>responsive_e8928d746f7cf273fa76aabf3f906216'] = '如果我们的免费模块和主题是对你有用，你可以贡献与捐赠。点击该链接并支持我们!';
$_MODULE['<{responsive}prestashop>responsive_4ad5cd8425d9ac520903c1819517f0e1'] = '更多模块 & 主题请访问: www.catalogo-onlinersi.com.ar';
