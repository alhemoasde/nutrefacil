<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{responsive}prestashop>responsive-account_4394c8d8e63c470de62ced3ae85de5ae'] = 'Uitloggen';
$_MODULE['<{responsive}prestashop>responsive_bd55c8a8a81cea7c239bb51f776ae688'] = 'Responsive-vloeistof lay-out';
$_MODULE['<{responsive}prestashop>responsive_e605cf1aeb90701696e960de896c5323'] = 'Vloeibare lay-out toevoegen aan het thema';
$_MODULE['<{responsive}prestashop>responsive_f4f70727dc34561dfde1a3c529b6205c'] = 'Instellingen';
$_MODULE['<{responsive}prestashop>responsive_67dea5fbf899bee41027413fc1c9fa7f'] = 'Kaarten code';
$_MODULE['<{responsive}prestashop>responsive_9887a4451812854f0f1b6f669a874307'] = 'Bijdragen';
$_MODULE['<{responsive}prestashop>responsive_e8928d746f7cf273fa76aabf3f906216'] = 'U kunt bijdragen met een donatie als onze gratis modules en thema\'s nuttig voor u zijn. Klik op de link en steun ons!';
$_MODULE['<{responsive}prestashop>responsive_4ad5cd8425d9ac520903c1819517f0e1'] = 'Voor meer modules & thema\'s bezoek: www.catalogo-onlinersi.com.ar';
