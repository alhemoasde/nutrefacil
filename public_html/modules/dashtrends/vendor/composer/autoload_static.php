<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInite212732fdcef3329eb7c226a1db621e2
{
    public static $classMap = array (
        'dashtrends' => __DIR__ . '/../..' . '/dashtrends.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInite212732fdcef3329eb7c226a1db621e2::$classMap;

        }, null, ClassLoader::class);
    }
}
