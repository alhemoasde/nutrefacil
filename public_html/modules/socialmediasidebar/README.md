# Social media sidebar (socialmediasidebar)

### Current version: 1.3.1

| PS compatibility       | Tested?            |    Ok?                |
| ---------------------- |:------------------:| ---------------------:|
| 1.6                    | :heavy_check_mark: |    :heavy_check_mark: |
| 1.7                    | :heavy_check_mark: |    :heavy_check_mark: |

Displays fixed sidebar on the left side with social media buttons.

### Detailed description, changelog, download:
http://prestacraft.com/free-modules/home/9-social-media-sidebar.html

### Your contribution
If You have any idea to improve this module please open an issue or pull request.

### Disclaimer
This software is provided "as is" without warranty of any kind.
