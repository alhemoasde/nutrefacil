<?php
/**
*  @author ST-themes https://www.sunnytoo.com
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class StTidio extends Module
{
    public $_html = '';
    public $fields_form;
    public $fields_value;
    public $validation_errors = array();
    public $_prefix_st = 'ST_TIDIO_';

    public function __construct()
    {
        $this->name          = 'sttidio';
        $this->tab           = 'front_office_features';
        $this->version       = '1.0.0';
        $this->author        = 'sunnytoo.com';
        $this->need_instance = 0;
        $this->bootstrap     = true;
        
        parent::__construct();
        
        $this->displayName = $this->l('Tidio PrestaShop');
        $this->description = $this->l('Integrate Tidio chat to your site.');
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    }
    public function install()
    {
        $result = true;
        if (!parent::install()
            || !Configuration::updateValue($this->_prefix_st.'CODE', '')
            || !Configuration::updateValue($this->_prefix_st.'LANG', 1)
            || !Configuration::updateValue($this->_prefix_st.'DELAY', 0)
            || !Configuration::updateValue($this->_prefix_st.'PAGES', '')
            || !$this->registerHook('displayHeader')
        ) {
             $result = false;
        }

        return $result;
    }
    
    public function uninstall()
    {
        if (!parent::uninstall()
        ) {
            return false;
        }
        return true;
    }
    public function getContent()
    {
        $this->initFieldsForm();
        if (isset($_POST['savesttidio']))
        {
            foreach($this->fields_form as $form)
                foreach($form['form']['input'] as $field)
                    if(isset($field['validation']))
                    {
                        $ishtml = ($field['validation']=='isAnything') ? true : false;
                        $errors = array();       
                        $value = Tools::getValue($field['name']);
                        if (isset($field['required']) && $field['required'] && $value==false && (string)$value != '0')
                                $errors[] = sprintf(Tools::displayError('Field "%s" is required.'), $field['label']);
                        elseif($value)
                        {
                            $field_validation = $field['validation'];
                            if (!Validate::$field_validation($value))
                                $errors[] = sprintf(Tools::displayError('Field "%s" is invalid.'), $field['label']);
                        }
                        // Set default value
                        if ($value === false && isset($field['default_value']))
                            $value = $field['default_value'];
                        
                        if(count($errors))
                        {
                            $this->validation_errors = array_merge($this->validation_errors, $errors);
                        }
                        elseif($value==false)
                        {
                            switch($field['validation'])
                            {
                                case 'isUnsignedId':
                                case 'isUnsignedInt':
                                case 'isInt':
                                case 'isBool':
                                    $value = 0;
                                break;
                                default:
                                    $value = '';
                                break;
                            }
                            Configuration::updateValue($this->_prefix_st.strtoupper($field['name']), $value);
                        }
                        else{
                            if($field['name']=='code')
                            {
                                preg_match("/\/\/code.tidio.co\/([^\.]*)\.js/", $value,$match);
                                $value = $match ? $match[1] : '';
                            }
                            Configuration::updateValue($this->_prefix_st.strtoupper($field['name']), $value, $ishtml);
                        }
                    }
                                                 
            if(count($this->validation_errors))
                $this->_html .= $this->displayError(implode('<br/>',$this->validation_errors));
            else 
                $this->_html .= $this->displayConfirmation($this->l('Settings updated'));
            $this->_clearCache('*');
        }

        $helper = $this->initForm();
        
        return $this->_html.$helper->generateForm($this->fields_form).'<div class="alert alert-info">This free module was created by <a href="https://www.sunnytoo.com" target="_blank">ST-THEMES</a>, it\'s not allow to sell it, it\'s also not allow to create new modules based on this one. Check more <a href="https://www.sunnytoo.com/blogs?term=743&orderby=date&order=desc" target="_blank">free modules</a>, <a href="https://www.sunnytoo.com/product-category/prestashop-modules" target="_blank">advanced paid modules</a> and <a href="https://www.sunnytoo.com/product-category/prestashop-themes" target="_blank">themes(transformer theme and panda  theme)</a> created by <a href="https://www.sunnytoo.com" target="_blank">ST-THEMES</a>.</div>';
    }
    public function getCMSpages()
    {
        $cms_tab = array();
        foreach (CMS::listCms($this->context->language->id) as $cms_file) {
            $cms_tab[] = array('id' => $cms_file['id_cms'], 'name' => $cms_file['meta_title']);
        }
        return $cms_tab;
    }
    protected function initFieldsForm()
    {
        $this->fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Tidio'),
                'icon' => 'icon-cogs'
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Tidio code:'),
                    'name' => 'code',
                    'validation' => 'isAnything',
                    'desc' => $this->l('Paste the Tidio code, this is how the code looks like').' &lt;script src="//code.tidio.co/aabbccddee.js" async&gt;&lt;/script&gt;',
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Don\'t show Tidio on these pages:'),
                    'name' => 'pages',
                    'validation' => 'isAnything',
                    'desc' => $this->l('Put page names here, separated by commas (",")'),
                ),
                array(
                    'type' => 'text',
                    'label' => $this->l('Delay to show:'),
                    'name' => 'delay',
                    'prefix' => 's',
                    'class' => 'fixed-width-lg',
                    'validation' => 'isUnsignedInt',
                ),
                array(
                    'type' => 'switch',
                    'label' => $this->l('Auto detect language:'),
                    'name' => 'lang',
                    'is_bool' => true,
                    'default_value' => 1,
                    'values' => array(
                        array(
                            'id' => 'lang_on',
                            'value' => 1,
                            'label' => $this->l('Yes')),
                        array(
                            'id' => 'lang_off',
                            'value' => 0,
                            'label' => $this->l('No')),
                    ),
                    'desc' => $this->l('Go to tidio.com to enable languages you need.'),
                    'validation' => 'isUnsignedInt',
                ), 
            ),
            'submit' => array(
                'title' => $this->l('   Save   ')
            )
        );
    }
    protected function initForm()
    {
        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table =  $this->table;
        $helper->module = $this;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;

        $helper->identifier = $this->identifier;
        $helper->submit_action = 'savesttidio';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        return $helper;
    }
    
    private function getConfigFieldsValues()
    {
        $fields_values = array(
            'code' => Configuration::get($this->_prefix_st.'CODE'),
            'pages' => Configuration::get($this->_prefix_st.'PAGES'),
            'delay' => Configuration::get($this->_prefix_st.'DELAY'),
            'lang' => Configuration::get($this->_prefix_st.'LANG'),
        );
        if($fields_values['code'])
            $fields_values['code'] = '<script src="//code.tidio.co/'.$fields_values['code'].'.js" async></script>';
        return $fields_values;
    }
    public function hookDisplayHeader($params)
    {
        $page = Context::getContext()->smarty->getTemplateVars('page');
        if(Configuration::get($this->_prefix_st.'PAGES') && strpos(','.$page['page_name'], ','.Configuration::get($this->_prefix_st.'PAGES')) !== false)
            return false;
        Media::addJsDef(array('sttidio' => array(
            'code' => Configuration::get($this->_prefix_st.'CODE'),
            'delay' => (int)Configuration::get($this->_prefix_st.'DELAY'),
            'lang' => (int)Configuration::get($this->_prefix_st.'LANG'),
        )));
        $this->context->controller->addJS($this->_path.'views/js/front.js');
    }
}
