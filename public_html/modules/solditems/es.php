<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{solditems}prestashop>infos_86d25b8026725cd8e9658fb01bf3194a'] = 'Para una versión completa de este módulo, con auto instalación y confirmación al cliente realizar el pago, consulte WesterUnion +:';
$_MODULE['<{solditems}prestashop>solditems_d4fd7635cd5a92dd744ae5ccc853e35d'] = 'Artículos vendidos';
$_MODULE['<{solditems}prestashop>solditems_40b03a205f85aae1e7f7351b9b367284'] = 'Muestra la cantidad vendida de un producto - www.catalogo-onlinersi.net';
$_MODULE['<{solditems}prestashop>solditems_c888438d14855d7d96a2724ee9c306bd'] = 'Configuración actualizada';
$_MODULE['<{solditems}prestashop>solditems_f4f70727dc34561dfde1a3c529b6205c'] = 'Configuración';
$_MODULE['<{solditems}prestashop>solditems_da15f26e93917c69ad5b6da1249df8d3'] = 'Mostrar elementos solded más de:';
$_MODULE['<{solditems}prestashop>solditems_e73348cee6e9b3ce6ef3333af4ecb94b'] = 'Estilo de imagen:';
$_MODULE['<{solditems}prestashop>solditems_c4ca4238a0b923820dcc509a6f75849b'] = '1';
$_MODULE['<{solditems}prestashop>solditems_c81e728d9d4c2f636f067f89cc14862c'] = '2';
$_MODULE['<{solditems}prestashop>solditems_eccbc87e4b5ce2fe28308fd9f2a7baf3'] = '3';
$_MODULE['<{solditems}prestashop>solditems_a87ff679a2f3e71d9181a67b7542122c'] = '4';
$_MODULE['<{solditems}prestashop>solditems_e4da3b7fbbce2345d7772b0674a318d5'] = '5';
$_MODULE['<{solditems}prestashop>solditems_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{solditems}prestashop>solditems_9887a4451812854f0f1b6f669a874307'] = 'Contribuir';
$_MODULE['<{solditems}prestashop>solditems_e8928d746f7cf273fa76aabf3f906216'] = 'Usted puede contribuir con una donación si nuestros módulos libres y temas son útiles para usted. Haga clic en el enlace y nos apoyen!';
$_MODULE['<{solditems}prestashop>solditems_4ad5cd8425d9ac520903c1819517f0e1'] = 'Más módulos y temas visite: www.catalogo-onlinersi.net';
$_MODULE['<{solditems}prestashop>solditems_254f642527b45bc260048e30704edb39'] = 'Configuración';
$_MODULE['<{solditems}prestashop>solditems_52898b080e886d440c6244f0bbe6be9f'] = 'Orden estado ID';
$_MODULE['<{solditems}prestashop>solditems_8ff86705ea42bdb6028f78790b404b86'] = 'Estado del pedido (se rellenará automáticamente)';
$_MODULE['<{solditems}prestashop>solditems_ddebde5253c8c1da051b3cd822bbf467'] = 'Leer el README para obtener este número de identificación';
$_MODULE['<{solditems}prestashop>solditems_ebf3d9d42804d691d712f778ea57f53b'] = 'Tipo de imagen';
$_MODULE['<{solditems}prestashop>solditems_b17f3f4dcf653a5776792498a9b44d6a'] = 'Configuración de actualización';
$_MODULE['<{solditems}prestashop>solditems_3576ed26d17b566642647c100113f6f1'] = 'Este artículo ha sido vendido';
$_MODULE['<{solditems}prestashop>solditems_f2b798f672d4b42c0359ced11d4f10cd'] = 'tiempos';
