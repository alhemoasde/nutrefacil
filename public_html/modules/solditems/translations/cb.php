<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{solditems}prestashop>solditems_d4fd7635cd5a92dd744ae5ccc853e35d'] = 'Artículos vendidos';
$_MODULE['<{solditems}prestashop>solditems_8a8fe762c42c0d252bb8b3e1dd6c2e6d'] = 'Muestra la cantidad vendida de un producto';
$_MODULE['<{solditems}prestashop>solditems_c888438d14855d7d96a2724ee9c306bd'] = 'Configuración actualizada';
$_MODULE['<{solditems}prestashop>solditems_f4f70727dc34561dfde1a3c529b6205c'] = 'Configuración';
$_MODULE['<{solditems}prestashop>solditems_da15f26e93917c69ad5b6da1249df8d3'] = 'Mostrar artículos vendidos más de:';
$_MODULE['<{solditems}prestashop>solditems_e73348cee6e9b3ce6ef3333af4ecb94b'] = 'Estilo de imagen:';
$_MODULE['<{solditems}prestashop>solditems_3576ed26d17b566642647c100113f6f1'] = 'Este artículo se ha vendido';
$_MODULE['<{solditems}prestashop>solditems_f2b798f672d4b42c0359ced11d4f10cd'] = 'veces';
