<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_fdf0a0f26be17259f9b6160078285433'] = '反應靈敏的 Iframe';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_cdcbf1204b17feb5f2743be000ab4224'] = '調整為較小的螢幕-iframe www.catalogo-onlinersi.net';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_254f642527b45bc260048e30704edb39'] = '配置';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_32802122a081816d701f505bf6fd8bbf'] = '寬屏模式';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_7fbaad05d376326bb1a7a30cd5e52120'] = '使用寬屏 （對於 vimeo 視頻）';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_c9cc8cce247e49bae79f15173ce97354'] = '保存';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_f4d1ea475eaa85102e2b4e6d95da84bd'] = '確認';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_c888438d14855d7d96a2724ee9c306bd'] = '設置更新';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_f4f70727dc34561dfde1a3c529b6205c'] = '設置';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_a6105c0a611b41b08f1209506350279e'] = '是的';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_7fa3b767c460b54a2be4d49030b349c7'] = '沒有';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_113d665fe5d9f23aba45cfe3a9ab7dff'] = '使用寬屏模式 （videmo)';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_9887a4451812854f0f1b6f669a874307'] = '貢獻';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_e8928d746f7cf273fa76aabf3f906216'] = '如果我們的免費模組和主題是對你有用，你可以貢獻與捐贈。點擊該連結並支援我們 ！';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_79674286356b8bc9c2b77d21247c953d'] = '更多模組 & 主題請訪問︰ www.catalogo-onlinersi.net';
