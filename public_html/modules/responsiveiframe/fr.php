<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_fdf0a0f26be17259f9b6160078285433'] = 'Iframes réactives';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_cdcbf1204b17feb5f2743be000ab4224'] = 'Ajuster les iframes pour petits écrans - www.catalogo-onlinersi.net';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_254f642527b45bc260048e30704edb39'] = 'Configuration';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_32802122a081816d701f505bf6fd8bbf'] = 'Mode écran large';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_7fbaad05d376326bb1a7a30cd5e52120'] = 'Écran large utilisation (pour les vidéos vimeo)';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_f4d1ea475eaa85102e2b4e6d95da84bd'] = 'Confirmation';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_c888438d14855d7d96a2724ee9c306bd'] = 'Paramètres mis à jour';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_f4f70727dc34561dfde1a3c529b6205c'] = 'Paramètres';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_a6105c0a611b41b08f1209506350279e'] = 'Oui';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_7fa3b767c460b54a2be4d49030b349c7'] = 'ne';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_113d665fe5d9f23aba45cfe3a9ab7dff'] = 'Utilisez le mode écran large (pour videmo)';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_9887a4451812854f0f1b6f669a874307'] = 'Contribuer';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_e8928d746f7cf273fa76aabf3f906216'] = 'Vous pouvez contribuer par un don, si nos modules gratuits et les thèmes sont utiles pour vous. Cliquez sur le lien et de nous soutenir !';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_79674286356b8bc9c2b77d21247c953d'] = 'Pour plus de modules & thèmes, visitez : www.catalogo-onlinersi.net';
