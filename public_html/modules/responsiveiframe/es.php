<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_fdf0a0f26be17259f9b6160078285433'] = 'Iframes sensible';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_cdcbf1204b17feb5f2743be000ab4224'] = 'Ajustar iframes para pantallas pequeñas - www.catalogo-onlinersi.net';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_254f642527b45bc260048e30704edb39'] = 'Configuración';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_32802122a081816d701f505bf6fd8bbf'] = 'Modo panorámico';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_7fbaad05d376326bb1a7a30cd5e52120'] = 'Pantalla ancha de uso (para videos de vimeo)';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_c9cc8cce247e49bae79f15173ce97354'] = 'Guardar';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_f4d1ea475eaa85102e2b4e6d95da84bd'] = 'Confirmación';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_c888438d14855d7d96a2724ee9c306bd'] = 'Configuración actualizada';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_f4f70727dc34561dfde1a3c529b6205c'] = 'Configuración';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_a6105c0a611b41b08f1209506350279e'] = 'sí';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_7fa3b767c460b54a2be4d49030b349c7'] = 'No';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_113d665fe5d9f23aba45cfe3a9ab7dff'] = 'Utilice el modo de pantalla ancha (para el videmo)';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_9887a4451812854f0f1b6f669a874307'] = 'Contribuir';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_e8928d746f7cf273fa76aabf3f906216'] = 'Usted puede contribuir con una donación si nuestros módulos libres y temas son útiles para usted. Haga clic en el enlace y nos apoyen!';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_79674286356b8bc9c2b77d21247c953d'] = 'Más módulos y temas visite: www.catalogo-onlinersi.net';
