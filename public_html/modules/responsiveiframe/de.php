<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_fdf0a0f26be17259f9b6160078285433'] = 'Reaktionsschnelle Iframes';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_cdcbf1204b17feb5f2743be000ab4224'] = 'Stellen Sie Iframes für kleine Bildschirme - www.catalogo-onlinersi.net';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_254f642527b45bc260048e30704edb39'] = 'Konfiguration';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_32802122a081816d701f505bf6fd8bbf'] = 'Breitbild-Modus';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_7fbaad05d376326bb1a7a30cd5e52120'] = 'Verwenden Sie Widescreen (für Vimeo-Videos)';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_c9cc8cce247e49bae79f15173ce97354'] = 'Speichern';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_f4d1ea475eaa85102e2b4e6d95da84bd'] = 'Bestätigung';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_c888438d14855d7d96a2724ee9c306bd'] = 'Einstellungen aktualisiert';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_f4f70727dc34561dfde1a3c529b6205c'] = 'Einstellungen';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_a6105c0a611b41b08f1209506350279e'] = 'Ja';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_7fa3b767c460b54a2be4d49030b349c7'] = 'Nein';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_113d665fe5d9f23aba45cfe3a9ab7dff'] = 'Breitbild-Modus (für Videmo) verwenden';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_9887a4451812854f0f1b6f669a874307'] = 'Einen Beitrag leisten';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_e8928d746f7cf273fa76aabf3f906216'] = 'Sie können mit einer Spende dazu beitragen, wenn unsere kostenlose Module und Themen für Sie nützlich sind. Klicken Sie auf den Link und unterstützen Sie uns!';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_79674286356b8bc9c2b77d21247c953d'] = 'Weitere Module & Themen finden Sie auf: www.catalogo-onlinersi.net';
