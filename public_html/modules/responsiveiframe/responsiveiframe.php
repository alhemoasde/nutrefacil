<?php
/**
 * 2007-2014 PrestaShop
 *
 * NOTICE OF LICENSE
 *
 * No redistribute in other sites, or copy.
 *
 * @author    shacker RSI
 * @copyright 2007-2014 RSI
 * @license   http://catalogo-onlinersi.net
 */

if (!defined('_PS_VERSION_'))
	exit;

class ResponsiveIframe extends Module
{
	public function __construct()
	{
		$this->name = 'responsiveiframe';
		if (_PS_VERSION_ < '1.4.0.0')

			$this->tab = 'Blocks';
		if (_PS_VERSION_ > '1.4.0.0' && _PS_VERSION_ < '1.5.0.0')

			$this->tab = 'front_office_features';

		if (_PS_VERSION_ > '1.5.0.0')

			$this->tab = 'front_office_features';

		if (_PS_VERSION_ > '1.6.0.0')

			$this->bootstrap = true;
		$this->version       = '2.0.0';
		$this->author        = 'RSI Sistemas';
		$this->need_instance = 0;

		parent::__construct();

		$this->displayName = $this->l('Responsive Iframes');
		$this->description = $this->l('Adjust iframes for small screens - www.catalogo-onlinersi.net');
		if (_PS_VERSION_ < '1.5')
			require(_PS_MODULE_DIR_.$this->name.'/backward_compatibility/backward.php');
	}

	public function install()
	{
		if (!parent::install() || !$this->registerHook('header') || !$this->registerHook('footer') || !$this->registerHook('displayBeforeBodyClosingTag'))
			return false;

		if (!Configuration::updateValue('RESPOI_NBR', '0'))
			return false;
			return true;
	}
	public function getPages($pety)
	{
		$result = Db::getInstance()->ExecuteS('
		SELECT pt.`id_page_type`,pt.`name`,p.`id_page_type`,p.`id_page`,p.`id_object`  
		FROM `'._DB_PREFIX_.'page` p
		LEFT JOIN `'._DB_PREFIX_.'page_type` pt ON p.`id_page_type` = pt.`id_page_type`
		WHERE p.`id_page` = '.(int)$pety.' LIMIT 1');		
		return ($result);
	}
	public function postProcess()
	{
		$errors = '';
		if (Tools::isSubmit('submitResponsiveIframe'))
		{
			if ($nbr = Tools::getValue('nbr'))
				Configuration::updateValue('RESPOI_NBR', $nbr);
			elseif (Shop::getContext() == Shop::CONTEXT_SHOP || Shop::getContext() == Shop::CONTEXT_GROUP)
				Configuration::deleteFromContext('RESPOI_NBR');

		

			// Reset the module properties
			//	$this->initialize();


			if (!$errors)
				Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&conf=6');
			echo $this->displayError($errors);
		}

	}

	public function getConfigFieldsValues()
	{
		return array(
			'nbr'     => Tools::getValue('nbr', Configuration::get('RESPOI_NBR')),
		
			
		);
	}

	public function renderForm()
	{
		$this->postProcess();




		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Configuration'),
					'icon'  => 'icon-cogs'
				),
				'input'  => array(
									                    array(
                        'type' => 'switch',
                        'label' => $this->l('Widescreen mode'),
                        'name' => 'nbr',
                        'description' =>  $this->l('Use widescreen (for vimeo videos)'),
                        'values' => array(
                            array(
                                'id' => 'on',
                                'value' => 1
                            ),
                            array(
                                'id' => 'off',
                                'value' => 0
                            )
                        )
                    ),
					
				

				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper                           = new HelperForm();
		$helper->show_toolbar             = false;
		$helper->table                    = $this->table;
		$lang                             = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language    = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form                = array();
		$helper->identifier               = $this->identifier;
		$helper->submit_action            = 'submitResponsiveIframe';
		$helper->currentIndex             = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token                    = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars                 = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages'    => $this->context->controller->getLanguages(),
			'id_language'  => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}

	
	public function getContent()
	{
		$errors = '';
		if (_PS_VERSION_ < '1.6.0.0')
		{
			$output = '<h2>'.$this->displayName.'</h2>';
			if (Tools::isSubmit('submitResponsiveIframe'))
			{
			
					$nbr        = Tools::getValue('nbr');
					
					Configuration::updateValue('RESPOI_NBR', $nbr);
				
	
				
					$output .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('Confirmation').'" />'.$this->l('Settings updated').'</div>';
				
			}
			return $output.$this->displayForm();
		}
		else
		return $this->postProcess().$this->_displayInfo().$this->renderForm().$this->_displayAdds();
	}

	private function _displayInfo()
	{
		return $this->display(__FILE__, 'views/templates/hook/infos.tpl');
	}

	private function _displayAdds()
	{
		return $this->display(__FILE__, 'views/templates/hook/adds.tpl');
	}

	public function displayForm()
	{
		$output = '
		
		<form action="'.Tools::safeOutput($_SERVER['REQUEST_URI']).'" method="post">
			<fieldset><legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Settings').'</legend>
	
		
				<label>'.$this->l('Widescreen mode').'</label>
				<div class="margin-form">
					
					<select name="nbr" id="nbr">
					
					<option value="1" '.(Configuration::get('RESPOI_NBR') == '1' ? 'selected' : '').'>'.$this->l('yes').'</option>
			<option value="0" '.(Configuration::get('RESPOI_NBR') == '0' ? 'selected' : '').'>'.$this->l('no').'</option>

					</select>
					<p class="clear">'.$this->l('Use widescreen mode (for videmo)').'</p>
					
					
				
		</div>
					<center><input type="submit" name="submitResponsiveIframe" value="'.$this->l('Save').'" class="button" /></center><br/>

						<center><a href="../modules/oldbrowser/moduleinstall.pdf">README</a></center><br/>
						<center><a href="../modules/oldbrowser/termsandconditions.pdf">TERMS</a></center><br/>
						
			</fieldset>
		</form><br/>
<fieldset>
		Video demo:<br/>
	<iframe width="560" height="315" src="https://www.youtube.com/embed/2KYfndnlcFw?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe><br/>
	Other products:<br/>
	<object type="text/html" data="http://catalogo-onlinersi.net/modules/productsanywhere/images.php?idproduct=&desc=yes&buy=yes&type=home_default&price=yes&style=false&color=10&color2=40&bg=ffffff&width=800&height=310&lc=000000&speed=5&qty=15&skip=29,14,42,44,45&sort=1" width="800" height="310" style="border:0px #066 solid;"></object>
</fieldset>
<br/>
<form action="https://www.paypal.com/cgi-bin/webscr" method="post">
			<fieldset><legend><img src="'.$this->_path.'logo.gif" alt="" title="" />'.$this->l('Contribute').'</legend>
				<p class="clear">'.$this->l('You can contribute with a donation if our free modules and themes are usefull for you. Clic on the link and support us!').'</p>
				<p class="clear">'.$this->l('For more modules & themes visit: www.catalogo-onlinersi.net').'</p>
<input type="hidden" name="cmd" value="_s-xclick">
<input type="hidden" name="hosted_button_id" value="HMBZNQAHN9UMJ">
<input type="image" src="https://www.paypalobjects.com/WEBSCR-640-20110401-1/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
<img alt="" border="0" src="https://www.paypalobjects.com/WEBSCR-640-20110401-1/en_US/i/scr/pixel.gif" width="1" height="1">
	</fieldset>
</form>
		';
		return $output;
	}



	public function hookFooter($params)
	{
		$ptypee=Page::getCurrentId();
$pety = $this->getPages($ptypee);
foreach ($pety as $pet)
{
$petid = $pet['id_page'];
$petnam = $pet['name'];
$pettyp = $pet['id_page_type'];
}
if (_PS_VERSION_ > '1.5.0.0')
{
		$ptypee1 = Page::getPageTypeByName('product');
		$ptypee2 = Page::getPageTypeByName('product.php');
		$ptypee3 = Page::getPageTypeByName('index');
		$ptypee4 = Page::getPageTypeByName('index.php');
		$ptypee5 = Page::getPageTypeByName('cms');
		$ptypee6 = Page::getPageTypeByName('cms.php');

}

			$nbr = Configuration::get('RESPOI_NBR');
			if ($nbr == 1){
				$nbr = 'true';
			} else {
				$nbr = 'false';
			}
			$this->context->smarty->assign(array(
			'respoi'      => $nbr,
			'psversion'      => _PS_VERSION_,
		
		));
		if ($pettyp == $ptypee1 || $pettyp == $ptypee2 || $pettyp == $ptypee3 || $pettyp == $ptypee4 || $pettyp == $ptypee5 || $pettyp == $ptypee6)
			{	
				if (_PS_VERSION_ < "1.7.0.0"){
	return $this->display(__FILE__, 'views/templates/front/respoi-footer.tpl');
				}
			}

	}
    public function hookdisplayBeforeBodyClosingTag($params)
    {
		$ptypee=Page::getCurrentId();
$pety = $this->getPages($ptypee);
foreach ($pety as $pet)
{
$petid = $pet['id_page'];
$petnam = $pet['name'];
$pettyp = $pet['id_page_type'];
}
if (_PS_VERSION_ > '1.5.0.0')
{
		$ptypee1 = Page::getPageTypeByName('product');
		$ptypee2 = Page::getPageTypeByName('product.php');
		$ptypee3 = Page::getPageTypeByName('index');
		$ptypee4 = Page::getPageTypeByName('index.php');
		$ptypee5 = Page::getPageTypeByName('cms');
		$ptypee6 = Page::getPageTypeByName('cms.php');

}

			$nbr = Configuration::get('RESPOI_NBR');
			if ($nbr == 1){
				$nbr = 'true';
			} else {
				$nbr = 'false';
			}
			$this->context->smarty->assign(array(
			'respoi'      => $nbr,
			'psversion'      => _PS_VERSION_,
		
		));
		if ($pettyp == $ptypee1 || $pettyp == $ptypee2 || $pettyp == $ptypee3 || $pettyp == $ptypee4 || $pettyp == $ptypee5 || $pettyp == $ptypee6)
			{	
	return $this->display(__FILE__, 'views/templates/front/respoi-footer.tpl');
			}
    }
	public function hookHeader($params)
	{
$ptypee=Page::getCurrentId();
$pety = $this->getPages($ptypee);
foreach ($pety as $pet)
{
$petid = $pet['id_page'];
$petnam = $pet['name'];
$pettyp = $pet['id_page_type'];
}
if (_PS_VERSION_ > '1.5.0.0')
{
		$ptypee1 = Page::getPageTypeByName('product');
		$ptypee2 = Page::getPageTypeByName('product.php');
		$ptypee3 = Page::getPageTypeByName('index');
		$ptypee4 = Page::getPageTypeByName('index.php');
		$ptypee5 = Page::getPageTypeByName('cms');
		$ptypee6 = Page::getPageTypeByName('cms.php');

}

		$iso_code = Language::getIsoById((int)$this->context->language->id );

			$nbr = Configuration::get('RESPOI_NBR');
			$this->context->smarty->assign(array(
			'respoi'      => $nbr,
			'psversion'      => _PS_VERSION_,
		
		));
		
	if ($pettyp == $ptypee1 || $pettyp == $ptypee2 || $pettyp == $ptypee3 || $pettyp == $ptypee4 || $pettyp == $ptypee5 || $pettyp == $ptypee6)
			{	
			if (_PS_VERSION_ > '1.4.0.0' && _PS_VERSION_ < '1.5.0.0')
		{
		
			Tools::addJS(__PS_BASE_URI__.'modules/responsiveiframe/views/js/flexy.js');
		}
		if (_PS_VERSION_ > '1.5.0.0')
{
			
			$this->context->controller->addJS(($this->_path).'views/js/flexy.js');
}
if (_PS_VERSION_ < "1.7.0.0") {
	return $this->display(__FILE__, 'views/templates/front/respoi-footer.tpl');
}

	}
	}

}

