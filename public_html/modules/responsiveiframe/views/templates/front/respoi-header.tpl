{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* No redistribute in other sites, or copy.
*
*  @author RSI 
*  @copyright  2007-2014 RSI
*}
{if $psversion < "1.5.0.0"}
<script src="{$module_dir}views/js/flexy.js" type="text/javascript"></script>
{/if}