{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* No redistribute in other sites, or copy.
*
*  @author RSI 
*  @copyright  2007-2014 RSI
*}

{if $psversion > '1.6.50.0'}

{literal}
<script type="text/javascript" > 
(function(){"use strict";var c=[],f={},a,e,d,b;if(!window.jQuery){a=function(g){c.push(g)};f.ready=function(g){a(g)};e=window.jQuery=window.$=function(g){if(typeof g=="function"){a(g)}return f};window.checkJQ=function(){if(!d()){b=setTimeout(checkJQ,100)}};b=setTimeout(checkJQ,100);d=function(){if(window.jQuery!==e){clearTimeout(b);var g=c.shift();while(g){jQuery(g);g=c.shift()}b=f=a=e=d=window.checkJQ=null;return true}return false}}})();
</script>
{/literal}
{/if}
{literal}
<script type="text/javascript" > 
$('iframe').flexy({
    addcss: true,
    classname: 'flexy-wrapper',
    widescreen: {/literal}{$respoi}{literal}

});
</script> 
	
	
{/literal}