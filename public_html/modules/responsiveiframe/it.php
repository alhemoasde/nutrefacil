<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_fdf0a0f26be17259f9b6160078285433'] = 'Iframes reattivo';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_cdcbf1204b17feb5f2743be000ab4224'] = 'Regolare gli iframe per schermi di piccole dimensioni - www.catalogo-onlinersi.net';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_254f642527b45bc260048e30704edb39'] = 'Configurazione';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_32802122a081816d701f505bf6fd8bbf'] = 'Modalità widescreen';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_7fbaad05d376326bb1a7a30cd5e52120'] = 'Utilizzare widescreen (per i video di vimeo)';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_c9cc8cce247e49bae79f15173ce97354'] = 'Salva';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_f4d1ea475eaa85102e2b4e6d95da84bd'] = 'Conferma';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_c888438d14855d7d96a2724ee9c306bd'] = 'Impostazioni aggiornate';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_f4f70727dc34561dfde1a3c529b6205c'] = 'Impostazioni';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_a6105c0a611b41b08f1209506350279e'] = 'Sì';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_7fa3b767c460b54a2be4d49030b349c7'] = 'No';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_113d665fe5d9f23aba45cfe3a9ab7dff'] = 'Utilizzare la modalità widescreen (per videmo)';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_9887a4451812854f0f1b6f669a874307'] = 'Contribuire';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_e8928d746f7cf273fa76aabf3f906216'] = 'Puoi contribuire con una donazione se i nostri moduli gratuiti e temi sono utili per voi. Clicca sul link e sostenerci!';
$_MODULE['<{responsiveiframe}prestashop>responsiveiframe_79674286356b8bc9c2b77d21247c953d'] = 'Per ulteriori moduli & temi visitare: www.catalogo-onlinersi.net';
